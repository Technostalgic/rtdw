Design Pillars

## 1 - Weapon Modification
The game revolves around creating weapons that behave exactly as you specify based on what mods you 
put into the weapon. Weapons consist of a weapon base and an array of mod slots that can be fitted 
with weapon mods. Weapon bases will specify the weapon's base attributes which are then modified by 
the mods that you put into the weapon.

Each wapon mod has a specific slot type that it will fit into, and may also provide additional slots
to the weapon that other mods may be slotted into. 

Some weapon mods may have base requirements that the weapon needs to meet in order for the mod to be
slotted onto the weapon. For instance, a weapon mod that increases the weapon's accuracy may have a 
requirement that it the weapon cannot fire more than one projectile at a time. Or a weapon mod that
provides seeking projectiles may require that the weapon fires a rocket-type projectile. 

**Main attributes:**  
* Damage  
* Fire Rate  
* Accuracy  
* Clip Size  
* Energy / Battery Power (will be consumed by mods with special behaviors)  
* Reload Speed  
* Projectile Count  
* Recoil  

There are also *special* attributes, where certain aspects of the weapon are effected based on a 
weighted value thatt's linked to a specific object type. For example, projectile type, each mod can
contribute to a weight for a specific projectile type, and the weapon will use whichever 
projectile type has the highest cumulative weight out of the pool. 

**Special attributes:**  
* Projectile Type (which projectile the weapon fires)  
* Reload Type (how the gun is reloaded, by clip, per shell, recharge, etc)  
* Damage Type (what type of damage the weapon deals, the total 'Damage' attribute value will be  
		split among the different damage types based on their weight)  

## 2 - Loot
Loot will drive the core weapon modification mechanic of the game. Players will try to create the 
most effective or fun weapons to use, and then while using those weapons to slay foes, they will be
rewarded with more weapon mods and bases to further customize their weapons.

The game's most sought after loot will consist mainly of weapon bases and weapon mods. But there 
will also be consumables and currency to collect. Most enemies will drop basic loot, with tougher 
enemies dropping more useful loot items. There will be chests that that the player can find in the 
environment which will generally contain high quality items. There will also be breakable physics
props such as crates and barrels that the player will recieve consumables and occasionally items 
from.

The player will also encounter Merchants and shops that they will be able to trade with. They'll 
be able to sell their current items for currency, or buy items from the seller with said currency. 
This will allow the player to purchase weapon modifications with specific attributes from a wide 
variety. This increases the chance that the player will be able to seek out a certain style of 
weapon with a specific behavior instead of relying entirely on RNG to get the type of weapon mods 
that they want.

## 3 - Combat
Combat will be fast paced and optionally tactical. In order to avoid too many balancing issues, 
enemies will be quick to kill and the player character will also be quick to kill. This allows 
flexibility such that players will be given the freedom to create over-the-top weaponry that is fun
to use without feeling like they have too much of an advantage. For an example of this, see some of
the over-the-top wand weapons that players of the game 'Noita' have created.

This game seeks to achieve a more satisfying version of combat that isn't completely unbelievable. 
Unlike what you see in more traditional RPGs such as borderlands, where you can unload 300 rounds 
into a shirtless bandit and they hardly flinch. This game will focus on methodical combat and weapon
*behavior*, rather than a list of stats that have to reach a certain threshold before becoming 
effective.

A wide variety of different weapon mod behaviors will also allow the user to take things a bit 
slower and safer, if that's their style. Add a bounce modifier to a weapon so you can hit enemies 
from behind cover, use trigger or conditional modifiers to set up a trap, or use specialized damage 
types to more effectively take down a specific enemy type, etc.