using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	[CreateAssetMenu(fileName = "Stat", menuName = "RTDW/Stats/Stat")]
	public class Stat : ScriptableObject
	{
		// ----------------------------------------------------------------------------------------

		[field: SerializeField]
		public string Label { get; private set; } = "Default Stat";

		public int Uid { get; private set; } =
			new System.Random().Next(int.MinValue, int.MaxValue);

		[field: SerializeField, Tooltip("A label that describes what the numeric value of this " +
			"stat does (the value that's returned when the stat value is passed into the " +
			"numeric function)")]
		public string NumericLabel { get; private set; } = "Numeric Value";

		[field: SerializeField, Space, Tooltip("The function used to convert the stat value into " +
			"a number that is mathematically useful, for example - the 'firerate' stat into a " +
			" 'shots per second' value")]
		public AnimationCurve NumericFunction { get; private set; } = new AnimationCurve(
				new Keyframe(0, 0, 0, 0, 0, 0),
				new Keyframe(100, 100, 0, 0, 0, 0)
			);

		[field: SerializeField, Tooltip("What happens when the Numeric Function is given an input above its highest range")]
		public InputExceedRangeMode LowInputMode { get; private set; } = InputExceedRangeMode.ClampAt;

		[field: SerializeField, Tooltip("The value referred to by the Low Input Mode")]
		public float LowInputValue { get; private set; } = 0;

		[field: SerializeField, Tooltip("What happens when the Numeric Function is given an input below its lowest range")]
		public InputExceedRangeMode HighInputMode { get; private set; } = InputExceedRangeMode.ExtrapolateFromAverageSlope;

		[field: SerializeField, Tooltip("The value referred to by the High Input Mode")]
		public float HighInputValue { get; private set; } = 100;

		// ----------------------------------------------------------------------------------------

		[ContextMenu("Print Function Keys")]
		private void PrintFunctionKeys()
		{
			for (int i = 0; i < NumericFunction.length; i++) Debug.Log(NumericFunction[i].time);
		}

		[ContextMenu("Numeric Function/Default")]
		private void SetDefaultNumericFunction()
		{
			NumericFunction = new AnimationCurve(
				new Keyframe(0, 0, 0, 0, 0, 0),
				new Keyframe(100, 100, 0, 0, 0, 0)
			);
			LowInputMode = InputExceedRangeMode.ClampAt;
			LowInputValue = 0;
			HighInputMode = InputExceedRangeMode.ExtrapolateFromAverageSlope;
			HighInputValue = 100;
		}

		[ContextMenu("Numeric Function/Linear")]
		private void SetLinearNumericFunction()
		{
			NumericFunction = new AnimationCurve(
				new Keyframe(0, LowInputValue, 0, 0, 0, 0),
				new Keyframe(100, HighInputValue, 0, 0, 0, 0)
			);
			LowInputMode = InputExceedRangeMode.ClampAt;
			HighInputMode = InputExceedRangeMode.ExtrapolateFromAverageSlope;
		}

		[ContextMenu("Numeric Function/Invert")]
		private void InvertNumericFunction()
		{
			AnimationCurve newcurve = new AnimationCurve();

			float highestTime = NumericFunction[NumericFunction.length - 1].time;
			foreach (Keyframe k in NumericFunction.keys)
			{
				newcurve.AddKey(new Keyframe(highestTime - k.time, k.value, 0, 0));
			}

			NumericFunction = newcurve;
		}

		// ----------------------------------------------------------------------------------------

		[ContextMenu("Print UID to console")]
		public void PrintUid()
		{
			Debug.Log("Stat UID of '" + name + "' is: " + Uid);
		}

		[ContextMenu("Change UID to new random value")]
		public void GenerateNewUid()
		{
			int old = Uid;
			Uid = new System.Random().Next(int.MinValue, int.MaxValue);
			Debug.Log("Stat UID of '" + name + "' changed: " + old + " -> " + Uid);
		}

		public override bool Equals(object other)
		{
			if (!base.Equals(other)) return false;
			if (other.GetType() != GetType()) return false;

			return this == (Stat)other;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public override string ToString()
		{
			return Label + " (Stat Object)";
		}

		// ----------------------------------------------------------------------------------------

		public static bool operator ==(Stat a, Stat b)
		{
			if (!a || !b) return false;
			return a.Uid == b.Uid;
		}

		public static bool operator !=(Stat a, Stat b)
		{
			if (!a && !b) return false;
			if (!a || !b) return true;
			return !(a == b);
		}

		// ----------------------------------------------------------------------------------------

		private float ApprochValueFromLeft(float input, float target, float slope, float off)
		{
			float off2DivPi = (off - target) * 2 / Mathf.PI;
			float r = input < 0 ?
				slope * input + off :
				Mathf.Atan(slope * input / off2DivPi) * off2DivPi + off;
			return r;

		}

		private float ApprochValueFromRight(float input, float target, float slope, float off)
		{
			float off2DivPi = (off - target) * 2 / Mathf.PI;
			float r = input > 0 ?
				slope * input + off :
				Mathf.Atan(slope * input / off2DivPi) * off2DivPi + off;
			return r;
		}

		public float ApplyNumericFunction(float statValue)
		{
			if (NumericFunction.keys.Length > 0)
			{
				float trange, vrange, slope;

				// input too low
				if (NumericFunction.keys[0].time > statValue)
				{
					switch (LowInputMode)
					{
						case InputExceedRangeMode.ClampAt: return LowInputValue;
						case InputExceedRangeMode.ExtrapolateFromAverageSlope:
							trange = NumericFunction[NumericFunction.length - 1].time -
								NumericFunction[0].time;
							vrange = NumericFunction[NumericFunction.length - 1].value -
								NumericFunction[0].value;
							slope = vrange / trange;
							return statValue * slope + NumericFunction[0].value;

						case InputExceedRangeMode.ApproachValue:
							trange = NumericFunction[NumericFunction.length - 1].time -
								NumericFunction[0].time;
							vrange = NumericFunction[NumericFunction.length - 1].value -
								NumericFunction[0].value;
							slope = vrange / trange;
							return ApprochValueFromRight(
								statValue - NumericFunction[0].time, LowInputValue, slope, NumericFunction[0].value
							);
					}
				}

				// input too high
				else if (NumericFunction.keys[NumericFunction.length - 1].time < statValue)
				{
					switch (HighInputMode)
					{
						case InputExceedRangeMode.ClampAt: return HighInputValue;
						case InputExceedRangeMode.ExtrapolateFromAverageSlope:
							trange = NumericFunction[NumericFunction.length - 1].time -
								NumericFunction[0].time;
							vrange = NumericFunction[NumericFunction.length - 1].value -
								NumericFunction[0].value;
							slope = vrange / trange;
							return statValue * slope +
								NumericFunction[NumericFunction.length - 1].value;

						case InputExceedRangeMode.ApproachValue:
							trange = NumericFunction[NumericFunction.length - 1].time -
								NumericFunction[0].time;
							vrange = NumericFunction[NumericFunction.length - 1].value -
								NumericFunction[0].value;
							slope = vrange / trange;
							return ApprochValueFromLeft(
								statValue - trange, HighInputValue, slope, NumericFunction[NumericFunction.length - 1].value
							);
					}
				}
			}
			return NumericFunction.Evaluate(statValue);
		}

		// ----------------------------------------------------------------------------------------

		[Serializable]
		public class Modifier
		{
			public Modifier(Stat stat, float add = 0, float multiply = 0)
			{
				Stat = stat;
				Addition = add;
				Multiplier = multiply;
			}

			[field: SerializeField]
			public Stat Stat { get; private set; } = null;

			[field: SerializeField]
			public float Addition { get; set; } = 0;

			[field: SerializeField]
			public float Multiplier { get; set; } = 0;

			public bool IsValid { get => Stat != null; }
		}

		[Serializable]
		public class Requirement
		{
			public Requirement() { }

			// ------------------------------------------------------------------------------------

			[field: SerializeField]
			public Stat Stat { get; set; } = null;

			[field: SerializeField]
			public float Value { get; set; } = 0;

			[field: SerializeField]
			public ComparisonMode Mode { get; set; } = ComparisonMode.GreaterThanOrEqual;

			// ------------------------------------------------------------------------------------

			public bool MeetsRequirement(EntityStats set)
			{
				switch (Mode)
				{
					case ComparisonMode.GreaterThan: return set[Stat] > Value;
					case ComparisonMode.GreaterThanOrEqual: return set[Stat] >= Value;
					case ComparisonMode.LessThan: return set[Stat] < Value;
					case ComparisonMode.LessThanOrEqual: return set[Stat] <= Value;
				}
				return false;
			}

			// ------------------------------------------------------------------------------------

			public enum ComparisonMode
			{
				GreaterThan,
				GreaterThanOrEqual,
				LessThan,
				LessThanOrEqual
			}
		}

		// ----------------------------------------------------------------------------------------

		public enum InputExceedRangeMode
		{
			ClampAt = 0,
			ExtrapolateFromAverageSlope = 1,
			ApproachValue = 2
		}
	}
}