using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTDW.Weapons;

namespace RTDW
{
	public class TurretControl : MonoBehaviour
	{
		[field: SerializeField]
		public Camera View { get; set; } = null;

		[field: SerializeField]
		public Weapon Controlled { get; private set; }

		[SerializeField]
		private KeyCode fire = KeyCode.RightControl;

		[SerializeField]
		private KeyCode triggerAlt = KeyCode.RightAlt;

		[SerializeField]
		private KeyCode reload = KeyCode.R;

		// --------------------------------------------------------------------------------------------

		private void HandleAim()
		{
			Vector2 mouse = View.ScreenToWorldPoint(Input.mousePosition);
			Vector2 dif = mouse - (Vector2)transform.position;
			float ang = Mathf.Atan2(dif.y, dif.x);
			transform.rotation = Quaternion.Euler(0, 0, ang * Mathf.Rad2Deg);
		}

		private void HandleActions()
		{
			if (Input.GetMouseButton(0) || Input.GetKey(fire))
			{
				Controlled.TriggerFire();
			}

			if (Input.GetMouseButton(2) || Input.GetKey(triggerAlt))
			{
				Controlled.TriggerAlt();
			}

			if (Input.GetKey(reload))
			{
				Controlled.TriggerReload();
			}
		}

		// --------------------------------------------------------------------------------------------

		private void Update()
		{
			HandleAim();
			HandleActions();
		}
	}
}