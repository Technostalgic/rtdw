using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	[System.Serializable]
	public class DamageData : IReadonlyDamageData, IEnumerable<KeyValuePair<DamageType, float>>
	{
		public DamageData(params (DamageType, float)[] damages)
		{
			for (int i = damages.Length - 1; i >= 0; i--)
			{
				data.Add(damages[i].Item1, damages[i].Item2);
			}
		}

		// --------------------------------------------------------------------------------------------

		[SerializeField]
		private Dictionary<DamageType, float> data = new Dictionary<DamageType, float>();

		public float Total
		{
			get
			{
				float r = 0;
				foreach (var kv in data) r += kv.Value;
				return r;
			}
		}

		// --------------------------------------------------------------------------------------------

		IEnumerator IEnumerable.GetEnumerator() => data.GetEnumerator();

		public IEnumerator<KeyValuePair<DamageType, float>> GetEnumerator() => data.GetEnumerator();

		public bool ContainsKey(DamageType key) => data.ContainsKey(key);

		// --------------------------------------------------------------------------------------------

		public void PrintData()
		{
			foreach (var kv in data) Debug.Log(kv.Key.ToString() + ": " + kv.Value);
		}

		/// <summary>
		/// Adds all the damage types from b to itself
		/// </summary>
		/// <param name="b">the damage data to add from</param>
		public void Combine(IReadonlyDamageData b)
		{
			foreach (var kv in b) this[kv.Key] += kv.Value;
		}

		/// <summary>
		/// multiplies each damage data component by the specified value
		/// </summary>
		/// <param name="factor">the amount to multiply by</param>
		public void MultiplyScalar(in float factor)
		{
			foreach (var kv in this) this[kv.Key] *= factor;
		}

		/// <summary> clears all damage data </summary>
		public void Clear() => data.Clear();

		/// <summary>
		/// sets all damage type values to zero so all damage is still cleared but the underlying 
		/// dictionary is not modified
		/// </summary>
		public void Clear_NoDealloc()
		{
			// TODO this method still allocates memory
			Clear();
		}

		/// <summary> sets the damage of the specified type </summary>
		/// <param name="key">the damage type to  record damage for</param>
		/// <param name="value">the amount of damage</param>
		public void Set(DamageType key, float value)
		{
			if (data.ContainsKey(key))
			{
				if (value == 0) data.Remove(key);
				else data[key] = value;
			}
			else if (value != 0) data.Add(key, value);
		}

		// --------------------------------------------------------------------------------------------

		public float this[DamageType key]
		{
			get
			{
				if (data.ContainsKey(key)) return data[key];
				return 0;
			}
			set => Set(key, value);
		}

		/// <summary>
		/// Adds all the damage types from b to a and returns the new value
		/// </summary>
		/// <param name="a">the lefthand DamageData object that is being added to</param>
		/// <param name="b">the righthand DamageData object that is adding value</param>
		public static DamageData operator +(DamageData a, IReadonlyDamageData b)
		{
			DamageData r = a.Clone();
			r.Combine(b);
			return r;
		}

		/// <summary>
		/// Multiplies all the damage values in this data by the specified factor and 
		/// returns the result
		/// </summary>
		/// <param name="a">the damage data to multiply</param>
		/// <param name="b">the scale factor to multiply by</param>
		public static DamageData operator *(DamageData a, in float b)
		{
			DamageData r = a.Clone();
			r.MultiplyScalar(b);
			return r;
		}

		// --------------------------------------------------------------------------------------------

		/// <summary>
		/// copies all the fields from the other damage data object. NOTE, this object should be 
		/// Cleared first, as some values may be left over from old data if not
		/// </summary>
		/// <param name="other"></param>
		public void CopyFrom(IReadonlyDamageData other)
		{
			foreach (var kv in other) this[kv.Key] = kv.Value;
		}

		public DamageData Clone()
		{
			DamageData r = new DamageData();
			foreach (var kv in data) r.data.Add(kv.Key, kv.Value);
			return r;
		}
	}

	public interface IReadonlyDamageData : IEnumerable<KeyValuePair<DamageType, float>>
	{
		public void PrintData();
		public float this[DamageType key] { get; }
		public DamageData Clone();
	}
}
