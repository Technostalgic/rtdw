using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTDW.Actors;

namespace RTDW
{
	[RequireComponent(typeof(CommandPalette))]
	public class CommandsFromInput : MonoBehaviour
	{
		private CommandPalette commands = null;
		public CommandPalette Commands => commands == null ?
			commands = GetComponent<CommandPalette>() : commands;

		[SerializeField]
		private InputControl[] inputControls = null;
		public IReadOnlyList<InputControl> InputControls => inputControls;

		// ----------------------------------------------------------------------------------------

		public void ResolveInputControls()
		{
			foreach (InputControl control in inputControls)
			{
				control.Resolve(Commands);
			}
		}

		private void Update()
		{
			ResolveInputControls();
		}

		// ----------------------------------------------------------------------------------------

		[System.Serializable]
		public class InputControl
		{
			public InputMode mode = InputMode.keyboardButton;

			public KeyCode keyInput = default;

			public int mouseButton = default;

			public string command = "Command";

			private int? commandHash = null;
			private int CommandHash => commandHash.HasValue ?
				commandHash.Value : (commandHash = command.GetHashCode()).Value;

			public Command.ParameterType commandParamType = Command.ParameterType.Void;

			public float analogueParamValue = 0;

			public Vector2 vectorParamValue = default;

			// ------------------------------------------------------------------------------------

			private void ResolveCommand(Command cmd)
			{
				switch (commandParamType)
				{
					case Command.ParameterType.Void: cmd.Send(); break;
					case Command.ParameterType.Analogue: cmd.Send(analogueParamValue); break;
					case Command.ParameterType.Vector: cmd.Send(vectorParamValue); break;
					default: cmd.Send(); break;
				}
			}

			// ------------------------------------------------------------------------------------

			public void Resolve(CommandPalette palette)
			{
				Command cmd = palette.TryGetCommand(CommandHash);
				if (cmd == null) return;

				switch (mode)
				{
					case InputMode.keyboardButton:
						if (Input.GetKey(keyInput)) ResolveCommand(cmd);
						break;
					case InputMode.mouseButton:
						if (Input.GetMouseButton(mouseButton)) ResolveCommand(cmd);
						break;
				}
			}

			// ------------------------------------------------------------------------------------

			public enum InputMode
			{
				keyboardButton,
				mouseButton
			}
		}
	}
}
