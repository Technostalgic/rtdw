using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	public class WeaponCursor : MonoBehaviour
	{
		[field: SerializeField]
		private RectTransform Centroid { get; set; } = null;

		[field: SerializeField]
		private PlayerController Player { get; set; } = null;

		[field: SerializeField]
		private Camera Camera { get; set; } = null;

		// ----------------------------------------------------------------------------------------

		private void PositionFromPlayer()
		{
			Vector2 worldMouse = Camera.ScreenToWorldPoint(transform.position);

			float baseSpread = Player.Gun.Stats[GameManager.InitParams.GunStats.Accuracy].FunctionalValue;
			float imgSpread = baseSpread;
			imgSpread += Player.Gun.AimRecoil.Spread;

			Vector2 localOff = Player.AimVector;
			Vector2 targetPos = Player.Gun.ProjectileSpawnPoint.position;
			// float offDist = 1 + localOff.magnitude * 2f;
			float minDistFactor = 1f;
			float maxDistFactor = 1f;
			Vector2 off = Player.AimVector.normalized * minDistFactor + Player.AimVector * maxDistFactor;
			targetPos += off;
			transform.position = Camera.WorldToScreenPoint(targetPos);

			float dist = Vector2.Distance(Player.Gun.ProjectileSpawnPoint.position, targetPos);
			Vector2 temp1 = Camera.WorldToScreenPoint(new Vector3(targetPos.x + dist, 0, 0));
			dist = (temp1.x - transform.position.x) * 2;

			imgSpread *= dist;

			Centroid.sizeDelta = new Vector2(imgSpread, imgSpread);
		}

		// ----------------------------------------------------------------------------------------

		private void Update()
		{
			PositionFromPlayer();
		}
	}
}
