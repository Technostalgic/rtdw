﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace RTDW.UI
{
	public class StatsList : MonoBehaviour
	{
		// Data Fields: ---------------------------------------------------------------------------

		[SerializeField]
		private EntityStats entity = null;

		[SerializeField]
		private StatsTextLink[] statLinks = null;

		// Primary Functionality: -----------------------------------------------------------------

		public void UpdateUI(EntityStats target = null)
		{
			if (target != null) entity = target;
			if (target == null) return;

			foreach(StatsTextLink link in statLinks)
			{
				if (link.stat == null) continue;

				EntityStats.StatValue val = entity[link.stat];
				if (val == null) continue;

				if (link.valueText != null)
				{
					link.valueText.text = val.Value.ToString("0.0");
				}

				if (link.unitsText != null)
				{
					link.unitsText.text = "(" +
						val.FunctionalValue.ToString("0.0") + link.stat.NumericLabel +
						")";
				}
			}
		}

		// Subtype Definitions: -------------------------------------------------------------------

		[System.Serializable]
		public struct StatsTextLink
		{
			public Stat stat;

			public Text valueText;

			public Text unitsText;
		}
	}
}