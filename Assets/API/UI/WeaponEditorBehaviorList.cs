﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RTDW.Weapons;

namespace RTDW.UI
{
	public class WeaponEditorBehaviorList : MonoBehaviour
	{
		// Data Fields: ---------------------------------------------------------------------------

		private readonly List<WeaponBehaviorSlot> slotPool = new List<WeaponBehaviorSlot>();

		[SerializeField]
		private WeaponBehaviorSlot slotPrefab = null;

		[SerializeField]
		private RectTransform container = null;

		// Primary Functionality: -----------------------------------------------------------------

		public void UpdateSlotList(Weapon wep)
		{
			ClearSlotList();
			foreach(BehaviorTrait trait in wep.AllBehaviorTraits)
			{
				WeaponBehaviorSlot slot = GetSlot(trait);
				slot.transform.SetParent(container);
			}
		}

		// Auxiliary Functionality: ---------------------------------------------------------------
		
		public void ClearSlotList()
		{
			WeaponBehaviorSlot[] slots = container.GetComponentsInChildren<WeaponBehaviorSlot>();
			foreach (WeaponBehaviorSlot slot in slots) RemoveSlot(slot);
		}

		// Internal Functionality: ----------------------------------------------------------------

		private void RemoveSlot(WeaponBehaviorSlot slot)
		{
			slot.gameObject.SetActive(false);
			slot.transform.SetParent(null, false);
			slotPool.Add(slot);
		}

		private WeaponBehaviorSlot GetSlot(BehaviorTrait trait)
		{
			WeaponBehaviorSlot r;

			if(slotPool.Count > 0)
			{
				int index = slotPool.Count - 1;
				r = slotPool[index];
				slotPool.RemoveAt(index);
			}
			else
			{
				r = Instantiate(slotPrefab);
			}
			r.SetTrait(trait);
			r.gameObject.SetActive(true);

			return r;
		}

		// Unity Messages: ------------------------------------------------------------------------

		private void Start()
		{
			
		}
	}
}