﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace RTDW.UI
{
	public class SimpleTextButton : Selectable
	{
		// Data Fields: ---------------------------------------------------------------------------

		[Space]

		[SerializeField]
		private readonly UnityEvent onSelectAction = new UnityEvent();

		[SerializeField]
		private Text textComponent = null;

		// Property Definitions: ------------------------------------------------------------------

		public string TextLabel
		{
			get => textComponent.text;
			set => textComponent.text = value;
		}

		public UnityEvent OnSelectAction => onSelectAction;

		// ----------------------------------------------------------------------------------------

		public override void OnSelect(BaseEventData eventData)
		{
			base.OnSelect(eventData);
			onSelectAction?.Invoke();
		}
	}
}