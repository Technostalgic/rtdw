using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using RTDW.Weapons;

namespace RTDW.UI
{
	public class WeaponModList : MonoBehaviour
	{
		[SerializeField]
		private WeaponMod[] modsToLoad = new WeaponMod[] { };

		[SerializeField]
		private WeaponModSource uiPrefab = null;

		[SerializeField]
		private RectTransform uiContainer = null;

		[field: SerializeField]
		public Weapon TargetWeapon { get; set; }

		// ----------------------------------------------------------------------------------------

		[ContextMenu("Remove duplicate mods from array")]
		public void RemoveDuplicateMods()
		{
			List<WeaponMod> mods = new List<WeaponMod>();

			for (int i0 = modsToLoad.Length - 1; i0 >= 0; i0--)
			{
				if (!mods.Contains(modsToLoad[i0]) && modsToLoad[i0] != null) mods.Add(modsToLoad[i0]);
			}

			modsToLoad = mods.ToArray();
		}

		// ----------------------------------------------------------------------------------------


		private void Start()
		{
			for (int i = uiContainer.childCount - 1; i >= 0; i--)
			{
				Destroy(uiContainer.GetChild(0).gameObject);
			}

			foreach (WeaponMod mod in modsToLoad)
			{
				WeaponModSource uiInstance = Instantiate(uiPrefab);
				uiInstance.transform.SetParent(uiContainer, false);
				uiInstance.WeaponMod = mod;
				uiInstance.TargetWeapon = TargetWeapon;
			}
		}
	}
}