using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using RTDW.Weapons;

namespace RTDW.UI
{
	[AddComponentMenu("RTDW/UI/WeaponModSlot")]
	public class WeaponModSlot : Selectable, IPointerClickHandler
	{
		[field: SerializeField]
		public WeaponMod.Slot Slot { get; set; } = null;

		public WeaponMod SlottedMod
		{
			get => Slot.EquippedMod;
			set
			{
				if (value == null)
				{
					Slot.EquippedMod = null;
				}
				else
				{
					Slot.EquippedMod = value;
				}

				RefreshSlotVisuals();
			}
		}

		[SerializeField]
		private Image icon = null;

		[SerializeField]
		private Text labelText = null;

		public string Label
		{
			get => labelText?.text ?? "";
			set { if (labelText != null) labelText.text = value; }
		}

		// ----------------------------------------------------------------------------------------

		[ContextMenu("Refresh slot icon")]
		public void RefreshSlotVisuals()
		{
			icon.sprite = GameUI.Instance.GetWeaponSlotSprite(Slot.SlotType);
			if (SlottedMod != null)
			{
				Label = SlottedMod.name;
				if(labelText != null) labelText.color = Slot.EquippedMod != null ? Color.white : Color.gray;
				// TODO check if it's compatible
				// labelText.color = Slot.GetIsModUsable(TargetWeapon) ?
				// 	new Color(0.2f, 0.2f, 0.2f) : Color.red;
			}

			else Label = "";
		}

		// ----------------------------------------------------------------------------------------

		public void OnPointerClick(PointerEventData eventData)
		{
			if (!interactable) return;

			switch (eventData.button)
			{
				case PointerEventData.InputButton.Left:
					WeaponMod cursorItm = GameUI.CursorItem?.ItemTarget as WeaponMod;
					if (cursorItm != null && SlottedMod == null)
					{
						if (WeaponMod.CheckSlotTypeCompatibility(Slot.SlotType, cursorItm.RequiredSlotType))
						{
							Slot.EquippedMod = cursorItm;
							RefreshSlotVisuals();
							GameUI.CursorContainer?.RemoveItem(GameUI.CursorItem);
							GameUI.RemoveCursorItem();
						}
					}
					else if (cursorItm == null && SlottedMod != null)
					{
						GameUI.SetCursorItem(SlottedMod.StorageItem);
						SlottedMod = null;
					}
					break;
				case PointerEventData.InputButton.Right:
					SlottedMod = null;
					break;
			}
		}

		public override void OnPointerEnter(PointerEventData eventData)
		{
			base.OnPointerEnter(eventData);

			RectTransform rt = transform as RectTransform;
			if (SlottedMod == null)
			{
				GameUI.TooltipRef.OpenGeneralTooltip(
					eventData.position, 
					"Mod Slot", 
					Slot.SlotType.ToString() + " - " + (Slot.Required ? "required" : "optional"));
				GameUI.TooltipRef.AddSubtext("Type", Slot.SlotType.ToString());
			}
			else
			{
				GameUI.TooltipRef.OpenModTooltip(eventData.position, SlottedMod);
			}
		}

		public override void OnPointerExit(PointerEventData eventData)
		{
			base.OnPointerExit(eventData);
			GameUI.TooltipRef.CloseTooltip();
		}

		// ----------------------------------------------------------------------------------------

		protected override void Start()
		{
			RefreshSlotVisuals();
		}
	}
}