using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RTDW.UI
{
	public class TooltipSubtextUI : MonoBehaviour
	{
		[field: SerializeField]
		public Text LabelPart { get; private set; } = null;

		[field: SerializeField]
		public Text ValuePart { get; private set; } = null;
	}
}
