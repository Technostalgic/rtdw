﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RTDW.Weapons;

namespace RTDW.UI
{
	public class WeaponModTreeNode : MonoBehaviour
	{
		// Flag Fields: ---------------------------------------------------------------------------

		private bool targetSlotDirty = true;

		// Data Fields: ---------------------------------------------------------------------------

		private WeaponMod.Slot targetSlot = null;

		[SerializeField]
		private Image containerImage = null;

		[SerializeField]
		private Image iconImage = null;

		[SerializeField]
		private HorizontalLayoutGroup childContainerGroup = null;

		private List<WeaponModTreeNode> children = new List<WeaponModTreeNode>();

		private WeaponModSlot modSlotUiComponent = null;

		// Property Definitions: ------------------------------------------------------------------

		public WeaponMod.Slot TargetSlot => targetSlot;

		public RectTransform ChildContainer => childContainerGroup.transform as RectTransform;

		public WeaponModTreeNode Parent
		{
			get
			{
				Transform parentTransform = transform.parent?.parent;
				if (parentTransform == null) return null;
				return Parent.GetComponent<WeaponModTreeNode>();
			}
		}

		public IReadOnlyList<WeaponModTreeNode> Children => children;

		public WeaponModSlot ModSlotUiComponent => modSlotUiComponent;

		// Primary Functionality: -----------------------------------------------------------------

		/// <summary>
		/// add a child node to this node in the UI scene hierarchy
		/// </summary>
		/// <param name="node">the node to add as a child</param>
		public void AddChild(WeaponModTreeNode node)
		{
			children.Add(node);
			node.transform.SetParent(ChildContainer, false);
		}

		/// <summary>
		/// Attach the node to a weapon slot and enable it
		/// </summary>
		/// <param name="target">the slot to attach this to</param>
		public WeaponModTreeNode AttachToSlot(WeaponMod.Slot target)
		{
			if (target == null) throw new System.ArgumentNullException("Target cannot be null");
			if (targetSlot != null) throw new System.Exception("Node is already in use");

			targetSlot = target;
			targetSlotDirty = true;
			Enable();
			return this;
		}

		/// <summary>
		/// Disable the node and detach it from the weapon slot
		/// </summary>
		/// <param name="pool">the object pool to add this object and it's children to recursively</param>
		public void Disable(List<WeaponModTreeNode> pool)
		{
			if (pool == null) throw new System.ArgumentNullException("Object pool cannot be null");

			void recursiveCall(WeaponModTreeNode target)
			{
				WeaponModTreeNode[] childs = 
					target.ChildContainer.GetComponentsInChildren<WeaponModTreeNode>();
				foreach (WeaponModTreeNode child in childs)
				{
					recursiveCall(child);
				}

				target.targetSlot = null;
				target.gameObject.SetActive(false);
				target.transform.SetParent(null, false);
				if (target.Parent != null) pool.Add(target);
			}

			recursiveCall(this);
		}

		// Internal Functionality: ----------------------------------------------------------------

		private void Enable()
		{
			gameObject.SetActive(true);
		}

		private void UpdateNodeUiToSlot()
		{
			if (targetSlot == null) return;

			iconImage.sprite = targetSlot.SlotType.SlotTypeToSprite();

			if(targetSlot.ActiveMod != null)
			{
				containerImage.color = 
					targetSlot.ActiveMod.MeetsModRequirements ? 
					Color.white : Color.red;
				iconImage.color = Color.white;
			}
			else
			{
				containerImage.color = Color.gray;
				iconImage.color = Color.gray;
			}

			if (ModSlotUiComponent != null)
			{
				ModSlotUiComponent.Slot = TargetSlot;
				if (TargetSlot.SlotType == WeaponMod.SlotType.Base)
				{
					ModSlotUiComponent.interactable = false;
				}
			}

			targetSlotDirty = false;
		}

		// Unity Messages: ------------------------------------------------------------------------

		private void OnEnable()
		{
			if (modSlotUiComponent == null)
			{
				modSlotUiComponent = containerImage.GetComponent<WeaponModSlot>();
			}
			UpdateNodeUiToSlot();
		}

		private void Update()
		{
			if (targetSlotDirty) UpdateNodeUiToSlot();
		}
	}
}