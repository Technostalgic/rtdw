using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RTDW.Weapons;

namespace RTDW.UI
{
	public class TooltipUI : MonoBehaviour
	{
		// Temporary Query Variables:--------------------------------------------------------------

		private static readonly Dictionary<Stat, (float, float)> qStatDict = 
			new Dictionary<Stat, (float, float)>();

		// ----------------------------------------------------------------------------------------

		private bool isLayoutDirty = true;

		// ----------------------------------------------------------------------------------------

		[SerializeField]
		private Text label = null;

		[SerializeField]
		private RectTransform descriptionContainer = null;

		[SerializeField]
		private Text description = null;

		[SerializeField]
		private RectTransform subtextContainer = null;

		[SerializeField]
		public TooltipSubtextUI SubtextPrefab = null;

		[SerializeField]
		private RectTransform subDescContainer = null;

		[SerializeField]
		private Text subDescPrefab = null;

		// ----------------------------------------------------------------------------------------
		
		public int SubtextCount { get; private set; } = 0;

		public int SubDescriptionCount { get; private set; } = 0;

		// Primary Functionality: -----------------------------------------------------------------

		public void OpenGeneralTooltip(Vector2 pos, string label, string description)
		{
			transform.position = pos;

			descriptionContainer.gameObject.SetActive(true);

			this.label.text = label;
			this.description.text = description;

			OpenToolTip();
		}

		public void OpenModTooltip(Vector2 pos, WeaponMod mod)
		{
			ClearSubtexts();
			ClearSubDescriptions();
			transform.position = pos;

			label.text = mod.name;

			AddSubtext(mod.RequiredSlotType.ToString(), "", new Color(0.75f, 0.75f, 0.75f));
			qStatDict.Clear();
			foreach (Stat.Modifier sm in mod.StatModifiers)
			{
				(float, float) smVals = (sm.Addition, sm.Multiplier);
				if (qStatDict.ContainsKey(sm.Stat))
				{
					smVals.Item1 += qStatDict[sm.Stat].Item1;
					smVals.Item2 += qStatDict[sm.Stat].Item2;
					qStatDict[sm.Stat] = smVals;
				}
				else qStatDict.Add(sm.Stat, smVals);
			}
			foreach(var kv in qStatDict)
			{
				string label = kv.Key.Label;
				if (kv.Value.Item1 != 0)
				{
					string val = (Mathf.Round(kv.Value.Item1 * 100f) * 0.01f).ToString("0.00");
					if (kv.Value.Item1 > 0) val = "+" + val;
					AddSubtext(
						label, val, Color.white,
						kv.Value.Item1 > 0 ? Color.green : Color.red);
					label = "";
				}
				if (kv.Value.Item2 != 0)
				{
					string val = (Mathf.Round(kv.Value.Item2 * 1000f) * 0.1f).ToString("0.0");
					if (kv.Value.Item2 > 0) val = "+" + val;
					AddSubtext(
						label, val + "%", Color.white,
						kv.Value.Item2 > 0 ? Color.green : Color.red);
				}
			}

			if (mod.TotalWeaponRequirements?.HasAnyRequirement ?? false)
			{
				AddSubtext("", "");
				AddSubtext("Requirements:", "", new Color(0.75f, 0.75f, 0.75f));
				string slotStr = "";
				// foreach(WeaponMod.SlotType slot in mod.WeaponRequirements.SlotsToFill)
				// {
				// 	slotStr += slot.ToString() + ", ";
				// }
				if (slotStr.Length > 0)
				{
					slotStr = slotStr.Remove(slotStr.Length - 2);
					AddSubtext(slotStr, "");
				}
				foreach (var req in mod.TotalWeaponRequirements.StatRequirements)
				{
					AddSubtext(req.GetLabel(), "");
				}
			}

			bool behaviorsShown = false;
			if(mod.BehaviorTraits.Length > 0)
			{
				behaviorsShown = true;
				Text label = AddSubDescription("Behaviors:");
				label.color = Color.white;
				label.alignment = TextAnchor.MiddleLeft;

				string behaviorsStr = "";
				foreach(BehaviorTrait trait in mod.BehaviorTraits) behaviorsStr += trait.Label + ", ";
				behaviorsStr = behaviorsStr.Substring(0, behaviorsStr.Length - 2);

				Text behtxt = AddSubDescription(behaviorsStr);
				behtxt.color = Color.gray;
				behtxt.alignment = TextAnchor.MiddleLeft;
			}

			if (mod.Slots.Count > 0)
			{
				if(behaviorsShown) AddSubDescription(" ");
				Text label = AddSubDescription("Slots:");
				label.alignment = TextAnchor.MiddleLeft;
				label.color = Color.white;

				foreach (WeaponMod.Slot slot in mod.Slots)
				{
					string str = slot.SlotType.ToString();
					str += " - " + (slot.Required ? "Required" : "Optional");
					Text slotDesc = AddSubDescription(str);
					slotDesc.color = slot.Required ? Color.yellow : Color.gray;
					slotDesc.alignment = TextAnchor.MiddleLeft;
				}
			}

			OpenToolTip();
		}

		public void OpenToolTip()
		{
			gameObject.SetActive(true);

			// TODO fix this hack
			// seems to force the layout to update another time before rendering but after child
			// layout modifications are made, doesn't seem to work for some reason if game object
			// is de and re-activated in LateUpdate but it does here for some reason even though
			// theoretically this should be called BEFORE LateUpdate in the execution order.
			// NOTE: The issue that this fixes seems to be one caused by nested
			// VerticalLayoutsGroups and ContentSizeFitters
			// IEnumerator co()
			// {
			// 	yield return new WaitForSecondsRealtime(0);
			// 	gameObject.SetActive(false);
			// 	gameObject.SetActive(true);
			// 	RepositionIntoScreen();
			// 	yield break;
			// }
			// StartCoroutine(co());
			LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
		}

		public void CloseTooltip()
		{
			ClearSubtexts();
			ClearSubDescriptions();
			subtextContainer.gameObject.SetActive(false);
			descriptionContainer.gameObject.SetActive(false);
			gameObject.SetActive(false);
		}

		// Aucillary Functionality: ---------------------------------------------------------------

		public void ClearSubtexts()
		{
			foreach (RectTransform rt in subtextContainer)
			{
				rt.gameObject.SetActive(false);
			}
			SubtextCount = 0;
		}

		public TooltipSubtextUI AddSubtext(
			string label, string value,
			Color? labelCol = null, Color? valueCol = null
		)
		{
			TooltipSubtextUI st;
			if (subtextContainer.childCount > SubtextCount)
			{
				st = subtextContainer.GetChild(SubtextCount).GetComponent<TooltipSubtextUI>();
				st.gameObject.SetActive(true);
			}
			else
			{
				st = Instantiate(SubtextPrefab);
				st.transform.SetParent(subtextContainer, false);
			}

			SubtextCount++;

			subtextContainer.gameObject.SetActive(true);

			st.LabelPart.text = label;
			st.LabelPart.color = labelCol ?? Color.white;

			st.ValuePart.text = value;
			st.ValuePart.color = valueCol ?? Color.white;

			return st;
		}

		public void ClearSubDescriptions()
		{
			foreach(RectTransform rt in subDescContainer)
			{
				rt.gameObject.SetActive(false);
			}
			SubDescriptionCount = 0;
		}

		public Text AddSubDescription(string text)
		{
			Text txt;
			if(subDescContainer.childCount > SubDescriptionCount)
			{
				txt = subDescContainer.GetChild(SubDescriptionCount).GetComponent<Text>();
				txt.gameObject.SetActive(true);
			}
			else
			{
				txt = Instantiate(subDescPrefab);
				txt.transform.SetParent(subDescContainer, false);
			}

			txt.color = Color.grey;
			txt.alignment = TextAnchor.UpperCenter;

			txt.text = text;

			SubDescriptionCount++;

			return txt;
		}

		// Internal Functionality: ----------------------------------------------------------------

		private void RepositionIntoScreen()
		{
			RectTransform rt = transform as RectTransform;
			Vector2 rtpos = rt.position;

			// TODO
		}

		// ----------------------------------------------------------------------------------------

		private void Awake()
		{
			CloseTooltip();
		}
	}
}
