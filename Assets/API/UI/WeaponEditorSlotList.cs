﻿using System.Collections.Generic;
using UnityEngine;
using RTDW.Weapons;

namespace RTDW.UI
{
	public class WeaponEditorSlotList : MonoBehaviour
	{
		// Data Fields: ---------------------------------------------------------------------------

		private readonly List<WeaponModSlot> modSlotPool = new List<WeaponModSlot>();

		[SerializeField]
		private WeaponModSlot slotPrefab = null;

		[SerializeField]
		private RectTransform slotContainer = null;

		// Primary Functionality: -----------------------------------------------------------------

		/// <summary>
		/// update the list to reflect recursively all the slots within the specified root mod
		/// </summary>
		/// <param name="rootMod">the weapon mod to start counting slots from</param>
		public void UpdateSlotList(WeaponMod rootMod)
		{
			ClearSlotList();

			WeaponMod.Slot[] slots = rootMod.GetAllSlotsRecursively();

			for(int i = 0; i < slots.Length; i++)
			{
				WeaponModSlot uiElem = GetModSlot();
				uiElem.Slot = slots[i];
				uiElem.RefreshSlotVisuals();
			}
		}

		/// <summary> removes all the ui weaponmod slot elements from the container </summary>
		public void ClearSlotList()
		{
			WeaponModSlot[] uiSlots = slotContainer.GetComponentsInChildren<WeaponModSlot>();
			for(int i = uiSlots.Length - 1; i >= 0; i--)
			{
				RemoveModSlot(uiSlots[i]);
			}
		}
		
		// Internal Functionality: ----------------------------------------------------------------

		private WeaponModSlot GetModSlot()
		{
			WeaponModSlot r;
			if (modSlotPool.Count > 0)
			{
				int ind = modSlotPool.Count - 1;
				r = modSlotPool[ind];
				modSlotPool.RemoveAt(ind);
			}
			else
			{
				r = Instantiate(slotPrefab);
			}

			r.gameObject.SetActive(true);
			r.transform.SetParent(slotContainer, false);
			return r;
		}

		private void RemoveModSlot(WeaponModSlot slot)
		{
			slot.gameObject.SetActive(false);
			slot.transform.SetParent(null, false);
			modSlotPool.Add(slot);
		}
	}
}