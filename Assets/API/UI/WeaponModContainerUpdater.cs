using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RTDW.Weapons;

namespace RTDW.UI
{
	public class WeaponModContainerUpdater : MonoBehaviour
	{
		[field: SerializeField]
		public Weapon Weapon { get; private set; } = null;

		[field: SerializeField]
		public RectTransform LayoutContainerTarget { get; private set; } = null;

		[field: SerializeField]
		public WeaponModSlot SlotPrefab { get; private set; } = null;

		private int prevModCount = 0;

		// --------------------------------------------------------------------------------------------

		private List<WeaponModSlot> instantiatedSlots = new List<WeaponModSlot>();

		// --------------------------------------------------------------------------------------------

		private void RefreshModSlots()
		{
			for (int i = instantiatedSlots.Count - 1; i >= 0; i--)
			{
				Destroy(instantiatedSlots[i].gameObject);
			}

			instantiatedSlots.Clear();

			foreach (WeaponMod.Slot modSlot in Weapon.ModSlots)
			{
				WeaponModSlot slot = Instantiate(SlotPrefab);
				slot.Slot = modSlot;
				slot.transform.SetParent(LayoutContainerTarget, true);
				// slot.TargetWeapon = Weapon;
				slot.RefreshSlotVisuals();
				instantiatedSlots.Add(slot);
			}

			prevModCount = Weapon.AppliedMods.Count;
		}

		// --------------------------------------------------------------------------------------------

		private void Start()
		{
			RefreshModSlots();
		}

		private void Update()
		{
			if (Weapon != null && Weapon.AppliedMods.Count != prevModCount)
			{
				RefreshModSlots();
			}
		}
	}
}