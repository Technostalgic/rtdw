﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using RTDW.Weapons;

namespace RTDW.UI
{
	public class WeaponBehaviorSlot : MonoBehaviour
	{
		// Data Fields: ---------------------------------------------------------------------------

		private BehaviorTrait trait = null;

		[SerializeField]
		private Text label = null;

		[SerializeField]
		private Text triggerText = null;

		[SerializeField]
		private Text conditionText = null;

		[SerializeField]
		private Image conditionInvertedCheckmark = null;

		// Exposed Properties: --------------------------------------------------------------------

		public BehaviorTrait Trait => trait;

		// Primary Functionality: -----------------------------------------------------------------

		/// <summary>
		/// Updates the ui of the slot to represent the data in the behavior trait that it 
		/// references
		/// </summary>
		public void UpdateSlotUI()
		{
			label.text = trait.Label;
			conditionText.text = trait.ActivationCondition.condition.ToString();
			conditionInvertedCheckmark.enabled = trait.ActivationCondition.invertCondition;

			bool triggerApplicable = 
				trait.ActivationCondition.condition != 
				BehaviorTrait.ConditionalActivation.ConditionType.Always;
			string trigStr = triggerApplicable ?
				trait.ActivationCondition.trigger.ToString() :
				"N/A";

			triggerText.text = trigStr;
			triggerText.color = triggerApplicable ? Color.white : Color.gray;
		}

		public void SetTrait(BehaviorTrait target)
		{
			trait = target;
			UpdateSlotUI();
		}

		// Auxiliary Functionality: ---------------------------------------------------------------

		public void CycleTriggerType()
		{
			trait.ActivationCondition.trigger =
				(BehaviorTrait.ConditionalActivation.TriggerType)
				((int)(trait.ActivationCondition.trigger + 1) % 3);

			UpdateSlotUI();
		}

		public void CycleCondition()
		{
			trait.ActivationCondition.condition =
				(BehaviorTrait.ConditionalActivation.ConditionType)
				((int)(trait.ActivationCondition.condition + 1) % 5);

			UpdateSlotUI();
		}

		public void ToggleInvertedCondition()
		{
			trait.ActivationCondition.invertCondition = !trait.ActivationCondition.invertCondition;

			UpdateSlotUI();
		}
	}
}