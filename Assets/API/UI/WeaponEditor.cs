﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using RTDW.Weapons;

namespace RTDW.UI
{
	public class WeaponEditor : MonoBehaviour
	{
		// Flag Fields: ---------------------------------------------------------------------------

		private bool weaponDirty = true;

		// Data Fields: ---------------------------------------------------------------------------

		[SerializeField]
		private Weapon targetWeapon = null;

		[SerializeField]
		private Text labelText = null;

		[SerializeField]
		private WeaponEditorSlotList slotList = null;

		[SerializeField]
		private WeaponEditorBehaviorList behaviorList = null;

		[SerializeField]
		private WeaponEditorModTree modTree = null;

		[SerializeField]
		private StatsList statsList = null;

		[SerializeField]
		private WeaponStatusUi statusUi = null;

		// Property Definitions: ------------------------------------------------------------------

		private string WeaponLabel
		{
			get => labelText.text;
			set => labelText.text = value;
		}

		public Weapon TargetWeapon {
			get => targetWeapon;
			set
			{
				targetWeapon = value;
				weaponDirty = true;
			}
		}

		// Primary Functionality: -----------------------------------------------------------------

		/// <summary>
		/// Forces the ui to update to the weapon on the next update call, should be called 
		/// whenever any changes are made to the weapon
		/// </summary>
		public void SetWeaponDirty()
		{
			weaponDirty = true;
		}

		// Internal Functionality: ----------------------------------------------------------------

		private void UpdateEditorUiToWeapon()
		{
			if(TargetWeapon != null)
			{
				WeaponLabel = TargetWeapon.name;
				slotList.UpdateSlotList(TargetWeapon.WeaponModBase);
				behaviorList.UpdateSlotList(TargetWeapon);
				modTree.BuildTreeFromMod(TargetWeapon.WeaponModBase);
				statsList.UpdateUI(TargetWeapon.Stats);
				statusUi.UpdateUiToAllWeaponStatus(targetWeapon);
			}
			else
			{
				WeaponLabel = "No Weapon";
				slotList.ClearSlotList();
				modTree.ClearTree();
			}

			weaponDirty = false;
		}

		private void OnWeaponModTreeChangedCall(WeaponMod.Slot targetSlot)
		{
			UpdateEditorUiToWeapon();
		}

		// Unity Messages: ------------------------------------------------------------------------

		private void Start()
		{
			if(targetWeapon != null)
			{
				targetWeapon.OnModTreeChanged.AddListener(OnWeaponModTreeChangedCall);
			}

			UpdateEditorUiToWeapon();
		}

		private void Update()
		{
			if (weaponDirty) UpdateEditorUiToWeapon();
			statusUi.UpdateUiToDynamicWeaponStatus(TargetWeapon);
		}
	}
}