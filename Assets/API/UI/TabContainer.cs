﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace RTDW.UI
{
	public class TabContainer : MonoBehaviour
	{
		// Data Fields: ---------------------------------------------------------------------------

		[SerializeField]
		private RectTransform tabContent = null;

		[SerializeField]
		private RectTransform tabButtons = null;

		[SerializeField]
		private int selectedTab = 0;

		// Exposed Properties: --------------------------------------------------------------------

		public RectTransform TabContent => tabContent;

		// Primary Functionality: -----------------------------------------------------------------

		public void UpdateToSelectedTab()
		{
			for (int i = tabContent.childCount - 1; i >= 0; i--)
			{
				tabContent.GetChild(i).gameObject.SetActive(i == selectedTab);
			}

			for (int i = tabButtons.childCount - 1; i >= 0; i--)
			{
				Image img = tabButtons.GetChild(i).GetComponent<Image>();
				img.color = i == selectedTab ? Color.white : Color.gray;
			}
		}

		public void SelectTab(int index)
		{
			selectedTab = index;
			UpdateToSelectedTab();
		}
		
		// Internal Functionality: ----------------------------------------------------------------

		private void AttachTabComponents()
		{
			for(int i = tabButtons.childCount - 1; i >= 0; i--)
			{
				RectTransform but = tabButtons.GetChild(i) as RectTransform;
				Button comp = but.gameObject.AddComponent<Button>();
				comp.transition = Selectable.Transition.None;
				
				void onTabPress()
				{
					SelectTab(comp.transform.GetSiblingIndex());
				}

				comp.onClick.AddListener(new UnityAction(onTabPress));
			}
		}

		// Unity Messages: ------------------------------------------------------------------------

		private void Start()
		{
			AttachTabComponents();
			UpdateToSelectedTab();
		}
	}
}