﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RTDW.Weapons;

namespace RTDW.UI
{
	public class WeaponEditorModTree : MonoBehaviour
	{
		// Data Fields: ---------------------------------------------------------------------------

		private readonly List<WeaponModTreeNode> unusedNodes = new List<WeaponModTreeNode>();

		private readonly List<RectTransform> modTreeNodeContainers = new List<RectTransform>();

		private WeaponModTreeNode rootNode = null;

		[SerializeField]
		private RectTransform rootNodeContainer = null;

		[SerializeField]
		private WeaponModTreeNode nodePrefab = null;

		// Primary Functionality: -----------------------------------------------------------------

		/// <summary>
		/// builds and places all the UI nodes that represent each slot which is available that can
		/// contain a part
		/// </summary>
		/// <param name="rootMod">the mod to start building the slot nodes from</param>
		public void BuildTreeFromMod(WeaponMod rootMod)
		{
			if (rootNode != null) ClearTree();

			void recursiveCall(WeaponMod childMod, WeaponModTreeNode childParent)
			{
				int slotCount = childMod.Slots.Count;
				for (int i = 0; i < slotCount; i++)
				{
					WeaponMod.Slot slot = childMod.Slots[i];
					WeaponModTreeNode node = CreateModSlotNode(slot, childParent);

					// position the node child container anchor
					if (slotCount > 1)
					{
						Vector2 pivot = node.ChildContainer.pivot;
						if (i == 0) pivot.x = 0.75f;
						else if (i == slotCount - 1) pivot.x = 0.25f;
						else pivot.x = 0.5f;
						node.ChildContainer.pivot = pivot;
					}
					else if (node.ChildContainer.pivot.x != 0.5)
					{
						node.ChildContainer.pivot = new Vector2(0.5f, 0.5f);
					}

					if(slot.ActiveMod != null)
					{
						recursiveCall(slot.ActiveMod, node);
					}
				}
			}

			rootNode = CreateRootModNode(rootMod);
			rootNode.transform.SetParent(rootNodeContainer);
			recursiveCall(rootMod, rootNode);

			AdjustNodesPositioning();
		}

		public void ClearTree()
		{
			rootNode.Disable(unusedNodes);
			rootNode = null;
		}

		// Internal Functionality: ----------------------------------------------------------------

		private WeaponModTreeNode GetNode()
		{
			WeaponModTreeNode node;

			if (unusedNodes.Count < 1)
			{
				node = Instantiate(nodePrefab);
			}
			else
			{
				int index = unusedNodes.Count - 1;
				node = unusedNodes[index];
				unusedNodes.RemoveAt(index);
			}

			return node;
		}

		private WeaponModTreeNode CreateRootModNode(WeaponMod mod)
		{
			return GetNode().AttachToSlot(WeaponMod.Slot.CreateForMod(mod));
		}

		private WeaponModTreeNode CreateModSlotNode(WeaponMod.Slot slot, WeaponModTreeNode parent)
		{
			WeaponModTreeNode node = GetNode().AttachToSlot(slot);
			parent.AddChild(node);

			if (slot.ActiveMod != null && slot.ActiveMod.Slots.Count > 0)
			{
				node.ChildContainer.gameObject.SetActive(true);
			}
			else node.ChildContainer.gameObject.SetActive(false);

			return node;
		}

		private void AdjustNodesPositioning()
		{
			modTreeNodeContainers.Clear();

			void RecursiveCall(WeaponModTreeNode node)
			{
				if(node.Children.Count > 0)
				{
					PositionNodeChildren(node);
					modTreeNodeContainers.Add(node.ChildContainer);
				}
				foreach(WeaponModTreeNode child in node.Children)
				{
					RecursiveCall(child);
				}
			}

			RecursiveCall(rootNode);
		}

		private void PositionNodeChildren(WeaponModTreeNode parent)
		{
			// adjust size to be the target size
			float childWidth = 64;
			HorizontalLayoutGroup childGroup = parent.ChildContainer.GetComponent<HorizontalLayoutGroup>();
			Vector2 tsize = new Vector2();
			tsize.x = parent.ChildContainer.childCount * childWidth;
			tsize.x += 
				(parent.ChildContainer.childCount - 1) * childGroup.spacing + 
				childGroup.padding.left + childGroup.padding.right;
			tsize.y = parent.ChildContainer.sizeDelta.y;
			parent.ChildContainer.sizeDelta = tsize;

			Rect childsRect = parent.ChildContainer.GetScreenRect();
			
			bool OverlapsOthers()
			{
				// TODO find a way to do this less inefficiently
				Canvas.ForceUpdateCanvases();

				foreach (RectTransform rt in modTreeNodeContainers)
				{
					if (parent.ChildContainer == rt) continue;
					Rect oRect = rt.GetScreenRect();
					if (childsRect.Overlaps(oRect))
					{
						// Debug.LogWarning(
						// 	parent.TargetSlot.SlotType.ToString() +
						// 	" children overlap with " +
						// 	rt.parent.GetComponent<WeaponModTreeNode>().TargetSlot.SlotType.ToString() +
						// 	" children"
						// );

						return true;
					}
				}
				return false;
			}

			while (OverlapsOthers())
			{
				Vector3 pos = parent.ChildContainer.position;
				pos.y -= 64;
				parent.ChildContainer.position = pos;

				// TODO find a way to do this less inefficiently
				Canvas.ForceUpdateCanvases(); 
				
				childsRect = parent.ChildContainer.GetScreenRect();
			}
		}
	}
}