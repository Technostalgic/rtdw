﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using RTDW.Weapons;

namespace RTDW.UI
{
	public class WeaponStatusUi : MonoBehaviour
	{
		[SerializeField]
		private Text projectileValueText = null;

		[SerializeField]
		private Text fireModeValueText = null;

		[SerializeField]
		private Text reloadModeValueText = null;

		[SerializeField]
		private Text DamageTypeValueText = null;

		[SerializeField]
		private Text roundsLeftValueText = null;

		// ----------------------------------------------------------------------------------------

		public void UpdateUiToAllWeaponStatus(Weapon wep)
		{
			projectileValueText.text = wep.SpecialAttributes.ProjectileType.name;
			fireModeValueText.text = wep.SpecialAttributes.FiringMode.ToString();
			reloadModeValueText.text = wep.SpecialAttributes.ReloadMode.ToString();

			DamageData dmgdat = wep.SpecialAttributes.DamageTypeData;
			string dmgTypeStr = "";
			foreach(var dat in dmgdat)
			{
				if(dmgTypeStr.Length > 0) dmgTypeStr += "/";
				dmgTypeStr += dat.Key.name + ":" + dat.Value * 100 + "%";
			}
			
			DamageTypeValueText.text = dmgTypeStr;

			UpdateUiToDynamicWeaponStatus(wep);
		}

		public void UpdateUiToDynamicWeaponStatus(Weapon wep)
		{
			roundsLeftValueText.text = wep.CurrentState.ClipRoundsWhole.ToString();
		}
	}
}