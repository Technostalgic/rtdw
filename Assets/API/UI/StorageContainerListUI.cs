using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace RTDW.UI
{
	public interface IStorageContainerUI
	{
		public StorageContainer ContainerTarget { get; }
	}

	public class StorageContainerListUI : MonoBehaviour, IStorageContainerUI, IPointerClickHandler
	{
		[SerializeField]
		private StackableItem goldAssetReference = null;

		[SerializeField]
		private Text GoldValueText = null;

		// ----------------------------------------------------------------------------------------

		[field: SerializeField]
		public StorageItemUI StorageItemPrefab { get; private set; }

		[field: SerializeField]
		public Transform StorageItemUiContainer { get; private set; }

		[field: SerializeField]
		public Text LabelComponent { get; private set; } = null;

		public string Label
		{
			get => LabelComponent.text;
			set => LabelComponent.text = value;
		}

		[field: SerializeField]
		public StorageContainer ContainerTarget { get; set; } = null;

		private List<StorageItemUI> uiStorageItems = new List<StorageItemUI>();

		// ----------------------------------------------------------------------------------------

		public void OnPointerClick(PointerEventData eventData)
		{
			if (GameUI.CursorItem == null) return;

			if (ContainerTarget.CanAddItem(GameUI.CursorItem))
			{
				ContainerTarget.AddItem(GameUI.CursorItem);
			}
			else return;

			GameUI.RemoveCursorItem();
		}

		// ----------------------------------------------------------------------------------------

		private void OnEnable()
		{
			// fetch all storage items already here
			uiStorageItems.AddRange(StorageItemUiContainer.GetComponentsInChildren<StorageItemUI>());
			RefreshUiItems();

			ContainerTarget.OnItemsModified.AddListener(OnItemsModified);
		}

		private void OnDisable()
		{
			ContainerTarget.OnItemsAdded.RemoveListener(this.OnItemsModified);
			ContainerTarget.OnItemsRemoved.RemoveListener(this.OnItemsModified);
		}

		// ----------------------------------------------------------------------------------------

		private void OnItemsModified(IReadOnlyList<StorageItem> items)
		{
			RefreshUiItems();
		}

		// ----------------------------------------------------------------------------------------

		public StorageItemUI GetNewStorageItemUiInstance()
		{
			StorageItemUI sui = Instantiate(StorageItemPrefab);
			return sui;
		}

		[ContextMenu("Refresh UI Items")]
		public void RefreshUiItems()
		{
			foreach (StorageItemUI sui in uiStorageItems) Destroy(sui.gameObject);
			uiStorageItems.Clear();

			if (ContainerTarget == null) return;

			int goldCount = 0;
			for (int i = 0; i < ContainerTarget.AllStoredItems.Count; i++)
			{
				StorageItem si = ContainerTarget.AllStoredItems[i];

				if(si.ItemTarget is StackableItem stackable && stackable.ItemAsset == goldAssetReference)
				{
					goldCount += stackable.StackSize;
					continue;
				}

				StorageItemUI sui = GetNewStorageItemUiInstance();
	
				sui.StorageItemTarget = si;
				sui.transform.SetParent(StorageItemUiContainer, false);

				uiStorageItems.Add(sui);
				sui.RefreshUiComponents();
			}

			GoldValueText.text = goldCount.ToString();
		}
	}
}