using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using RTDW.Weapons;

namespace RTDW.UI
{
	[AddComponentMenu("RTDW/UI/WeaponModSource")]
	public class WeaponModSource : Selectable, IPointerClickHandler
	{
		[Space(20)]

		[field: SerializeField]
		private WeaponMod weaponMod = null;

		[field: SerializeField]
		public Weapon TargetWeapon { get; set; } = null;

		public WeaponMod WeaponMod
		{
			get => weaponMod;
			set
			{
				Label = value.name;
				weaponMod = value;
			}
		}

		protected string Label
		{
			get => transform.GetChild(0).GetComponent<Text>().text;
			set => transform.GetChild(0).GetComponent<Text>().text = value;
		}

		// ----------------------------------------------------------------------------------------

		public void OnPointerClick(PointerEventData eventData)
		{
			switch (eventData.button)
			{
				case PointerEventData.InputButton.Left:
					if (GameUI.CursorItem == null)
					{
						GameUI.SetCursorItem(WeaponMod.StorageItem);
					}
					else GameUI.RemoveCursorItem();
					break;
				case PointerEventData.InputButton.Right:
					TargetWeapon.RemoveMod(weaponMod, null);
					break;
			}
		}
	}
}