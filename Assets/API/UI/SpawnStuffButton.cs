using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.UI
{
	public class SpawnStuffButton : MonoBehaviour
	{
		public StorageContainerListUI target = null;

		public Weapons.WeaponModBlueprint[] spawnBlueprints = default;

		public int AmountToSpawn = 1;

		public void SpawnStuff()
		{
			if (spawnBlueprints == null || spawnBlueprints.Length <= 0) return;

			for(int i = AmountToSpawn; i > 0; i--)
			{
				int index = Random.Range(0, spawnBlueprints.Length);
				Weapons.WeaponMod mod = spawnBlueprints[index].CreateInstance(Random.value);

				target.ContainerTarget.AddItem(mod.StorageItem);
				target.RefreshUiItems();
			}
		}
	}
}
