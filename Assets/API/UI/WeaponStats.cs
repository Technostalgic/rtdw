using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RTDW.Weapons;

namespace RTDW.UI
{
	public class WeaponStats : MonoBehaviour
	{
		[field: SerializeField]
		public Weapon Weapon { get; private set; } = null;

		[field: SerializeField]
		public Text ClipRoundsDisplay { get; private set; } = null;
		[field: SerializeField]
		public Text ReloadTimeDisplay { get; private set; } = null;
		[field: SerializeField]
		public Text FireWaitDisplay { get; private set; } = null;
		[field: SerializeField]
		public Text CanFireDisplay { get; private set; } = null;

		[field: SerializeField]
		public Text DamageDisplay { get; private set; } = null;
		[field: SerializeField]
		public Text ProjCountDisplay { get; private set; } = null;
		[field: SerializeField]
		public Text FireRateDisplay { get; private set; } = null;
		[field: SerializeField]
		public Text AccuracyDisplay { get; private set; } = null;
		[field: SerializeField]
		public Text ClipSizeDisplay { get; private set; } = null;
		[field: SerializeField]
		public Text ReloadSpdDisplay { get; private set; } = null;
		[field: SerializeField]
		public Text ProjSpdDisplay { get; private set; } = null;

		// --------------------------------------------------------------------------------------------

		private void UpdateDynamicStats()
		{
			ClipRoundsDisplay.text = Weapon.CurrentState.ClipRoundsWhole.ToString();
			ReloadTimeDisplay.text = Weapon.CurrentState.ReloadWait.ToString("0.00");
			FireWaitDisplay.text = Weapon.CurrentState.fireWait.ToString("0.00");

			bool canFire = Weapon.CurrentState.FireWait <= 0 && !Weapon.CurrentState.IsReloading;
			if (Weapon.SpecialAttributes.FiringMode == Weapon.FireModeType.SemiAuto)
				canFire = canFire && Weapon.CurrentState.FireTriggeredTime <= 0;
			CanFireDisplay.text = canFire.ToString();
		}

		private void UpdateStaticStats()
		{
			GunStatSet stats = GameManager.InitParams.GunStats;
			DamageDisplay.text = Weapon.Stats[stats.Damage].Value.ToString("0.00");
			ProjCountDisplay.text = Weapon.Stats[stats.ProjectileCount].Value.ToString("0.00");
			FireRateDisplay.text = Weapon.Stats[stats.FireRate].Value.ToString("0.00");
			AccuracyDisplay.text = Weapon.Stats[stats.Accuracy].Value.ToString("0.00");
			ClipSizeDisplay.text = Weapon.Stats[stats.ClipSize].Value.ToString("0.00");
			ReloadSpdDisplay.text = Weapon.Stats[stats.ReloadSpeed].Value.ToString("0.00");
			ProjSpdDisplay.text = Weapon.Stats[stats.ProjectileSpeed].Value.ToString("0.00");
		}

		// --------------------------------------------------------------------------------------------

		private void Update()
		{
			UpdateDynamicStats();
			UpdateStaticStats();
		}
	}
}