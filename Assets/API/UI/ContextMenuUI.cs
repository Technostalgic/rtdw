﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace RTDW.UI
{
	public class ContextMenuUI : MonoBehaviour
	{
		// Data Fields: ---------------------------------------------------------------------------

		[SerializeField]
		private Text labelText = null;

		[SerializeField]
		private Text descriptionText = null;

		[SerializeField]
		private SimpleTextButton buttonPrefab = null;

		private List<SimpleTextButton> inactiveButtons = new List<SimpleTextButton>();

		// Property Definitions: ------------------------------------------------------------------

		public string LabelText
		{
			get => labelText.text;
			set => labelText.text = value;
		}

		public string DescriptionText
		{
			get => descriptionText.text;
			set => descriptionText.text = value;
		}

		// Primary Funtionality: ------------------------------------------------------------------

		public void OpenStorageItemMenu(
			Vector2 position, StorageItem item, StorageContainer container
		) {
			Open();
			transform.position = position;

			labelText.text = item.Label;
			descriptionText.text = "Actions for " + item.Label;

			SimpleTextButton use = AddButton("Use");
			use.OnSelectAction.AddListener(() => {
				// TODO
				Close();
			});

			SimpleTextButton remove = AddButton("Remove");
			remove.OnSelectAction.AddListener(() => {
				container.RemoveItem(item);
				Close();
			});

			SimpleTextButton cancel = AddButton("Cancel");
			cancel.OnSelectAction.AddListener(() => {
				Close();
			});
		}

		// Auxilary Functionality: ----------------------------------------------------------------

		public void ClearButtons()
		{
			SimpleTextButton[] buttons = GetComponentsInChildren<SimpleTextButton>(false);
			foreach(SimpleTextButton button in buttons)
			{
				button.OnSelectAction.RemoveAllListeners();
				button.gameObject.SetActive(false);
				inactiveButtons.Add(button);
			}
		}

		public SimpleTextButton AddButton(string text)
		{
			SimpleTextButton r = null;
			if(inactiveButtons.Count > 0)
			{
				int ind = inactiveButtons.Count - 1;
				r = inactiveButtons[ind];
				inactiveButtons.RemoveAt(ind);
			}
			else
			{
				r = Instantiate(buttonPrefab);
				r.transform.SetParent(transform, false);
			}

			r.gameObject.SetActive(true);
			r.transform.SetAsLastSibling();
			r.TextLabel = text;

			return r;
		}

		public void Close()
		{
			if (!gameObject.activeSelf) return;

			ClearButtons();
			gameObject.SetActive(false);
		}

		public void Open()
		{
			if (gameObject.activeSelf) Close();
			gameObject.SetActive(true);
		}

		// Unity Messages: ------------------------------------------------------------------------

		private void Awake()
		{
			ClearButtons();
		}
	}
}