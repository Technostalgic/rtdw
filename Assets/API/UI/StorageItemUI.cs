using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using RTDW.Weapons;

namespace RTDW.UI
{
	public class StorageItemUI : Selectable, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
	{
		[field: SerializeField, HideInInspector]
		public StorageItem StorageItemTarget { get; set; } = null;

		public StorageContainer StorageContainerTarget => 
			GetComponentInParent<IStorageContainerUI>()?.ContainerTarget;

		[field: SerializeField]
		public UnityEvent OnSelectAction { get; private set; } = new UnityEvent();

		[field: SerializeField]
		public Text LabelComponent { get; private set; } = null;

		[field: SerializeField]
		public Image IconComponent { get; private set; } = null;

		[field: SerializeField]
		public Image IconBackgroundComponent { get; private set; } = null;

		public string Label
		{
			get => LabelComponent.text;
		}

		// ----------------------------------------------------------------------------------------

		[ContextMenu("Refresh ui components from StorageItem info")]
		public virtual void RefreshUiComponents()
		{
			LabelComponent.text = StorageItemTarget.Label;

			if(StorageItemTarget.ItemTarget is StackableItem stackable)
			{
				LabelComponent.text += " - " + stackable.StackSize;
			}

			IconComponent.sprite = StorageItemTarget.Icon;
			IconBackgroundComponent.sprite = StorageItemTarget.IconBackground;

			IconComponent.color = StorageItemTarget.IconColor;
			IconBackgroundComponent.color = StorageItemTarget.IconBackgroundColor;
		}

		// ----------------------------------------------------------------------------------------

		protected virtual void ItemPrimarySelect()
		{
			if (GameUI.CursorItem == null)
			{
				GameUI.SetCursorItem(StorageItemTarget, StorageContainerTarget);
			}
			GameUI.Instance.ContextMenu.Close();
		}

		protected virtual void ItemAltSelect()
		{
			GameUI.TooltipRef.CloseTooltip();
			GameUI.Instance.ContextMenu.OpenStorageItemMenu(
				transform.position, StorageItemTarget, StorageContainerTarget
			);
		}

		// ----------------------------------------------------------------------------------------

		public override void OnPointerEnter(PointerEventData eventData)
		{
			base.OnPointerEnter(eventData);
			if (StorageItemTarget.ItemTarget is WeaponMod mod)
			{
				GameUI.TooltipRef.OpenModTooltip(eventData.position, mod);
				GameUI.Instance.ContextMenu.Close();
			}
		}

		public override void OnPointerExit(PointerEventData eventData)
		{
			base.OnPointerExit(eventData);
			GameUI.TooltipRef.CloseTooltip();
		}

		public override void OnSelect(BaseEventData eventData)
		{
			base.OnSelect(eventData);
			ItemPrimarySelect();
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			switch (eventData.button)
			{
				case PointerEventData.InputButton.Left: ItemPrimarySelect(); break;
				case PointerEventData.InputButton.Right: ItemAltSelect(); break;
			}
		}
	}
}