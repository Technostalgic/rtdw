using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RTDW.Weapons;

namespace RTDW.UI
{
	public class GameUI : MonoBehaviour
	{
		private static GameUI instance = null;

		public static GameUI Instance
		{
			get
			{
				if (instance == null) instance = FindObjectOfType<GameUI>(true);
				return instance;
			}
		}

		public static StorageContainer CursorContainer { get; private set; } = null;

		private static StorageItem cursorItem = null;
		public static StorageItem CursorItem
		{
			get => cursorItem;
			private set
			{
				cursorIcon.sprite = value?.Icon;
				cursorIcon.color = value?.IconColor ?? Color.white;
				cursorItem = value;
			}
		}

		private static Image cursorIcon = null;

		public static TooltipUI TooltipRef => Instance.Tooltip;

		// ----------------------------------------------------------------------------------------

		/// <summary>
		/// specify the item carried by the cursor and the container from where it came
		/// </summary>
		/// <param name="item"> the item that the cursor is dragging </param>
		/// <param name="container"> the container that the item is inside of, or null if none
		/// 	</param>
		public static void SetCursorItem(StorageItem item, StorageContainer container = null)
		{
			CursorItem = item;
			CursorContainer = container;
		}

		/// <summary> remove the cursor item if it exists </summary>
		public static void RemoveCursorItem()
		{
			CursorItem = null;
			CursorContainer = null;
		}

		// ----------------------------------------------------------------------------------------

		[field: SerializeField]
		public TooltipUI Tooltip { get; private set; } = null;

		[field: SerializeField]
		public ContextMenuUI ContextMenu { get; private set; } = null;

		[field: SerializeField]
		public RectTransform WeaponModMenu { get; private set; } = null;

		[field: SerializeField]
		public ItemShopUI ShopUI { get; private set; } = null;

		// ----------------------------------------------------------------------------------------

		public void SetMenuInputMode(bool active)
		{
			if (active)
			{
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
			}

			else
			{
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}
		}

		public void CloseMenus()
		{
			WeaponModMenu.gameObject.SetActive(false);
			ShopUI.gameObject.SetActive(false);
			SetMenuInputMode(false);
		}

		public void OpenWeaponModMenu()
		{
			SetMenuInputMode(true);
			WeaponModMenu.gameObject.SetActive(true);
		}

		public void OpenItemShopUI(ItemShop targetShop, StorageContainer targetContainer)
		{
			SetMenuInputMode(true);
			ShopUI.OpenShopUI(targetShop, targetContainer);
		}

		// ----------------------------------------------------------------------------------------

		public Sprite GetWeaponSlotSprite(WeaponMod.SlotType slotType)
		{
			switch (slotType)
			{
				case WeaponMod.SlotType.Base: return GameManager.InitParams.ModSlotIcon_Chassis;
				case WeaponMod.SlotType.Barrel: return GameManager.InitParams.ModSlotIcon_Barrel;
				case WeaponMod.SlotType.BarrelMod: return GameManager.InitParams.ModSlotIcon_BarrelMod;
				case WeaponMod.SlotType.Chassis: return GameManager.InitParams.ModSlotIcon_Chassis;
				case WeaponMod.SlotType.ChassisMod: return GameManager.InitParams.ModSlotIcon_ChassisMod;
				case WeaponMod.SlotType.Clip: return GameManager.InitParams.ModSlotIcon_Clip;
				case WeaponMod.SlotType.ClipMod: return GameManager.InitParams.ModSlotIcon_ClipMod;
				case WeaponMod.SlotType.Grip: return GameManager.InitParams.ModSlotIcon_Grip;
				case WeaponMod.SlotType.GripMod: return GameManager.InitParams.ModSlotIcon_GripMod;
				case WeaponMod.SlotType.Sights: return GameManager.InitParams.ModSlotIcon_Sights;
				case WeaponMod.SlotType.SightsMod: return GameManager.InitParams.ModSlotIcon_SightsMod;
				case WeaponMod.SlotType.Chamber: return GameManager.InitParams.ModSlotIcon_Chamber;
				case WeaponMod.SlotType.ChamberMod: return GameManager.InitParams.ModSlotIcon_ChamberMod;
				case WeaponMod.SlotType.Stock: return GameManager.InitParams.ModSlotIcon_Stock;
				case WeaponMod.SlotType.StockMod: return GameManager.InitParams.ModSlotIcon_StockMod;
				case WeaponMod.SlotType.Booster: return GameManager.InitParams.ModSlotIcon_Booster;
			}

			return null;
		}

		private void CreateCursorIcon()
		{
			cursorIcon = new GameObject("CursorItem", typeof(RectTransform)).AddComponent<Image>();
			cursorIcon.rectTransform.SetParent((transform as RectTransform).root, true);
			cursorIcon.rectTransform.pivot = new Vector2(0, 1);
			cursorIcon.rectTransform.anchoredPosition = Vector2.zero;
			cursorIcon.rectTransform.anchorMin = Vector2.zero;
			cursorIcon.rectTransform.anchorMax = Vector2.zero;
			cursorIcon.rectTransform.offsetMax = Vector2.zero;
			cursorIcon.rectTransform.offsetMin = Vector2.zero;
			cursorIcon.rectTransform.sizeDelta = new Vector2(25, 25);
			cursorIcon.gameObject.SetActive(false);
		}

		private void HandleCursorIcon()
		{
			cursorIcon.gameObject.SetActive(CursorItem != null);
			if (cursorIcon.gameObject.activeSelf) cursorIcon.rectTransform.position = Input.mousePosition + new Vector3(20, -20, 0);
		}

		private void HandleDropCursorItem()
		{
			IEnumerator dropAfterFrame()
			{
				yield return new WaitForEndOfFrame();
				if (CursorItem != null) CursorItem = null;
				yield break;
			}

			if (Input.GetMouseButtonUp(1))
			{
				StartCoroutine(dropAfterFrame());
			}
		}

		// ----------------------------------------------------------------------------------------

		private void Start()
		{
			instance = this;
			CreateCursorIcon();
		}

		private void Update()
		{
			HandleCursorIcon();
			HandleDropCursorItem();
		}
	}
}