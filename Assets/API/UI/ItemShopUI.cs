﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace RTDW.UI
{
	public class ItemShopUI : MonoBehaviour
	{
		[SerializeField]
		private StorageContainerListUI targetContainerUI = default;

		[SerializeField]
		private StorageContainerListUI shopInventoryUI = default;

		[SerializeField]
		private Selectable rerollButton = default;

		// ----------------------------------------------------------------------------------------

		public StorageContainerListUI ShopInventoryUI => shopInventoryUI;

		public StorageContainerListUI TargetContainerUI => targetContainerUI;

		public StorageContainer ShopInventory => shopInventoryUI.ContainerTarget;

		public StorageContainer TargetContainer => targetContainerUI.ContainerTarget;

		public ItemShop TargetShop { get; private set; } = null;

		// ----------------------------------------------------------------------------------------

		public void OpenShopUI(ItemShop shop, StorageContainer targetContainer)
		{
			TargetShop = shop;
			targetContainerUI.ContainerTarget = targetContainer;
			targetContainerUI.RefreshUiItems();
			shopInventoryUI.ContainerTarget = shop.ShopInventory;
			shopInventoryUI.RefreshUiItems();
			gameObject.SetActive(true);
			UpdateRerollText();
		}

		public void CloseUI()
		{
			gameObject.SetActive(false);
		}

		public void TryReroll()
		{
			if (targetContainerUI?.ContainerTarget == null) throw new System.Exception(
				"Invalid target container"
			);

			TargetShop.TryRerollInventory(targetContainerUI.ContainerTarget);
			UpdateRerollText();
		}
		
		// ----------------------------------------------------------------------------------------

		private void UpdateRerollText()
		{
			rerollButton.gameObject.SetActive(TargetShop.CanReroll);
			if (!TargetShop.CanReroll) return;

			rerollButton.GetComponentInChildren<Text>().text = "Reroll Shop - " + TargetShop.ShopRerollCost + "g";
		}
	}
}