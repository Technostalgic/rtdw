﻿using UnityEngine;
using UnityEngine.UI;

namespace RTDW.UI
{
	public class ShopItemUI : StorageItemUI
	{
		/// <summary>
		/// The shop ui that the item ui element belongs to
		/// </summary>
		public ItemShopUI ShopParentUI => GetComponentInParent<ItemShopUI>();

		[field: SerializeField]
		public Text ValueLabelComponent { get; private set; } = null;

		// ----------------------------------------------------------------------------------------

		public override void RefreshUiComponents()
		{
			base.RefreshUiComponents();

			ItemShopUI shopUI = ShopParentUI;
			float mult = 1;
			if(shopUI != null)
			{
				if (shopUI.ShopInventory == StorageContainerTarget) mult = shopUI.TargetShop.BuyCostMultiplier;
				else mult = shopUI.TargetShop.SellCostMultiplier;
			}

			int value = StorageItemTarget.GetGoldValue();
			value = (int)(value * mult);

			if (value > 0) ValueLabelComponent.text = value.ToString() + "g";
			else ValueLabelComponent.text = "";
		}

		// ----------------------------------------------------------------------------------------

		protected override void ItemPrimarySelect()
		{
			StorageContainer sc = StorageContainerTarget;
			ItemShopUI parentShopUI = ShopParentUI;

			// if in shop inventory
			if (parentShopUI.ShopInventory == StorageContainerTarget)
			{
				parentShopUI.TargetShop.TryBuyItem(
					parentShopUI.TargetContainer, StorageItemTarget
				);
			}
			
			// if in buyer inventory
			else
			{
				parentShopUI.TargetShop.TrySellItem(
					parentShopUI.TargetContainer, StorageItemTarget
				);
			}
		}

		protected override void ItemAltSelect()
		{

		}
	}
}