using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	[SelectionBase, DisallowMultipleComponent]
	[RequireComponent(typeof(Rigidbody2D))]
	public class Ragdoll : RagdollLimb
	{
		public RagdollLimb RootLimb => this;

		[field: SerializeField, Tooltip("All the physics objects attached to the ragdoll, order DOES matter, it should be iterable from root to leaf in ascending order")]
		private RagdollLimb[] limbs = null;
		public IReadOnlyList<RagdollLimb> Limbs => limbs;

		[field: SerializeField, Tooltip("All joints that are holding the limbs together")]
		private Joint2D[] joints = null;
		public IReadOnlyList<Joint2D> Joints => joints;

		// ----------------------------------------------------------------------------------------

		public static Ragdoll BuildRagdollFromTargetRoot(Transform root)
		{
			Ragdoll ragdoll = new GameObject("Ragdoll").AddComponent<Ragdoll>();

			// foreach (Transform child in ragdoll.transform)
			// {
			// 	if (child.GetComponent<RagdollLimb>() == null) continue;
			// 	if (Application.isPlaying) Destroy(child.gameObject);
			// 	else DestroyImmediate(child.gameObject);
			// }

			// Transform rootLimb = new GameObject(root.name + "_limb").transform;
			// rootLimb.SetParent(ragdoll.transform, false);
			AddLimbComponents(ragdoll.gameObject, root);

			BuildRagdoll_Recursive(root, ragdoll.transform);

			ragdoll.CollectAllLimbs();
			ragdoll.ConnectAllLimbs();

			return ragdoll;
		}

		// ----------------------------------------------------------------------------------------

		private static void BuildRagdoll_Recursive(Transform bone, Transform parentLimb)
		{
			foreach (Transform child in bone)
			{
				if (!EligibleForRagdollLimb(child)) continue;

				Transform limb = new GameObject(child.name + "_limb").transform;
				limb.gameObject.layer = parentLimb.gameObject.layer;
				AddLimbComponents(limb.gameObject, child);

				limb.SetParent(parentLimb, false);
				BuildRagdoll_Recursive(child, limb);
			}
		}

		private static void AddLimbComponents(GameObject limb, Transform source)
		{
			SpriteRenderer childSpriteRend = source.GetComponent<SpriteRenderer>();
			Collider2D childCollider = source.GetComponent<Collider2D>();

			limb.transform.localPosition = source.transform.localPosition;
			limb.transform.localRotation = source.transform.localRotation;
			limb.transform.localScale = source.transform.localScale;

			SpriteRenderer sr = null;
			if (childSpriteRend != null)
			{
				sr = limb.gameObject.AddComponent<SpriteRenderer>();
				sr.CopyComponentValues(childSpriteRend);
			}
			if (childCollider != null)
			{
				Collider2D col = limb.gameObject.AddComponent(childCollider.GetType()) as Collider2D;
				col.CopyComponentValues(childCollider);
				col.offset = childCollider.offset;

				Rigidbody2D rb = limb.GetComponent<Rigidbody2D>();
				if (rb == null) rb = limb.AddComponent<Rigidbody2D>();

				rb.interpolation = RigidbodyInterpolation2D.Interpolate;
				CreateLimb(limb, rb, sr, col);
			}
		}

		// ----------------------------------------------------------------------------------------

		[ContextMenu("Gather Limb GameObjects")]
		public void CollectAllLimbs()
		{
			limbs = null;
			List<RagdollLimb> r = new List<RagdollLimb>();
			List<Joint2D> j = joints != null ? new List<Joint2D>(joints) : new List<Joint2D>();
			CollectAllLimbs_Recursive(RootLimb.transform, r, j);
			joints = j.ToArray();
			limbs = r.ToArray();
		}

		private void CollectAllLimbs_Recursive(Transform startNode, List<RagdollLimb> outList, List<Joint2D> outJoints)
		{
			RagdollLimb limb = startNode.GetComponent<RagdollLimb>();
			if (limb == null) return;

			Joint2D joint = limb.GetComponent<Joint2D>();
			bool condition =
				joint != null &&
				joint.connectedBody == limb.transform.parent.GetComponent<Rigidbody2D>();
			if (condition)
			{
				if (!outJoints.Contains(joint)) outJoints.Add(joint);
			}

			outList.Add(limb);
			foreach (Transform child in startNode)
				CollectAllLimbs_Recursive(child, outList, outJoints);
		}

		[ContextMenu("ConnectLimbs With Basic Joints")]
		private void ConnectAllLimbs()
		{
			List<Joint2D> r = joints != null ? new List<Joint2D>(joints) : new List<Joint2D>();
			for (int i = 0; i < limbs.Length; i++)
			{
				RagdollLimb limb = limbs[i];
				if (limb.transform.parent == null) continue;

				RagdollLimb parentLimb = limb.transform.parent.GetComponent<RagdollLimb>();
				if (parentLimb == null) continue;

				Joint2D prevJoint = limb.GetComponent<Joint2D>();
				if (prevJoint != null && r.Contains(prevJoint)) continue;

				HingeJoint2D joint = limb.gameObject.AddComponent<HingeJoint2D>();
				joint.connectedBody = parentLimb.Rigidbody;
				joint.autoConfigureConnectedAnchor = false;
				joint.connectedAnchor = parentLimb.transform.InverseTransformPoint(limb.transform.position);
				r.Add(joint);
			}
			joints = r.ToArray();
		}
	}
}
