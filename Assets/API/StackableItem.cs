﻿using System.Collections;
using UnityEngine;

namespace RTDW
{
	[CreateAssetMenu(fileName = "StackableItem", menuName = "RTDW/Item Definition")]
	public class StackableItem : ScriptableObject, IStorageItemTarget, IStorageItemValue
	{
		[SerializeField]
		private StorageItem storageItem = null;

		[HideInInspector]
		private int stackSize = 1;

		// ----------------------------------------------------------------------------------------

		public int StackSize
		{
			get => stackSize;
			set
			{
				if (ItemAsset == this) throw new System.Exception(
					"Item asset stack size cannot be modified"
				);
				else stackSize = value;
			}
		}

		// ----------------------------------------------------------------------------------------

		public StorageItem StorageItem => storageItem?.AttachToObject(this);

		public Object Self => this;

		[field: SerializeField, Tooltip("The item asset defintion (should be a self reference)")]
		public StackableItem ItemAsset { get; private set; } = default;

		[field: SerializeField, Tooltip("How much gold each item in the stack is worth")]
		public int goldValue { get; private set; } = 1;

		// ----------------------------------------------------------------------------------------

		public StackableItem Set(StackableItem copyFrom, int count = 1)
		{
			if (ItemAsset != copyFrom.ItemAsset)
			{
				storageItem.CopyFrom(copyFrom.storageItem);
			}
			ItemAsset = copyFrom.ItemAsset;
			stackSize = count;
			return this;
		}

		/// <summary>
		/// creates another instance of the stackable item of the same type with the specified 
		/// amount in the stack
		/// </summary>
		public StackableItem CreateInstance(int count = 1)
		{
			return ObjectPooling.StackableItemPool.GetObject().Set(this, count);
		}

		public int GetGoldValue() => stackSize * goldValue;
	}
}