using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RTDW
{
	[DisallowMultipleComponent]
	public class Killable : MonoBehaviour
	{
		public OnKillEvent OnKill { get; private set; } = new OnKillEvent();

		[field: SerializeField]
		public Corpse Corpse { get; private set; } = null;

		[field: SerializeField]
		public LootDropPool Drops { get; private set; } = null;

		public bool IsKilled { get; private set; } = false;

		// ----------------------------------------------------------------------------------------

		public void Kill()
		{
			IsKilled = true;
			if (Corpse != null) Corpse.CreateCorpseFromEntity(gameObject);
			if (Drops != null) Drops.DropLoot(transform.position, Vector2.zero);
			OnKill.Invoke();
			gameObject.SetActive(false);
		}

		// ----------------------------------------------------------------------------------------

		public class OnKillEvent : UnityEvent { }
	}
}
