﻿using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	[RequireComponent(typeof(StorageContainer))]
	public class ItemShop : MonoBehaviour
	{
		private static StackableItem GoldAsset => GameManager.GoldAsset;

		// ----------------------------------------------------------------------------------------

		[SerializeField]
		private StorageContainer shopInventory = default;

		[SerializeField]
		private Weapons.WeaponModBlueprint[] ModBlueprintPool = default;

		[SerializeField]
		private int inventorySize = 10;

		// Property Definitions: ------------------------------------------------------------------

		public StorageContainer ShopInventory => shopInventory;

		[field: SerializeField]
		public int ShopRerollCost { get; private set; } = 1;

		[field: SerializeField]
		public bool CanReroll { get; private set; } = true;

		[field: SerializeField]
		[field: Tooltip("The multiplier applied to an item's value when calculating it's price to buy from the shop")]
		public float BuyCostMultiplier { get; private set; } = 1.2f;

		[field: SerializeField]
		[field: Tooltip("The multiplier applied to an item's value when calculating it's price to sell to the shop")]
		public float SellCostMultiplier { get; private set; } = 0.5f;

		// Primary Functionality: -----------------------------------------------------------------

		/// <summary>
		/// Opens the shop user interface for trading with the specified storage container
		/// </summary>
		/// <param name="targetInventory">The container that sold items will be put into</param>
		public void OpenShopUI(StorageContainer targetInventory)
		{
			if(shopInventory.TotalItemCount <= 0)
			{
				GenerateShopInventory();
			}
			GameManager.GameUi.OpenItemShopUI(this, targetInventory);
		}

		/// <summary>
		/// Removes all the old items and replaces them with new items
		/// </summary>
		public void GenerateShopInventory()
		{
			shopInventory.ClearAllItems();

			for(int i = inventorySize; i > 0; i--)
			{
				StorageItem itm = CreateItemFromPool(Random.value);
				shopInventory.AddItem(itm);
			}

			GameManager.GameUi.ShopUI.ShopInventoryUI.RefreshUiItems();
		}

		/// <summary>
		/// re-rolls the inventory if the specified container has enough gold, and deducts gold
		/// from the container
		/// </summary>
		/// <param name="goldContainer">the container to search for the gold payment</param>
		public void TryRerollInventory(StorageContainer goldContainer)
		{
			// cancel if not enough gold
			if (!TryDeductExactGoldAmount(goldContainer, ShopRerollCost)) return; 

			GenerateShopInventory();
			int oldCost = ShopRerollCost;
			ShopRerollCost = (int)(ShopRerollCost * 1.5f);
			if (ShopRerollCost == oldCost) ShopRerollCost++;
		}

		/// <summary>
		/// buys the item and places it in the specified container if it has enough gold
		/// </summary>
		/// <param name="buyer">the container that is buying the item</param>
		/// <param name="item">the item that is being bought</param>
		public void TryBuyItem(StorageContainer buyer, StorageItem item)
		{
			// cancel if item is not in shop
			if (!ShopInventory.ContainsItem(item)) return;

			int cost = GetItemBuyCost(item);

			// cancel if not enough gold
			if (!TryDeductExactGoldAmount(buyer, cost)) return;

			// transfer item from shop to buyer
			ShopInventory.AddItem(GoldAsset.CreateInstance(cost).StorageItem);
			ShopInventory.RemoveItem(item);
			buyer.AddItem(item);
		}

		public void TrySellItem(StorageContainer seller, StorageItem item)
		{
			if (!seller.ContainsItem(item)) return;

			int cost = GetItemSellCost(item);

			// cancel if not enough gold
			if (!TryDeductExactGoldAmount(ShopInventory, cost)) return;

			seller.AddItem(GoldAsset.CreateInstance(cost).StorageItem);
			seller.RemoveItem(item);
			ShopInventory.AddItem(item);
		}

		// Auxillary Functionality: ---------------------------------------------------------------

		/// <summary>
		/// used for calling from an interactible script event
		/// </summary>
		public void OnInteract(GameObject interactor)
		{
			StorageContainer container =  interactor.GetComponent<StorageContainer>();
			if (container == null) return;

			OpenShopUI(container);
		}

#if UNITY_EDITOR
		[ContextMenu("Use All Mod Blueprints")]
		private void PopulateBlueprintPool()
		{
			List<Weapons.WeaponModBlueprint> blueprints = new List<Weapons.WeaponModBlueprint>();

			string[] guids = UnityEditor.AssetDatabase.FindAssets("t:WeaponModBlueprint");
			foreach(string guid in guids)
			{
				string path = UnityEditor.AssetDatabase.GUIDToAssetPath(guid);
				Weapons.WeaponModBlueprint blueprint = 
					UnityEditor.AssetDatabase.LoadAssetAtPath<Weapons.WeaponModBlueprint>(path);
				blueprints.Add(blueprint);
			}

			ModBlueprintPool = blueprints.ToArray();
		}
#endif

		// Internal Functionality: ----------------------------------------------------------------

		private bool TryDeductExactGoldAmount(StorageContainer container, int count)
		{
			// no gold
			StackableItem gold = container.GetItemStack(GoldAsset);
			if (gold == null || gold.StackSize < 0) return false;

			// not enough gold
			if (gold.StackSize < count) return false;

			// deduct gold and remove from container
			gold.StackSize -= count;
			if (GoldAsset.StackSize <= 0) container.RemoveItem(gold.StorageItem);
			else container.OnItemsModified.Invoke(gold.StorageItem.ToSingleArray());

			// gold was successfully deducted
			return true;
		}

		private StorageItem CreateItemFromPool(float quality)
		{
			int index = Random.Range(0, ModBlueprintPool.Length);
			return ModBlueprintPool[index].CreateInstance(quality).StorageItem;
		}

		private int GetItemBuyCost(StorageItem item)
		{
			return (int)(StorageItem.CalculateGoldValue(item) * BuyCostMultiplier);
		}

		private int GetItemSellCost(StorageItem item)
		{
			return (int)(StorageItem.CalculateGoldValue(item) * SellCostMultiplier);
		}
	}
}