using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	[RequireComponent(typeof(Interactable))]
	[RequireComponent(typeof(SpriteRenderer))]
	[RequireComponent(typeof(Rigidbody2D))]
	public class ItemPickup : MonoBehaviour
	{
		[SerializeField, HideInInspector]
		private StorageItem item = null;
		public StorageItem Item
		{
			get => item;
			set
			{
				if (item == value) return;
				item = value;
				RefreshGameObjectFromItem();
			}
		}

		private SpriteRenderer spriteRenderer = null;
		public SpriteRenderer SpriteRenderer => spriteRenderer != null ?
			spriteRenderer : spriteRenderer = GetComponent<SpriteRenderer>();

		private Rigidbody2D rigidbody2d = null;
		public Rigidbody2D Rigidbody2d => rigidbody2d != null ?
			rigidbody2d : rigidbody2d = GetComponent<Rigidbody2D>();

		// ----------------------------------------------------------------------------------------

		/// <summary> 
		/// set in editor to call this function in the Interactible.OnInterace event
		/// </summary>
		public void OnInteract(GameObject go)
		{
			if (Item == null)
				throw new System.Exception("ItemPickups should always be associated with an item");

			ItemValidityCheck();

			StorageContainer container = go.GetComponent<StorageContainer>();
			if (container != null)
			{
				if (container.CanAddItem(Item))
				{
					container.AddItem(Item);
					Item = null;
					Remove();
				}
			}
		}

		/// <summary>
		/// Remove the item pickup from the game scene, integrated with object pooling
		/// </summary>
		public void Remove()
		{
			gameObject.SetActive(false);
			ObjectPooling.ItemPickupPool.PoolObject(this);
		}

		// ----------------------------------------------------------------------------------------

		private void ItemValidityCheck()
		{
			// check to make sure if it's a stackable item, that it's not a reference to the item
			// asset which is essentially used as an ID to compare if stackable items are of the
			// same type or not, since we don't want to accidentally modify the asset
			if (item.ItemTarget is StackableItem stackable && stackable == stackable.ItemAsset)
			{
				item = ObjectPooling.StackableItemPool.GetObject().Set(
					stackable, stackable.StackSize
				).StorageItem;
			}
		}

		// ----------------------------------------------------------------------------------------

		[ContextMenu("Refresh Game Object From Item")]
		public void RefreshGameObjectFromItem()
		{
			gameObject.name = "Item - " + (Item?.Label ?? "none");
			SpriteRenderer.sprite = Item?.Icon;
			SpriteRenderer.color = Item?.IconColor ?? Color.white;
		}
	}
}
