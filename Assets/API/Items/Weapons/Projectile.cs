using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Weapons
{
	[DisallowMultipleComponent]
	public class Projectile : MonoBehaviour
	{
		private static readonly List<RaycastHit2D> qHits = new List<RaycastHit2D>();

		private static readonly List<RaycastHit2D> qHits2 = new List<RaycastHit2D>();

		// --------------------------------------------------------------------------------------------

		private static GunStatSet GunStats => GameManager.InitParams.GunStats;
		
		// Data Fields: -------------------------------------------------------------------------------

		[SerializeField]
		private float radius = 0.05f;

		[SerializeField]
		private ContactFilter2D contactFilter = new ContactFilter2D();

		// Property Defintions: -----------------------------------------------------------------------

		[field: SerializeField]
		public bool CollideWithPlatforms { get; private set; } = false;

		[field: SerializeField]
		public SpriteRenderer SpriteRenderer { get; private set; } = null;

		public Vector2 LastPosition { get; private set; } = default;

		public Weapon ParentGun { get; private set; } = null;

		public ContactFilter2D ContactFilter => contactFilter;

		public float Radius
		{
			get => radius;
			set
			{
				if(radius != value)
				{
					radius = value;
					ResizeGraphic();
				}
				else radius = value;
			}
		}

		[field: SerializeField]
		public float MaxLifetime { get; private set; } = 0.5f;

		public float Lifetime { get; private set; } = 0f;

		public float PenetrationPower { get; private set; } = 0f;

		public float DamageMultiplier { get; private set; } = 1f;

		[field: SerializeField]
		public float GravityScale { get; private set; } = 0.5f;

		[field: SerializeField]
		public float Impulse { get; private set; } = 0.1f;

		[field: SerializeField, Tooltip("How much the speed of the projectile effects the impact force")]
		public float ImpulseSpeedFactor { get; private set; } = 1f;

		[field: SerializeField]
		public GameObject HitEffect { get; set; } = null;

		public Vector2 Velocity { get; set; } = default;

		public bool IsDead { get; private set; } = default;

		// Primary Functionality: ---------------------------------------------------------------------

		public void LaunchFrom(Weapon wep, float spread)
		{
			transform.position = wep.ProjectileSpawnPoint.position;

			LastPosition = transform.position;
			Radius = ParentGun.Stats[GunStats.ProjectileSize].FunctionalValue * 0.5f;

			float speed = wep.Stats[GunStats.ProjectileSpeed].FunctionalValue;
			float speedVar = wep.Stats[GunStats.ProjectileSpeedGrouping].FunctionalValue;

			float ang = Mathf.Atan2(wep.ProjectileSpawnPoint.right.y, wep.ProjectileSpawnPoint.right.x);
			ang += wep.AimRecoil.AdjustedOffset;
			ang += UnityEngine.Random.Range(-spread, spread);

			float tspeed = speed + speed * UnityEngine.Random.Range(-speedVar, 0);
			Vector2 vel = new Vector2(Mathf.Cos(ang) * tspeed, Mathf.Sin(ang) * tspeed);

			Velocity = vel;
		}

		// Internal Functionality: --------------------------------------------------------------------

		protected virtual void ResizeGraphic()
		{
			if (SpriteRenderer == null || SpriteRenderer.sprite == null) return;
			float sprheight = SpriteRenderer.sprite.rect.height / SpriteRenderer.sprite.pixelsPerUnit;
			float targetSize = (Radius * 2) / sprheight;
			SpriteRenderer.transform.localScale = new Vector3(targetSize, targetSize, 1);
		}

		private void HandleDeltaPosition()
		{
			transform.position += (Vector3)Velocity * Time.deltaTime;

			qHits.Clear();
			Vector2 dif = (Vector2)transform.position - LastPosition;
			Physics2D.CircleCast(LastPosition, Radius, dif, contactFilter, qHits, dif.magnitude);

			// ensure no collisions are skipped over due to dynamic rigidbodies moving over the
			// projectile's lastPosition
			if(qHits.Count <= 0)
			{
				Collider2D col = Physics2D.OverlapCircle(LastPosition, Radius, contactFilter.layerMask);
				if (col != null)
				{
					qHits2.Clear();
					Vector2 origin = LastPosition - Velocity * 5;
					col.CircleCastAgainstCollider(qHits2, origin, radius, Velocity, Velocity.magnitude * 5);
					foreach(RaycastHit2D hit in qHits2) qHits.Add(hit);
				}
			}

			HandleProjectileRayHits(qHits);
		}

		private void HandlePhysics()
		{
			Velocity += Physics2D.gravity * GravityScale * Time.deltaTime;
		}

		private void HandleProjectileRayHits(IReadOnlyList<RaycastHit2D> hits)
		{
			if (hits.Count <= 0) return;

			HashSet<int> usedIndices = new HashSet<int>();
			int closestIndex = 0;
			do
			{
				for (int i = hits.Count - 1; i >= 0; i--)
				{
					if (usedIndices.Contains(i)) continue;
					if (closestIndex < 0) closestIndex = i;
					if (hits[closestIndex].distance > hits[i].distance) closestIndex = i;
				}

				usedIndices.Add(closestIndex);
				RaycastHit2D hit = hits[closestIndex];
				closestIndex = -1;

				if (hit.collider.usedByEffector)
				{
					PlatformEffector2D platform = hit.collider.GetComponent<PlatformEffector2D>();
					if (!CollideWithPlatforms) continue;
					if (platform != null)
					{
						float surfDif = Vector2.Angle(hit.normal, platform.transform.up);
						if (surfDif > platform.surfaceArc * 0.5) continue;
					}
				}
				HandleCollision(hit);
			} while (!IsDead && usedIndices.Count < hits.Count);
		}

		public Projectile SetPropertiesFromParentGun(Weapon gun)
		{
			if (ParentGun != null)
			{
				Debug.LogError("Parent Gun is already set");
				return null;
			}
			ParentGun = gun;

			return this;
		}

		public void Teleport(Vector2 position)
		{
			transform.position = position;
			LastPosition = position;
		}

		// Internal Physics: --------------------------------------------------------------------------

		protected virtual void HandleCollision(RaycastHit2D rayHit)
		{
			PenetrationPower -= 1;
			if (PenetrationPower <= 0)
			{
				Destroy(gameObject);
				IsDead = true;
			}

			GameObject hitObj = rayHit.collider.gameObject;
			ProjectileTarget pt = hitObj.GetComponent<ProjectileTarget>();
			IProjectileListener[] pls1 = hitObj.GetComponents<IProjectileListener>();

			GameObject hitfx = Instantiate(HitEffect);
			hitfx.transform.position = rayHit.point;

			Rigidbody2D rb = rayHit.collider.attachedRigidbody;
			if (rb != null)
			{
				HitRigidbody(rayHit, rb);
				hitObj = rb.gameObject;
			}

			if (pt != null) pt.OnHit.Invoke(rayHit, this);
			foreach (IProjectileListener lis in pls1) lis.OnProjectileHit(rayHit, this);

			IProjectileListener[] pls2 = hitObj.GetComponents<IProjectileListener>();
			foreach (IProjectileListener lis in pls2) lis.OnProjectileHit(rayHit, this);
		}

		protected virtual void HitRigidbody(RaycastHit2D rayHit, Rigidbody2D rb)
		{
			Vector2 impulse = GetImpulseForHit(rayHit, rb.velocity);
			rb.AddForceAtPositionCapped(impulse, rayHit.point, ForceMode2D.Impulse, 50, 10000);
		}

		public virtual Vector2 GetImpulseForHit(RaycastHit2D hit, Vector2 otherVelocity)
		{
			float impulseMag = Impulse;
			if (ImpulseSpeedFactor > 0)
			{
				Vector2 dvel = Velocity - otherVelocity;
				float mag = Vector2.Dot(Velocity, dvel);
				impulseMag += ImpulseSpeedFactor * (mag > 1 ? Mathf.Log10(mag) : 0);
			}

			Vector2 impulse = new Vector2(Velocity.x * impulseMag, Velocity.y * impulseMag);
			return impulse;
		}

		// Unity Messages: ----------------------------------------------------------------------------

		private void OnValidate()
		{
			ResizeGraphic();
		}

		private void Start()
		{
			ResizeGraphic();
		}

		private void Update()
		{
			HandleDeltaPosition();
			HandlePhysics();

			LastPosition = transform.position;

			if (Lifetime >= MaxLifetime) Destroy(gameObject);

			else Lifetime += Time.deltaTime;
		}
	}
}