using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RTDW.Weapons
{
	/// <summary>
	/// A component that is used for polling projectile hits on a specific game object
	/// </summary>
	[RequireComponent(typeof(Collider2D))]
	public class ProjectileTarget : MonoBehaviour
	{
		// [
		// 	field: SerializeField, Tooltip("Whether or not the other components that implement " +
		// 	"IProjectileListener will be triggered when this component receives a " +
		// 	"projectile hit")
		// ]
		// public bool AutoAttachProjectileListeners { get; set; } = true;

		// [
		// 	field: SerializeField, Tooltip(
		// 	"If true, the OnHit event will invoke find the parent ProjectileTarget component " +
		// 	"and invoke that one automatically")
		// ]
		// public bool AutoAttachToParent { get; set; } = false;

		[field: SerializeField]
		public ProjectileHitEvent OnHit { get; private set; } = new ProjectileHitEvent();

		private HashSet<UnityAction<RaycastHit2D, Projectile>> listenersFound =
			new HashSet<UnityAction<RaycastHit2D, Projectile>>();

		// ----------------------------------------------------------------------------------------

		private void FindAndAttachParent()
		{
			ProjectileTarget parentTarg = transform.parent?.GetComponent<ProjectileTarget>();
			if (parentTarg == null) return;

			parentTarg.OnHit.AddListener(OnHit.Invoke);
		}

		private void AttachProjectileListeners()
		{
			var listeners = GetComponents<IProjectileListener>();
			foreach (var listen in listeners)
			{
				if (listenersFound.Contains(listen.OnProjectileHit)) continue;
				OnHit.AddListener(listen.OnProjectileHit);
				listenersFound.Add(listen.OnProjectileHit);
			}
		}

		// ----------------------------------------------------------------------------------------

		// private void Start()
		// {
		// 	if (AutoAttachProjectileListeners) AttachProjectileListeners();
		// 	if (AutoAttachToParent) FindAndAttachParent();
		// }
	}

	[System.Serializable]
	public class ProjectileHitEvent : UnityEvent<RaycastHit2D, Projectile> { }

	public interface IProjectileListener
	{
		void OnProjectileHit(RaycastHit2D hit, Projectile proj);
	}
}
