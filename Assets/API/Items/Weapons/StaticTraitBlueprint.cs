﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Weapons
{
	[CreateAssetMenu(fileName = "StatTraitBlueprint", menuName = "RTDW/WeaponMods/Stat Trait Blueprint")]
	public class StaticTraitBlueprint : WeaponModTraitBlueprint
	{
		// Temporary Query Variables: -------------------------------------------------------------

		private static readonly List<Stat.Modifier> qModList = new List<Stat.Modifier>();

		private static readonly List<WeaponAttribute> qAttrs = new List<WeaponAttribute>();

		// ----------------------------------------------------------------------------------------

		[field: SerializeField]
		public WeaponRequirementStatic StaticRequirements { get; private set; } = default;

		[field: SerializeField]
		public WeaponAttributeBlueprint[] WeaponAttributes { get; private set; } = default;

		[Space]

		[SerializeField, Tooltip("The stats that will be affected by this trait")]
		private StatRange[] statRanges;

		public IReadOnlyList<StatRange> StatRanges => statRanges;

		// ----------------------------------------------------------------------------------------

		public override WeaponModTrait CreateInstance(float effectFactor)
		{
			qModList.Clear();
			foreach (StatRange sr in statRanges)
			{
				Stat.Modifier mod = new Stat.Modifier(
					sr.stat,
					sr.addModifier.Evaluate(effectFactor),
					sr.multModifier.Evaluate(effectFactor)
				);
				qModList.Add(mod);
			}

			qAttrs.Clear();
			foreach (var bp in WeaponAttributes)
			{
				qAttrs.Add(bp.CreateInstance());
			}

			StaticTrait r = new StaticTrait(
				StaticRequirements, qAttrs.ToArray(),
				qModList.ToArray()
			);
			r.Label = Label;

			return r;
		}

		// ----------------------------------------------------------------------------------------

		[Serializable]
		public class StatRange
		{
			[Tooltip("the stat that this stat range will modify")]
			public Stat stat = default;

			[Tooltip("the function to determine the addition modifier applied to the stat")]
			public AnimationCurve addModifier = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 0));

			[Tooltip("the function to determine the multiplier modifier applied to the stat")]
			public AnimationCurve multModifier = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 0));
		}
	}
}