﻿using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Weapons
{
	[System.Serializable]
	public class WeaponAttributeSet
	{
		// Temporary Query Variables: -------------------------------------------------------------

		private static readonly List<WeaponAttribute.FiringMode> qFireModes = 
			new List<WeaponAttribute.FiringMode>();

		private static readonly List<WeaponAttribute.ReloadingMode> qReloadModes = 
			new List<WeaponAttribute.ReloadingMode>();

		private static readonly List<WeaponAttribute.ProjectileTypeSpec> qProjTypes = 
			new List<WeaponAttribute.ProjectileTypeSpec>();

		private static readonly List<WeaponAttribute.DamageTypeSpec> qDamageTypes = 
			new List<WeaponAttribute.DamageTypeSpec>();

		// Flag Fields: ---------------------------------------------------------------------------

		private bool firingModeDirty = true;

		private bool reloadingModeDirty = true;

		private bool projectileTypeDirty = true;

		private bool damageTypeMultiplierDirty = true;

		// Data Fields: ---------------------------------------------------------------------------

		[SerializeField]
		private WeaponAttribute.FiringMode defaultFiringMode;

		[SerializeField]
		private WeaponAttribute.ReloadingMode defaultReloadMode;

		[SerializeField]
		private WeaponAttribute.ProjectileTypeSpec defaultProjectileType;

		[SerializeField]
		private WeaponAttribute.DamageTypeSpec defaultDamageType;

		private Weapon.FireModeType firingMode = default;

		private Weapon.ReloadModeType reloadMode = default;

		private Projectile projectileType = default;

		private DamageData damageTypeData = default;

		private WeaponAttribute.FiringMode[] firingModeAttrs = default;

		private WeaponAttribute.ReloadingMode[] reloadingModeAttrs = default;

		private WeaponAttribute.ProjectileTypeSpec[] projectileTypeAttrs = default;

		private WeaponAttribute.DamageTypeSpec[] damageTypeAttrs = default;

		// Property Definitions: ------------------------------------------------------------------

		public IReadOnlyList<WeaponAttribute.FiringMode> FiringModes =>
			firingModeAttrs;

		public IReadOnlyList<WeaponAttribute.ReloadingMode> ReloadingModes =>
			reloadingModeAttrs;

		public IReadOnlyList<WeaponAttribute.ProjectileTypeSpec> ProjectileTypes =>
			projectileTypeAttrs;

		public IReadOnlyList<WeaponAttribute.DamageTypeSpec> DamageTypes =>
			damageTypeAttrs;

		public Weapon.FireModeType DefaultFiringMode => defaultFiringMode.FiringType;

		public Weapon.ReloadModeType DefaultReloadMode => defaultReloadMode.ReloadType;

		public Projectile DefaultProjectileType => defaultProjectileType.ProjectileType;

		public DamageType DefaultDamageType => defaultDamageType.Type;

		public Weapon.FireModeType FiringMode
		{
			get
			{
				if (firingModeDirty) RecalculateFiringMode();
				return firingMode;
			}
		}

		public Weapon.ReloadModeType ReloadMode
		{
			get
			{
				if (reloadingModeDirty) RecalculateReloadMode();
				return reloadMode;
			}
		}

		public Projectile ProjectileType
		{
			get
			{
				if (projectileTypeDirty) RecalculateProjectileType();
				return projectileType;
			}
		}

		public DamageData DamageTypeData
		{
			get
			{
				if (damageTypeMultiplierDirty) RecalculateDamageTypeData();
				return damageTypeData;
			}
		}

		// ----------------------------------------------------------------------------------------

		private void RecalculateFiringMode()
		{
			float prec = defaultFiringMode.Precedence;
			var val = DefaultFiringMode;
			if (firingModeAttrs != null) foreach (var attr in firingModeAttrs)
				{
					if (attr.Precedence > prec)
					{
						val = attr.FiringType;
						prec = attr.Precedence;
					}
				}
			firingMode = val;
		}

		private void RecalculateReloadMode()
		{
			float prec = defaultReloadMode.Precedence;
			var val = DefaultReloadMode;
			if (reloadingModeAttrs != null) foreach (var attr in reloadingModeAttrs)
				{
					if (attr.Precedence > prec)
					{
						val = attr.ReloadType;
						prec = attr.Precedence;
					}
				}
			reloadMode = val;
		}

		private void RecalculateProjectileType()
		{
			float prec = defaultProjectileType.Precedence;
			var val = DefaultProjectileType;
			if (projectileTypeAttrs != null) foreach (var attr in projectileTypeAttrs)
				{
					if (attr.Precedence > prec)
					{
						val = attr.ProjectileType;
						prec = attr.Precedence;
					}
				}
			projectileType = val;
		}

		private void RecalculateDamageTypeData()
		{
			float totalPrecedence = defaultDamageType.Precedence;
			damageTypeData = new DamageData();
			if (damageTypeAttrs != null) foreach (var attr in damageTypeAttrs)
				{
					damageTypeData[attr.Type] = attr.Precedence;
					totalPrecedence += attr.Precedence;
				}

			if (totalPrecedence > 0) damageTypeData.MultiplyScalar(1 / totalPrecedence);
			else
			{
				damageTypeData[DefaultDamageType] = 1;
			}
		}

		private void SetDefaults(
			Weapon.FireModeType defaultFireMode,
			Weapon.ReloadModeType defaultReloadMode,
			Projectile defaultProjectile, DamageType defaultDamageType
		)
		{
			firingModeAttrs = new WeaponAttribute.FiringMode[]
			{
				new WeaponAttribute.FiringMode(defaultFireMode)
			};

			reloadingModeAttrs = new WeaponAttribute.ReloadingMode[]
			{
				new WeaponAttribute.ReloadingMode(defaultReloadMode)
			};

			projectileTypeAttrs = new WeaponAttribute.ProjectileTypeSpec[]
			{
				new WeaponAttribute.ProjectileTypeSpec(defaultProjectile)
			};

			damageTypeAttrs = new WeaponAttribute.DamageTypeSpec[]
			{
				new WeaponAttribute.DamageTypeSpec(defaultDamageType)
			};
		}

		// ----------------------------------------------------------------------------------------

		/// <summary>
		/// clear all the attributes from the set, so that only the default ones remain
		/// </summary>
		public void ClearAttributes()
		{
			firingModeAttrs = new WeaponAttribute.FiringMode[] { };
			reloadingModeAttrs = new WeaponAttribute.ReloadingMode[] { };
			projectileTypeAttrs = new WeaponAttribute.ProjectileTypeSpec[] { };
			damageTypeAttrs = new WeaponAttribute.DamageTypeSpec[] { };
			firingModeDirty = true;
			reloadingModeDirty = true;
			projectileTypeDirty = true;
			damageTypeMultiplierDirty = true;
		}

		public void ConcatAttributes(params WeaponAttribute[] attrs)
		{
			WeaponAttribute[] testArr = null;
			foreach (WeaponAttribute attr in attrs)
			{
				if (attr.Precedence == 0) continue;
				switch (attr)
				{
					case WeaponAttribute.FiringMode t:
						testArr = firingModeAttrs;
						break;
					case WeaponAttribute.ReloadingMode t:
						testArr = reloadingModeAttrs;
						break;
					case WeaponAttribute.ProjectileTypeSpec t:
						testArr = projectileTypeAttrs;
						break;
					case WeaponAttribute.DamageTypeSpec t:
						testArr = damageTypeAttrs;
						break;
				}

				bool didAppend = false;
				if (testArr != null) foreach (var test in testArr)
				{
					if (attr.HasSameArgument(test))
					{
						test.Precedence += attr.Precedence;
						didAppend = true;
						break;
					}
				}
				if (!didAppend)
				{
					switch (attr)
					{
						case WeaponAttribute.FiringMode t:
							firingModeDirty = true;
							qFireModes.Clear();
							if(testArr != null)
								qFireModes.AddRange(testArr as WeaponAttribute.FiringMode[]);
							qFireModes.Add(t);
							firingModeAttrs = qFireModes.ToArray();
							break;
						case WeaponAttribute.ReloadingMode t:
							reloadingModeDirty = true;
							qReloadModes.Clear();
							if (testArr != null)
								qReloadModes.AddRange(testArr as WeaponAttribute.ReloadingMode[]);
							qReloadModes.Add(t);
							reloadingModeAttrs = qReloadModes.ToArray();
							break;
						case WeaponAttribute.ProjectileTypeSpec t:
							projectileTypeDirty = true;
							qProjTypes.Clear();
							if (testArr != null)
								qProjTypes.AddRange(testArr as WeaponAttribute.ProjectileTypeSpec[]);
							qProjTypes.Add(t);
							projectileTypeAttrs = qProjTypes.ToArray();
							break;
						case WeaponAttribute.DamageTypeSpec t:
							qDamageTypes.Clear();
							if (testArr != null)
								qDamageTypes.AddRange(testArr as WeaponAttribute.DamageTypeSpec[]);
							qDamageTypes.Add(t);
							damageTypeAttrs = qDamageTypes.ToArray();
							break;
					}
				}
			}
		}
	}
}