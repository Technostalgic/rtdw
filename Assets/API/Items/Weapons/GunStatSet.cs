using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Weapons
{
	[CreateAssetMenu(fileName = "GunStats", menuName = "RTDW/Stats/GunStats")]
	public class GunStatSet : StatSet
	{
		[field: SerializeField]
		public Stat Damage { get; private set; }

		[field: SerializeField]
		public Stat ProjectileSpeed { get; private set; }

		[field: SerializeField]
		public Stat ProjectileSpeedGrouping { get; private set; }

		[field: SerializeField]
		public Stat ProjectileCount { get; private set; }

		[field: SerializeField]
		public Stat ProjectileSize { get; private set; }

		[field: SerializeField]
		public Stat Accuracy { get; private set; }

		[field: SerializeField]
		public Stat FireRate { get; private set; }

		[field: SerializeField]
		public Stat Maneuverability { get; private set; }

		[field: SerializeField]
		public Stat RecoilSteadiness { get; private set; }

		[field: SerializeField]
		public Stat ClipSize { get; private set; }

		[field: SerializeField]
		public Stat ReloadSpeed { get; private set; }

		[field: SerializeField]
		public Stat EnergyCapacity { get; private set; }

		// ----------------------------------------------------------------------------------------

		private Stat[] stats = null;

		public override IReadOnlyCollection<Stat> AllStats
		{
			get
			{
				if (stats == null || stats.Length <= 0)
				{
					stats = new Stat[] {
						Damage, ProjectileSpeed, ProjectileSpeedGrouping, ProjectileCount,
						ProjectileSize, Accuracy, FireRate, Maneuverability, RecoilSteadiness,
						ClipSize, ReloadSpeed, EnergyCapacity
					};
				}
				return stats;
			}
		}
	}
}