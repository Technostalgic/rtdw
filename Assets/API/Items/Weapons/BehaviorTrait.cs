﻿using System;
using System.Collections;
using UnityEngine;

namespace RTDW.Weapons
{
	[Serializable]
	public class BehaviorTrait : WeaponModTrait
	{
		public BehaviorTrait(
			// BehaviorType type, BehaviorOverlapType overlap, float precedence,
			string description,
			WeaponRequirementStatic reqStatic, WeaponRequirementDynamic reqDynamic,
			WeaponAttribute[] attrs, ConditionalActivation condition,
			bool boolVal, int intVal, float floatVal, GunUpdateBehavior update,
			GunActionBehavior fireTriggerStart, GunActionBehavior fireTriggerEnd,
			GunActionBehavior reloadStart, GunFireBehavior projectilesFired
		)
		{
			Description = description;

			StaticRequirements.ConcatRequirements(reqStatic);
			DynamicRequirements = reqDynamic;
			attributes = attrs;

			// Type = type;
			// OverlapBehavior = overlap;
			// Precedence = precedence;
			ActivationCondition = condition;
			boolValue = boolVal;
			intValue = intVal;
			floatValue = floatVal;

			OnUpdate = update;
			OnTriggerStart = fireTriggerStart;
			OnTriggerEnd = fireTriggerEnd;
			OnReloadEnd = reloadStart;
			OnProjectilesFired = projectilesFired;
		}

		// ----------------------------------------------------------------------------------------

		public WeaponRequirementDynamic DynamicRequirements { get; protected set; }
			= new WeaponRequirementDynamic();


		[field: SerializeField]
		public string Description { get; protected set; } = "Trait Description";

		// public BehaviorType Type { get; private set; } = BehaviorType.Misc;

		// public BehaviorOverlapType OverlapBehavior { get; private set; } = BehaviorOverlapType.Flexible;

		// public float Precedence { get; private set; } = 0;

		[Space]

		// Parameters for Behaviors: --------------------------------------------------------------


		public ConditionalActivation ActivationCondition = null;

		[Tooltip("Used as a parameter to modify event behaviors")]
		public bool boolValue = false;

		[Tooltip("Used as a parameter to modify event behaviors")]
		public int intValue = 0;

		[Tooltip("Used as a parameter to modify event behaviors")]
		public float floatValue = 0f;

		/// <summary>
		/// persistent data opbject so that a behavior state can keep track of any information 
		/// that it needs to
		/// </summary>
		[HideInInspector]
		public object persistentData = null;

		// Behavior Events: -----------------------------------------------------------------------

		public GunUpdateBehavior OnUpdate { get; private set; } = null;

		public GunActionBehavior OnTriggerStart { get; private set; } = null;

		public GunActionBehavior OnTriggerEnd { get; private set; } = null;

		public GunActionBehavior OnReloadEnd { get; private set; } = null;

		public GunFireBehavior OnProjectilesFired { get; private set; } = null;

		// Subtype Defintions: --------------------------------------------------------------------

		public enum BehaviorType
		{
			Misc,
			FiringSequence,
			ReloadSequence
		}

		/// <summary>
		/// determines if the behaviors can overlap or interact with other behaviors
		/// </summary>
		public enum BehaviorOverlapType
		{
			/// <summary>
			/// flexible - is not effected by any other behaviors
			/// </summary>
			Flexible,

			/// <summary>
			/// combine - stacks with other behaviors of the same type
			/// </summary>
			Combine,

			/// <summary>
			/// picky - only allows one behavior of this type, the behavior with highest 
			/// precedence is picked
			/// </summary>
			Picky
		}

		/// <summary> determines the condition of when the behavior is active </summary>
		[Serializable]
		public class ConditionalActivation
		{
			public ConditionType condition = ConditionType.Always;

			public bool invertCondition = false;

			public TriggerType trigger = TriggerType.AltTrigger;

			private bool isToggled = true;

			// ---------------------------------------------------------------------------------------------

			public bool IsActive(Weapon wep)
			{
				bool active = false;

				switch (condition)
				{
					case ConditionType.Always: active = true; break;
					case ConditionType.Toggle: active = isToggled; break;
					case ConditionType.TriggerHeld:
						if(trigger == TriggerType.PrimaryTrigger) active = wep.CurrentState.fireTriggered;
						else if(trigger == TriggerType.AltTrigger) active = wep.CurrentState.altTriggered;
						else if(trigger == TriggerType.Reload) active = wep.CurrentState.IsReloading;
						break;
					case ConditionType.TriggerStart:
					case ConditionType.TriggerEnd:
						active = wep.EvaluateBehaviorCondition(this);
						break;
				}

				if (invertCondition) active = !active;
				return active;
			}

			public ConditionalActivation Clone()
			{
				ConditionalActivation r = new ConditionalActivation();

				r.condition = condition;
				r.invertCondition = invertCondition;
				r.trigger = trigger;
				r.isToggled = isToggled;

				return r;
			}

			// ---------------------------------------------------------------------------------------------

			public enum ConditionType
			{
				Always,
				Toggle,
				TriggerHeld,
				TriggerStart,
				TriggerEnd
			}

			public enum TriggerType
			{
				AltTrigger,
				PrimaryTrigger,
				Reload
			}
		}
	}
}