using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RTDW.Weapons
{
	[SelectionBase]
	[RequireComponent(typeof(EntityStats))]
	public class Weapon : MonoBehaviour, IStorageItemTarget
	{
		// Static Shortcuts: ----------------------------------------------------------------------

		private static GunStatSet GunStats => GameManager.InitParams.GunStats;

		// Temporary Query Variables: -------------------------------------------------------------

		private static readonly HashSet<WeaponMod.Slot> qModSlotSet = new HashSet<WeaponMod.Slot>();

		private static readonly List<Projectile> qProjList = new List<Projectile>();

		private static readonly Dictionary<BehaviorTrait.BehaviorType, HashSet<BehaviorTrait>>
			qBehaviorSort = new Dictionary<BehaviorTrait.BehaviorType, HashSet<BehaviorTrait>>();

		private static readonly List<WeaponMod> qModList = new List<WeaponMod>();

		// Events: --------------------------------------------------------------------------------

		public readonly UnityEvent<WeaponMod.Slot> OnModTreeChanged = 
			new UnityEvent<WeaponMod.Slot>();

		// Flag Fields: ---------------------------------------------------------------------------

		private bool requirementsMetDirty = true;

		private bool allBehaviorTraitsDirty = true;

		private Coroutine currentRoutine = null;

		private bool modSlotsDirty = true;

		private bool modTreeDirty = true;

		// Data Fields-----------------------------------------------------------------------------

		[SerializeField]
		private StorageItem storageItem = null;

		private Transform positionalTarget = null;

		private bool requirementsMet = false;

		private State currentState = default;

		private HashSet<WeaponMod> appliedMods = new HashSet<WeaponMod>(); 

		private HashSet<BehaviorTrait> allBehaviorTraits = new HashSet<BehaviorTrait>();

		private WeaponMod.Slot[] modSlots = null;

		[field: SerializeField, Tooltip("The weapon base where other parts can be attached to")]
		private WeaponMod weaponModBase = null;

		private readonly HashSet<Projectile> trackedProjectiles = new HashSet<Projectile>();

		private readonly Dictionary<BehaviorTrait, BehaviorStatePropertyBase> behaviorStateData
			= new Dictionary<BehaviorTrait, BehaviorStatePropertyBase>();

		private Transform primaryTarget = null;

		private EntityStats stats = null;

		// Exposed Properties: --------------------------------------------------------------------

		#region Exposed Properties

		public StorageItem StorageItem => storageItem.AttachToObject(this);

		public UnityEngine.Object Self => this;

		[field: SerializeField, Tooltip("The point at which the projectile will spawn")]
		public Transform ProjectileSpawnPoint { get; private set; } = null;

		/// <summary> The "root node" of the weapon mod tree </summary>
		public WeaponMod WeaponModBase {
			get => weaponModBase; 
			private set
			{
				if (weaponModBase == value) return;
				if(weaponModBase != null)
				{
					weaponModBase.OnModTreeChanged.RemoveListener(OnModTreeChangedCall);
				}

				weaponModBase = value;
				if (weaponModBase != null)
				{
					weaponModBase.OnModTreeChanged.AddListener(OnModTreeChangedCall);
				}
			}
		}

		/// <summary>
		/// the projectiles being tracked by the weapon that behavior traits can reference
		/// </summary>
		public ICollection<Projectile> TrackedProjectiles => trackedProjectiles;

		/// <summary>
		/// the primary target of the weapon that behaviors traits will use for reference
		/// </summary>
		public Transform PrimaryTarget => primaryTarget;

		[field: SerializeField, Tooltip("The requirements that the weapon must meet for it to be functional")]
		public WeaponRequirementStatic Requirements { get; private set; } = null;

		/// <summary>
		/// The attributes set for the weapon to keep track of attributes modified by weapon mods
		/// </summary>
		[field: SerializeField]
		public WeaponAttributeSet SpecialAttributes { get; private set; } = null;

		/// <summary> the default projectile the weapon will use if no other mods effect it </summary>
		public Projectile DefaultProjectile => SpecialAttributes.DefaultProjectileType;

		/// <summary> The angular recoil applied to the weapon as it is fired </summary>
		public AngularRecoil AimRecoil { get; private set; } = new AngularRecoil();

		/// <summary> A reference to the entity stat container used for this weapon </summary>
		public EntityStats Stats 
		{
			get 
			{
				if (modTreeDirty) ApplyMods();
				return stats;
			} 
		}

		/// <summary> contains information to do with the current state of the weapon </summary>
		public ref State CurrentState => ref currentState;

		/// <summary> All of the mods currently applied to the weapon </summary>
		public IReadOnlyCollection<WeaponMod> AppliedMods => appliedMods;

		/// <summary> all the behavior traits currently active on the weapon </summary>
		public IReadOnlyCollection<BehaviorTrait> AllBehaviorTraits
		{
			get
			{
				if (allBehaviorTraitsDirty) RecalculateAllBehaviorTraits();
				return allBehaviorTraits;
			}
		}

		/// <summary>
		/// Whether or not the minimum requirements for the weapon have been met or not, if not,
		/// the weapon is unable to function
		/// </summary>
		public bool RequirementsMet
		{
			get => requirementsMetDirty ? CheckRequirements() : requirementsMet;
		}

		/// <summary> whether or not the weapon is currently running a routine </summary>
		public bool IsInRoutine { get => currentRoutine != null; }

		/// <summary> The fire delay between each shot fired in seconds </summary>
		public float FireDelay { get => Stats[GunStats.FireRate].FunctionalValue; }

		/// <summary> the time it takes to reload the weapon in seconds </summary>
		public float ReloadLength { get => Stats[GunStats.ReloadSpeed].FunctionalValue; }

		public IReadOnlyList<WeaponMod.Slot> ModSlots
		{
			get
			{
				if (modSlotsDirty) RecalculateModSlots();
				return modSlots;
			}
		}

		#endregion

		// Flag Field Validation: -----------------------------------------------------------------

		#region Flag Field Validation

		private bool CheckRequirements()
		{
			requirementsMet = Requirements.MeetsRequirements(this);
			requirementsMetDirty = false;
			return requirementsMet;
		}

		private void RecalculateAllBehaviorTraits()
		{
			if (modTreeDirty) ApplyMods();

			// clear all sets that will be used or modified
			allBehaviorTraits.Clear();
			foreach (var kv in qBehaviorSort)
			{
				kv.Value.Clear();
			}

			// sort each trait into a set based on it's behavior type
			foreach (var mod in AppliedMods)
			{
				foreach (var trait in mod.BehaviorTraits)
				{
					// if (!qBehaviorSort.ContainsKey(trait.Type))
					// 	qBehaviorSort.Add(trait.Type, new HashSet<BehaviorTrait>());
					// qBehaviorSort[trait.Type].Add(trait);
					allBehaviorTraits.Add(trait);
				}
			}

			// go through each set of sorted traits and only add the ones who's overlap
			// interactions don't interfere with each other
			// foreach (var kv in qBehaviorSort)
			// {
			// 	float precedence = 0;
			// 	BehaviorTrait domTrait = null;
			// 	foreach (var trait in kv.Value)
			// 	{
			// 		switch (trait.OverlapBehavior)
			// 		{
			// 			case BehaviorTrait.BehaviorOverlapType.Flexible:
			// 			case BehaviorTrait.BehaviorOverlapType.Combine:
			// 				activeBehaviorTraits.Add(trait);
			// 				break;
			// 
			// 			case BehaviorTrait.BehaviorOverlapType.Picky:
			// 				if (domTrait == null || trait.Precedence > precedence)
			// 				{
			// 					precedence = trait.Precedence;
			// 					domTrait = trait;
			// 				}
			// 				break;
			// 		}
			// 	}
			// 
			// 	if (domTrait != null) activeBehaviorTraits.Add(domTrait);
			// }

			allBehaviorTraitsDirty = false;
		}

		private void RecalculateModSlots()
		{
			modSlots = WeaponModBase.GetAllSlotsRecursively();
			modSlotsDirty = false;
		}

		private void OnModTreeChangedCall(WeaponMod.Slot target)
		{
			modTreeDirty = true;
			modSlotsDirty = true;
			requirementsMetDirty = true;
			allBehaviorTraitsDirty = true;

			OnModTreeChanged.Invoke(target);
		}

		#endregion

		// Primary Functionality: -----------------------------------------------------------------

		#region Primary Functionality

		public void TriggerFire()
		{
			if (!RequirementsMet) return;
			currentState.fireTriggered = true;
		}

		public void TriggerAlt()
		{
			if (!RequirementsMet) return;
			CurrentState.altTriggered = true;
		}

		public void TriggerReload()
		{
			if (CurrentState.reloadWait > 0) return;
			CurrentState.reloadTriggered = true;
		}

		#endregion

		// Auxillary Functionality: ---------------------------------------------------------------

		#region Auxillary Functionality

		public void ForceFire()
		{
			if (currentState.clipRounds > 0) Fire();
		}

		public void StartRoutine(Func<IEnumerator> routine)
		{
			if (IsInRoutine) return;

			IEnumerator wrapper()
			{
				IEnumerator crStart = routine();
				do yield return crStart;
				while (crStart.MoveNext());
				currentRoutine = null;
			}

			currentRoutine = StartCoroutine(wrapper());
		}

		public BehaviorStateProperty<T> GetBehaviorStateData<T>(BehaviorTrait trait, T defaultValue)
		{
			if (!behaviorStateData.ContainsKey(trait))
			{
				var prop = new BehaviorStateProperty<T>(defaultValue);
				behaviorStateData.Add(trait, prop);
				return prop;
			}

			return behaviorStateData[trait] as BehaviorStateProperty<T>;
		}

		public void ResetBehaviorStateData(BehaviorTrait trait)
		{
			if (!behaviorStateData.ContainsKey(trait)) return;
			behaviorStateData[trait].Reset();
		}

		/// <summary> sets the weapon target to be empty </summary>
		public void TargetNothing()
		{
			if (primaryTarget == null) return;
			if (primaryTarget == positionalTarget)
			{
				positionalTarget.gameObject.SetActive(false);
				positionalTarget.SetParent(transform, false);
			}

			primaryTarget = null;
		}

		/// <summary> sets the weapon target to a position in worldspace </summary>
		/// <param name="position"> the position that the weapon should target </param>
		public void TargetPosition(Vector3 position) 
		{
			if (primaryTarget != null && primaryTarget != positionalTarget) TargetNothing();
			if (!positionalTarget.gameObject.activeSelf)
			{
				positionalTarget.SetParent(null, false);
				positionalTarget.gameObject.SetActive(true);
			}

			positionalTarget.position = position;
		}

		/// <summary> sets the weapon to target a specific object </summary>
		/// <param name="target"> the object's transform that the weapon should target </param>
		public void TargetObject(Transform target)
		{
			if (target == primaryTarget) return;
			if (target == null)
			{
				TargetNothing();
				return;
			}

			primaryTarget = target;
		}

		/// <summary>
		/// Evaluates whether the specified condition returns true or false taking the weapon 
		/// state into consideration
		/// </summary>
		/// <param name="condition"> the condition to evaluate </param>
		public bool EvaluateBehaviorCondition(BehaviorTrait.ConditionalActivation condition)
		{
			switch (condition.condition)
			{
				case BehaviorTrait.ConditionalActivation.ConditionType.Always:
				case BehaviorTrait.ConditionalActivation.ConditionType.Toggle:
					return condition.IsActive(this);
				
			}
			return false;
		}

		#endregion

		// Modification: --------------------------------------------------------------------------

		#region Modification TODO: remove these and dependencies as it does not follow modular paradigm

		public void RemoveMod(WeaponMod mod, WeaponMod.Slot slot)
		{
			if (slot == null)
			{
				foreach (WeaponMod.Slot curSlot in ModSlots)
				{
					if (mod == curSlot.EquippedMod)
					{
						slot = curSlot;
						break;
					}
				}
			}

			if (slot == null) throw new Exception("Unable to remove weapon mod, slot not found");
			UnapplyMods();

			slot.EquippedMod = null;
			requirementsMetDirty = true;
			allBehaviorTraitsDirty = true;

			ApplyMods();
		}

		#endregion

		// Internal Functionality: ----------------------------------------------------------------

		#region Internal Functionality

		private void InvokeUpdateBehavior(float dt)
		{
			foreach (BehaviorTrait trait in AllBehaviorTraits)
			{
				if (!trait.DynamicRequirements.MeetsRequirements(this)) continue;
				if (!trait.ActivationCondition.IsActive(this)) continue;
				trait.OnUpdate.Invoke(trait, this, dt);
			}
		}

		private void InvokeProjectilesFiredBehavior(IReadOnlyList<Projectile> projs)
		{
			foreach (BehaviorTrait trait in AllBehaviorTraits)
			{
				if (!trait.DynamicRequirements.MeetsRequirements(this)) continue;
				if (!trait.ActivationCondition.IsActive(this)) continue;
				trait.OnProjectilesFired.Invoke(trait, this, projs);
			}
		}

		private void InvokeTriggerStartBehavior(BehaviorTrait.ConditionalActivation.TriggerType trigger)
		{
			foreach (BehaviorTrait trait in AllBehaviorTraits)
			{
				if (!trait.DynamicRequirements.MeetsRequirements(this)) continue;
				if (trait.ActivationCondition.trigger != trigger) continue;
				if (!trait.ActivationCondition.IsActive(this)) continue;
				trait.OnTriggerStart.Invoke(trait, this);
			}
		}

		private void InvokeTriggerEndBehavior(BehaviorTrait.ConditionalActivation.TriggerType trigger)
		{
			foreach (BehaviorTrait trait in AllBehaviorTraits)
			{
				if (!trait.DynamicRequirements.MeetsRequirements(this)) continue;
				if (trait.ActivationCondition.trigger != trigger) continue;
				if(!trait.ActivationCondition.IsActive(this)) continue;
				trait.OnTriggerEnd.Invoke(trait, this);
			}
		}

		private void InvokeReloadEndBehavior()
		{
			foreach (BehaviorTrait trait in AllBehaviorTraits)
			{
				if (!trait.DynamicRequirements.MeetsRequirements(this)) continue;
				if (!trait.ActivationCondition.IsActive(this)) continue;
				trait.OnReloadEnd.Invoke(trait, this);
			}
		}

		private void Fire()
		{
			int projCount = Mathf.RoundToInt(Stats[GunStats.ProjectileCount].Value);
			List<Projectile> projs = qProjList;
			projs.Clear();

			float spread = Stats[GunStats.Accuracy].FunctionalValue;

			for (int i = projCount - 1; i >= 0; i--)
			{
				Projectile proj = GetNewProjectile();
				proj.LaunchFrom(this, spread);
				projs.Add(proj);
			}

			AimRecoil.Kick(1);

			currentState.clipRounds -= 1;

			InvokeProjectilesFiredBehavior(projs);
		}

		private void AdjustRecoil()
		{
			AimRecoil.RecoilCoefficient = Stats[GunStats.RecoilSteadiness].FunctionalValue;
		}

		private void RecalculateAttributes()
		{
			foreach (WeaponMod mod in appliedMods)
			{
				SpecialAttributes.ConcatAttributes(mod.GetAllAttributes());
			}
		}

		private void ApplyMods()
		{
			if (appliedMods.Count > 0) UnapplyMods();
			behaviorStateData.Clear();

			// prepare mod list to store all mods that need to be applied
			qModList.Clear();
			
			// ---- Build mod tree begin ----
			bool ApplyRecursive(WeaponMod.Slot slot, WeaponMod mod, HashSet<WeaponMod.Slot> missedReqs)
			{
				if (mod == null) return false;

				bool modApplied = false;
				if (mod.TotalWeaponRequirements.MeetsRequirements(this))
				{
					// queue up to apply the mod when the mod tree is finished being built
					qModList.Add(mod);

					modApplied = true;
					foreach (WeaponMod.Slot childSlot in mod.Slots)
						ApplyRecursive(childSlot, childSlot.ActiveMod, missedReqs);
				}
				else if (slot != null) missedReqs.Add(slot);

				return modApplied;
			}

			qModSlotSet.Clear();
			ApplyRecursive(null, WeaponModBase, qModSlotSet);

			// ensure that any mods who didn't meet the weapon requirements when they were first
			// iterated through don't get missed, as long as they now do meet the requirements
			while (qModSlotSet.Count > 0)
			{
				bool wasModApplied = false;
				foreach (var slot in qModSlotSet)
				{
					if (slot.ActiveMod != null)
					{
						if (slot.ActiveMod.TotalWeaponRequirements.MeetsRequirements(this))
						{
							qModSlotSet.Remove(slot);
							ApplyRecursive(slot, slot.ActiveMod, qModSlotSet);
							wasModApplied = true;
						}
					}
				}

				if (!wasModApplied) break;
			}
			// ---- Build mod tree end ----

			// mark mod tree as clean
			modTreeDirty = false;
			modSlotsDirty = true;
			requirementsMetDirty = true;
			allBehaviorTraitsDirty = true;

			// call mod.apply now that mod tree is constructed
			foreach (WeaponMod mod in qModList)
			{
				mod.ApplyMod(this);
				appliedMods.Add(mod);
			}

			AdjustRecoil();
			RecalculateAttributes();
		}

		private void UnapplyMods()
		{
			// temporarily set the mod tree as clean so that mod.UnapplyMod doesn't cause
			// recursive call back to ApplyMods. TODO should probably find a better way to
			// do this
			modTreeDirty = false;

			foreach (WeaponMod mod in AppliedMods) mod.UnapplyMod(this);
			appliedMods.Clear();

			AdjustRecoil();

			// mark mod tree as dirty
			modTreeDirty = true;
			modSlotsDirty = true;
			requirementsMetDirty = true;
			allBehaviorTraitsDirty = true;
		}

		#region Weapon State Handling

		private void HandleWeaponActions()
		{
			// reloading
			if (currentState.IsReloading)
			{
				HandleReload();
				if (!currentState.IsReloading)
				{
					FinishReload();
					if (currentState.fireWait < 0) currentState.fireWait = 0;
				}
			}

			// reload trigger
			else
			{
				if (CurrentState.reloadTriggered) StartReload();

				// handle firing
				if (currentState.fireTriggered)
				{
					HandleFiring();
				}

			}
			

			AimRecoil.HandleRecoil();

			InvokeUpdateBehavior(Time.deltaTime);
			HandleWeaponStateTriggers();

			currentState.fireWait -= Time.deltaTime;
			if (currentState.fireWait < 0) currentState.fireWait = 0;
		}

		private void HandleWeaponStateTriggers()
		{
			if (!currentState.IsReloading)
			{
				// Primary Trigger
				if (currentState.fireTriggered)
				{
					if (currentState.fireTriggeredTime <= 0) 
						InvokeTriggerStartBehavior(
							BehaviorTrait.ConditionalActivation.TriggerType.PrimaryTrigger
						);
					currentState.fireTriggeredTime += Time.deltaTime;
				}
				else
				{
					if (CurrentState.FireTriggeredTime > 0)
						InvokeTriggerEndBehavior(
							BehaviorTrait.ConditionalActivation.TriggerType.PrimaryTrigger
						);
					currentState.fireTriggeredTime = 0;
				}

				// Alt Trigger
				if (currentState.altTriggered)
				{
					if (currentState.altTriggeredTime <= 0)
						InvokeTriggerStartBehavior(
							BehaviorTrait.ConditionalActivation.TriggerType.AltTrigger
						);
					currentState.altTriggeredTime += Time.deltaTime;
				}
				else
				{
					if (CurrentState.altTriggeredTime > 0)
						InvokeTriggerEndBehavior(
							BehaviorTrait.ConditionalActivation.TriggerType.AltTrigger
						);
					currentState.altTriggeredTime = 0;
				}

				// Reload
				if (currentState.reloadTriggered)
				{
					if (currentState.reloadTriggeredTime <= 0)
						InvokeTriggerStartBehavior(
							BehaviorTrait.ConditionalActivation.TriggerType.Reload
						);
					currentState.reloadTriggeredTime += Time.deltaTime;
				}
				else
				{
					if (CurrentState.reloadTriggeredTime > 0)
						InvokeTriggerEndBehavior(
							BehaviorTrait.ConditionalActivation.TriggerType.Reload
						);
					currentState.reloadTriggeredTime = 0;
				}
			}

			currentState.fireTriggered = false;
			currentState.altTriggered = false;
			currentState.reloadTriggered = false;
		}

		private void StartReload()
		{
			currentState.reloadWait = ReloadLength;

			InvokeReloadEndBehavior();
		}

		private void FinishReload()
		{
			currentState.reloadWait = 0;
			currentState.clipRounds = Mathf.Round(Stats[GunStats.ClipSize].Value);
		}

		private void HandleReload()
		{
			if (currentState.reloadWait <= 0)
			{
				FinishReload();
			}

			currentState.reloadWait -= Time.deltaTime;

			if (currentState.reloadWait == 0f) currentState.reloadWait = -1;
		}

		private void HandleFiring()
		{
			bool fireWaitFinished = currentState.fireWait <= 0;
			bool hasAmmo = currentState.clipRounds > Mathf.Epsilon;
			bool firstFireStep = currentState.fireTriggeredTime <= 0;

			bool canFire = false;
			switch (SpecialAttributes.FiringMode)
			{
				case FireModeType.SemiAuto:
					canFire = hasAmmo && firstFireStep && fireWaitFinished;
					break;

				case FireModeType.FullAuto:
					canFire = hasAmmo && fireWaitFinished;
					break;

				case FireModeType.Continuous:
					canFire = hasAmmo;
					break;
			}

			if (canFire)
			{
				currentState.fireWait += FireDelay;
				Fire();
			}

			// auto reload
			else if (!hasAmmo && firstFireStep)
			{
				StartReload();
			}
		}

		#endregion

		#endregion

		// Internal Utility: ----------------------------------------------------------------------

		#region Internal Utility

		private void ForceApplyModBase()
		{
			WeaponMod temp = weaponModBase;
			WeaponModBase = null;
			if(temp != null) WeaponModBase = Instantiate(temp);
		}

		private Projectile GetNewProjectile()
		{
			Projectile proj = Instantiate(DefaultProjectile);
			proj.SetPropertiesFromParentGun(this);
			proj.name = DefaultProjectile.name;
			return proj;
		}

		#endregion

		// Unity Messages: ------------------------------------------------------------------------

		#region Unity Messages

		private void Start()
		{
			positionalTarget = new GameObject(name + "Target").transform;
			positionalTarget.gameObject.SetActive(false);
			positionalTarget.SetParent(transform, false);

			stats = GetComponent<EntityStats>();
			ForceApplyModBase();
			ApplyMods();

			AdjustRecoil();
			currentState.clipRounds = Stats[GunStats.ClipSize];
		}

		private void Update()
		{
			if (modTreeDirty) ApplyMods();
			HandleWeaponActions();
		}

		private void OnDestroy()
		{
			if (positionalTarget != null && positionalTarget.parent != this)
			{
				Destroy(positionalTarget.gameObject);
			}
		}

		#endregion

		// Subtype Definitions: -------------------------------------------------------------------

		#region Subtype Definitions

		public enum FireModeType
		{
			SemiAuto,
			FullAuto,
			Continuous
		}

		public enum ReloadModeType
		{
			Magazine,
			IndividualRounds,
			None
		}

		public interface IReadonlyState
		{
			public float ReloadWait { get; }
			public float FireWait { get; }
			public float ClipRounds { get; }
			public float FireTriggeredTime { get; }
			public float AltTriggeredTime { get; }
			public float ReloadTriggeredTime { get; }
			public int ClipRoundsWhole { get; }
			public bool IsReloading { get; }
			public bool FireTriggered { get; }
			public bool AltTriggered { get; }
			public bool ReloadTriggered { get; }
		}

		public abstract class BehaviorStatePropertyBase { public abstract void Reset(); }

		public class BehaviorStateProperty<T> : BehaviorStatePropertyBase
		{
			public BehaviorStateProperty(T initialValue)
			{
				value = initialValue;
			}

			public T value = default;

			public override void Reset()
			{
				value = default;
			}
		}

		[Serializable]
		public struct State : IReadonlyState
		{
			public float reloadWait;

			public float fireWait;

			public float clipRounds;

			public float currentEnergy;

			public bool fireTriggered;

			public float fireTriggeredTime;

			public bool altTriggered;

			public float altTriggeredTime;

			public bool reloadTriggered;

			public float reloadTriggeredTime;

			// ------------------------------------------------------------------------------------

			public int ClipRoundsWhole { get => Mathf.RoundToInt(clipRounds); }

			public bool IsReloading { get => reloadWait > 0; }

			// ------------------------------------------------------------------------------------

			public float ReloadWait => reloadWait;

			public float FireWait => fireWait;

			public float ClipRounds => clipRounds;

			public float FireTriggeredTime => fireTriggeredTime;

			public float AltTriggeredTime => altTriggeredTime;

			public float ReloadTriggeredTime => reloadTriggeredTime;

			public bool FireTriggered => fireTriggered;

			public bool AltTriggered => altTriggered;

			public bool ReloadTriggered => reloadTriggered;
		}

		[Serializable]
		public class AngularRecoil
		{
			/// <summary>
			/// the angular offset from the weapon's aim direction that the projectiles will be 
			/// effected by
			/// </summary>
			public float AdjustedOffset => offset * Spread;

			public float Spread { get; private set; } = 0f;

			public float RecoilCoefficient { get; set; } = 0f;

			private readonly float spreadDamping = 0.5f;

			private readonly float spreadDeltaDamping = 0.5f;

			private readonly float offDeltaDamping = 0.35f;

			private float spreadDampDelta = 0;

			private float spreadDeltaDampDelta = 0;

			private float offDeltaDampDelta = 0;

			private float spreadDelta = 0;

			private float offDelta = 0;

			private float offset = 0;

			// ------------------------------------------------------------------------------------

			/// <summary>
			/// handles recoil offset and spead over time
			/// </summary>
			public void HandleRecoil()
			{
				// offset = Mathf.SmoothDamp(offset, 0, ref dampDelta, Time.deltaTime);
				Spread = Mathf.SmoothDamp(
					Spread, 0, ref spreadDampDelta, 
					spreadDamping, 100, Time.deltaTime
				);
				spreadDelta = Mathf.SmoothDamp(
					spreadDelta, 0, ref spreadDeltaDampDelta, 
					spreadDeltaDamping, 100, Time.deltaTime
				);
				offDelta = Mathf.SmoothDamp(
					offDelta, 0, ref offDeltaDampDelta, 
					offDeltaDamping, 100, Time.deltaTime
				);

				Spread += spreadDelta * Time.deltaTime;
				offset += offDelta * Time.deltaTime;

				// force offset to stay between -1 and 1
				if(Math.Abs(offset) > 1)
				{
					float offSign = Math.Sign(offset);
					offDelta = Math.Abs(offDelta) * -offSign;
					offset = offSign;
				}
			}

			/// <summary>
			/// cause the weapon recoil to kick and increase weapon spread - as if the 
			/// gun were fired
			/// </summary>
			/// <param name="amount">the amount of kick to apply to the recoil</param>
			public void Kick(float amount)
			{
				amount *= 0.5f;

				spreadDelta += (amount * RecoilCoefficient);

				float kickDir = UnityEngine.Random.value < 0.5 ? -1 : 1;
				offDelta = (Math.Abs(offDelta) + (amount * RecoilCoefficient)) * kickDir;
			}
		}

		#endregion
	}
}