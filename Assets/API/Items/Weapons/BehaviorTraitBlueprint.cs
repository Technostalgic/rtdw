﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Weapons
{
	[CreateAssetMenu(fileName = "BehaviorTraitBlueprint", menuName = "RTDW/WeaponMods/Behavior Trait Blueprint")]
	public class BehaviorTraitBlueprint : WeaponModTraitBlueprint
	{
		// Static Shortcuts: ----------------------------------------------------------------------

		private static GunStatSet GunStats => GameManager.InitParams.GunStats;

		// Temporary Query Variables: -------------------------------------------------------------

		private static readonly List<WeaponAttribute> qAttrs = new List<WeaponAttribute>();

		// ----------------------------------------------------------------------------------------

		[field: SerializeField, TextArea]
		public string Description { get; private set; } = "Trait Description";

		[field: SerializeField]
		public WeaponRequirementStatic RequirementStatic { get; private set; } = default;

		[field: SerializeField]
		public WeaponRequirementDynamic RequirementDynamic { get; private set; } = default;

		[field: SerializeField]
		public WeaponAttributeBlueprint[] WeaponAttributes { get; private set; } = default;

		// public BehaviorTrait.BehaviorType type = BehaviorTrait.BehaviorType.Misc;

		// public BehaviorTrait.BehaviorOverlapType overlapBehavior = BehaviorTrait.BehaviorOverlapType.Flexible;

		// public AnimationCurve precedence = default;

		[Space]
		
		[Tooltip("The defauld condition for the behavior activation, the player can modify this")]
		public BehaviorTrait.ConditionalActivation activationCondition = default;

		[Space]

		// Parameters for Behaviors: --------------------------------------------------------------

		[Tooltip("Will be true in created instance if curve evaluates to greater than 0.5")]
		public AnimationCurve boolValue = default;

		[Tooltip("Rounded to nearest whole number when curve is evalueated")]
		public AnimationCurve intValue = default;

		[Tooltip("Used as a parameter to modify event behaviors")]
		public AnimationCurve floatValue = default;

		[field: Space]

		// Behavior Events: -----------------------------------------------------------------------

		[field: SerializeField]
		public GunUpdateBehavior OnUpdate { get; private set; } = new GunUpdateBehavior();

		[field: SerializeField]
		public GunActionBehavior OnFireTriggerStart { get; private set; } = new GunActionBehavior();

		[field: SerializeField]
		public GunActionBehavior OnFireTriggerEnd { get; private set; } = new GunActionBehavior();

		[field: SerializeField]
		public GunActionBehavior OnReloadStart { get; private set; } = new GunActionBehavior();

		[field: SerializeField]
		public GunFireBehavior OnProjectilesFired { get; private set; } = new GunFireBehavior();

		// ----------------------------------------------------------------------------------------

		public override WeaponModTrait CreateInstance(float effectFactor)
		{
			// float prec = precedence.Evaluate(effectFactor);
			bool bv = boolValue.Evaluate(effectFactor) > 0.5;
			int iv = Mathf.RoundToInt(intValue.Evaluate(effectFactor));
			float fv = floatValue.Evaluate(effectFactor);

			qAttrs.Clear();
			foreach (var bp in WeaponAttributes)
			{
				qAttrs.Add(bp.CreateInstance());
			}

			BehaviorTrait r = new BehaviorTrait(
				// type, overlapBehavior, prec, 
				Description,
				RequirementStatic, RequirementDynamic.Clone(), qAttrs.ToArray(), 
				activationCondition.Clone(), bv, iv, fv,
				OnUpdate, OnFireTriggerStart, OnFireTriggerEnd,
				OnReloadStart, OnProjectilesFired
			);
			r.Label = Label;

			return r;
		}

		// Behavior Function Definitions: ---------------------------------------------------------

		/// <summary>
		/// makes the weapon target the position that it's pointint at
		/// </summary>
		public void Behavior_RayTargeting_Update(BehaviorTrait trait, Weapon gun, float dt)
		{
			float dist = 100;
			Vector2 pos = gun.ProjectileSpawnPoint.position;
			Vector2 dir = gun.transform.right;
			LayerMask colMask = gun.SpecialAttributes.ProjectileType.ContactFilter.layerMask;

			RaycastHit2D hit = Physics2D.Raycast(pos, dir, dist, colMask);
			if (hit.collider != null) gun.TargetPosition(hit.point);
			else gun.TargetPosition(pos + dir * dist);
		}

		/// <summary>
		/// regenerate ammo in the gun's clip based on the float value paramater. Bool value 
		/// parameter specifies whether it is a percentage of the max clip size or not. Float 
		/// parameter specifies units / second
		/// </summary>
		public void Behavior_RegenClip_Update(BehaviorTrait trait, Weapon gun, float dt)
		{
			float regenAmount = dt * trait.floatValue;
			if (trait.boolValue) regenAmount *= gun.Stats[GunStats.ClipSize];

			gun.CurrentState.clipRounds += regenAmount;
		}

		/// <summary>
		/// Causes all tracked projectiles to seek the target if they are within the specified 
		/// range. Float value is the energy cost per second per projectile. Int value is range
		/// </summary>
		public void Behavior_SeekTarget_Update(BehaviorTrait trait, Weapon gun, float dt)
		{
			float cost = trait.floatValue * dt;
			if (gun.CurrentState.currentEnergy < cost) return;

			float rangeSq = trait.intValue * trait.intValue;
			Transform target = gun.PrimaryTarget;

			foreach (Projectile proj in gun.TrackedProjectiles)
			{
				Vector2 dif = target.position - proj.transform.position;
				float distSq = dif.sqrMagnitude;
				if (rangeSq < distSq) continue;

				Vector2 difNorm = dif.normalized;
				float difDot = Vector2.Dot(difNorm, 
					(proj.Velocity.sqrMagnitude <= 1) ? 
						proj.Velocity : 
						proj.Velocity.normalized
				);

				// damp the projectile if it's heading the opposit direction of the target
				float maxDamp = 0.5f;
				float damp = difDot < 0 ? 1 + (difDot * maxDamp) : 1;
				float dampFactor = Mathf.Pow(damp, dt);
				proj.Velocity *= dampFactor;

				// calculate and apply acceleration to projectile
				float seekFactor = 10;
				if (proj.Velocity.sqrMagnitude > 1) seekFactor *= proj.Velocity.magnitude;
				Vector2 acceleration = difNorm * seekFactor;
				proj.Velocity += acceleration * dt;
			}
		}

		/// <summary>
		/// Tracks projectiles fired from a weapon at a specified shot interval. Float value is 
		/// energy cost. Int value specifies the interval, 0 and 1 track every shot, 2 tracks ever 
		/// other, 3 tracks every 3rd, etc. Bool value specifies whether or not the interval 
		/// tracks the first shot or not, this may be desirable to use (with a state reset on 
		/// reload behavior) to make every first shot of a clip get tracked
		/// </summary>
		public void Behavior_TrackOnInterval_Fire(
			BehaviorTrait trait, Weapon gun, IReadOnlyList<Projectile> projs
		) {
			int interval = trait.intValue;
			bool startOnClip = trait.boolValue;
			var sequence = gun.GetBehaviorStateData<int>(
				trait, 
				startOnClip ? 
					0 : 
					Mathf.RoundToInt(gun.Stats[GunStats.ClipSize]) % interval
			);

			if(sequence.value > 0)
			{
				sequence.value--;
				return;
			}
			
			sequence.value = interval - 1;

			float cost = trait.floatValue;
			foreach(var proj in projs)
			{
				if (gun.CurrentState.currentEnergy < cost) break;
				gun.TrackedProjectiles.Add(proj);
				gun.CurrentState.currentEnergy -= cost;
			}
		}

		/// <summary>
		/// Recharges the gun a bit every time the weapon is fired
		/// </summary>
		public void Behavior_RechargeOnFire_Fire(
			BehaviorTrait trait, Weapon gun, IReadOnlyList<Projectile> projs
		) {
			float gain = trait.floatValue;
			gun.CurrentState.currentEnergy += gain;
		}

		/// <summary>
		/// recharges the weapon by a certain amoount every time it is reloaded. Float value 
		/// specifies how much is recharged
		/// </summary>
		public void Behavior_Recharge_Reload(BehaviorTrait trait, Weapon gun)
		{
			gun.CurrentState.currentEnergy += trait.floatValue;
		}

		/// <summary>
		/// Reset the behavior state data every time the weapon is reloaded
		/// </summary>
		public void Behavior_ResetStateOnReload_Reload(BehaviorTrait trait, Weapon gun)
		{
			gun.ResetBehaviorStateData(trait);
			trait.persistentData = null;
		}
	}
}