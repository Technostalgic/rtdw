﻿using System.Collections;
using UnityEngine;

namespace RTDW.Weapons
{
	[System.Serializable]
	public abstract class WeaponAttribute
	{
		/// <summary>
		/// the amount of precedence this attribute has over other attributes that make a 
		/// different argument
		/// </summary>
		public float Precedence { get; set; } = 0;

		public abstract bool HasSameArgument(WeaponAttribute compare);

		public abstract WeaponAttribute Clone();

		// ----------------------------------------------------------------------------------------

		[System.Serializable]
		public class FiringMode : WeaponAttribute
		{
			public FiringMode(Weapon.FireModeType firingType)
			{
				FiringType = firingType;
			}

			[field: SerializeField]
			public Weapon.FireModeType FiringType { get; private set; } = default;

			public override bool HasSameArgument(WeaponAttribute compare)
			{
				if ((compare is FiringMode tcomp))
				{
					return tcomp.FiringType == FiringType;
				}
				return false;
			}

			public override WeaponAttribute Clone()
			{
				var r = new FiringMode(FiringType);
				r.Precedence = Precedence;
				return r;
			}
		}

		[System.Serializable]
		public class ReloadingMode : WeaponAttribute
		{
			public ReloadingMode(Weapon.ReloadModeType type)
			{
				ReloadType = type;
			}

			[field: SerializeField]
			public Weapon.ReloadModeType ReloadType { get; private set; } = default;

			public override bool HasSameArgument(WeaponAttribute compare)
			{
				if ((compare is ReloadingMode tcomp))
				{
					return tcomp.ReloadType == ReloadType;
				}
				return false;
			}

			public override WeaponAttribute Clone()
			{
				var r = new ReloadingMode(ReloadType);
				r.Precedence = Precedence;
				return r;
			}
		}

		[System.Serializable]
		public class ProjectileTypeSpec : WeaponAttribute
		{
			public ProjectileTypeSpec(Projectile prefab)
			{
				ProjectileType = prefab;
			}

			[field: SerializeField]
			public Projectile ProjectileType { get; private set; } = default;

			public override bool HasSameArgument(WeaponAttribute compare)
			{
				if ((compare is ProjectileTypeSpec tcomp))
				{
					return tcomp.ProjectileType == ProjectileType;
				}
				return false;
			}

			public override WeaponAttribute Clone()
			{
				var r = new ProjectileTypeSpec(ProjectileType);
				r.Precedence = Precedence;
				return r;
			}
		}

		[System.Serializable]
		public class DamageTypeSpec : WeaponAttribute
		{
			public DamageTypeSpec(DamageType type)
			{
				Type = type;
			}

			[field: SerializeField]
			public DamageType Type { get; private set; } = default;

			public override bool HasSameArgument(WeaponAttribute compare)
			{
				if ((compare is DamageTypeSpec tcomp))
				{
					return tcomp.Type == Type;
				}
				return false;
			}

			public override WeaponAttribute Clone()
			{
				var r = new DamageTypeSpec(Type);
				r.Precedence = Precedence;
				return r;
			}
		}
	}
}