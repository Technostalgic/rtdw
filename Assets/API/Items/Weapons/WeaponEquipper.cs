using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTDW.Actors;

namespace RTDW.Weapons
{
	public class WeaponEquipper : MonoBehaviour
	{
		[field: SerializeField]
		public Weapon EquippedWeapon { get; private set; }

		// ----------------------------------------------------------------------------------------

		public void Cmd_Attack(Command.IVectorCommand cmd)
		{
			UseWeapon();
		}

		public void Cmd_TriggerAlt(Command.IVectorCommand cmd)
		{
			UseWeaponAlt();
		}

		public void Cmd_Reload(Command.IVoidCommand cmd)
		{
			ReloadWeapon();
		}

		// ----------------------------------------------------------------------------------------

		/// <summary>
		/// equips a weapon and returns a storage item of the previously equipped weapon
		/// </summary>
		/// <param name="wep">the weapon to be equipped</param>
		public StorageItem Equip(Weapon wep)
		{
			StorageItem si = EquippedWeapon?.StorageItem;

			EquippedWeapon = wep;

			return si;
		}

		/// <summary>
		/// attempts to use the equipped weapon if there is a weapon equipped
		/// </summary>
		public void UseWeapon()
		{
			EquippedWeapon?.TriggerFire();
		}

		/// <summary>
		/// attempts to use the alternate trigger on the weapon
		/// </summary>
		public void UseWeaponAlt()
		{
			EquippedWeapon?.TriggerAlt();
		}

		/// <summary>
		/// attempt to reload the equipped weapon if there is one
		/// </summary>
		public void ReloadWeapon()
		{
			EquippedWeapon?.TriggerReload();
		}
	}
}
