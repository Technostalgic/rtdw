﻿using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Weapons
{
	[CreateAssetMenu(fileName = "WeaponModBlueprint", menuName = "RTDW/WeaponMods/Weapon Mod Blueprint")]
	public class WeaponModBlueprint : ScriptableObject
	{
		// Temporary Query Variables: -------------------------------------------------------------

		private static readonly List<StaticTrait> qStatTraitList = new List<StaticTrait>();

		private static readonly List<BehaviorTrait> qBehaviorTraitList = new List<BehaviorTrait>();

		private static readonly List<WeaponMod.SlotType> qModSlotList = new List<WeaponMod.SlotType>();

		private static readonly List<WeaponAttribute> qAttrs = new List<WeaponAttribute>();

		// ----------------------------------------------------------------------------------------

		public string mainLabel = "WeaponMod";

		[TextArea]
		public string description = "";

		public Sprite icon = null;

		public Sprite iconBackground = null;

		public Color iconColor = Color.white;

		public Color iconBackgroundColor = Color.clear;

		[Space]

		public WeaponMod.SlotType requiredSlot = WeaponMod.SlotType.None;

		public WeaponRequirementStatic selfStaticRequirements = default;

		public WeaponAttributeBlueprint[] selfAttributes = default;

		public StatTraitProbInfo[] statTraits = default;

		public BehaviorTraitProbInfo[] behaviorTraits = default;

		public SlotProbabilityInfo[] slots = default;

		// ----------------------------------------------------------------------------------------

		/// <summary>
		/// Create an instance of the weapion mod based on the blueprint rules
		/// </summary>
		/// <param name="quality">the quality of the instance that is made</param>
		public WeaponMod CreateInstance(float quality)
		{
			qStatTraitList.Clear();
			foreach (StatTraitProbInfo bpInfo in statTraits)
			{
				WeaponModTrait trait = bpInfo.TryGetTrait(quality);
				if (trait == null) continue;
				qStatTraitList.Add(trait as StaticTrait);
			}

			qBehaviorTraitList.Clear();
			foreach (BehaviorTraitProbInfo bpInfo in behaviorTraits)
			{
				WeaponModTrait trait = bpInfo.TryGetTrait(quality);
				if (trait == null) continue;
				qBehaviorTraitList.Add(trait as BehaviorTrait);
			}

			qModSlotList.Clear();
			foreach (SlotProbabilityInfo slotInfo in slots)
			{
				if(Random.value < slotInfo.probabilty)
				{
					qModSlotList.Add(slotInfo.slotType);
				}
			}

			StorageItem si = new StorageItem(null);
			si.Label = mainLabel;
			si.Description = description;
			si.Icon = icon;
			si.IconBackground = iconBackground;
			si.IconColor = iconColor;
			si.IconBackgroundColor = iconBackgroundColor;

			qAttrs.Clear();
			foreach (var bp in selfAttributes)
			{
				qAttrs.Add(bp.CreateInstance());
			}

			WeaponMod r = WeaponMod.Create(
				requiredSlot, si, qAttrs.ToArray(),
				qStatTraitList.ToArray(), qBehaviorTraitList.ToArray(), qModSlotList
			);

			return r;
		}

		// Subtype Definitions: -------------------------------------------------------------------

		public abstract class TraitProbInfo<T> where T : WeaponModTraitBlueprint
		{
			public T traitBlueprint = null;

			[Range(0, 1)]
			public float probability = 1;

			[Range(0, 1)]
			public float effectRangeMin = 0;

			[Range(0, 1)]
			public float effectRangeMax = 1;

			public WeaponModTrait TryGetTrait(float quality)
			{
				if (Random.value > probability) return null;

				float tquality = (effectRangeMax - effectRangeMin) * quality;
				tquality += effectRangeMin;

				return traitBlueprint.CreateInstance(tquality);
			}
		}

		[System.Serializable]
		public class StatTraitProbInfo : TraitProbInfo<StaticTraitBlueprint> { }

		[System.Serializable]
		public class BehaviorTraitProbInfo : TraitProbInfo<BehaviorTraitBlueprint> { }

		[System.Serializable]
		public class SlotProbabilityInfo 
		{
			public WeaponMod.SlotType slotType = WeaponMod.SlotType.None;

			[Range(0, 1)]
			public float probabilty = 1;
		}
	}
}