using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RTDW.Weapons
{
	[CreateAssetMenu(fileName = "WeaponMod", menuName = "RTDW/WeaponMods/Weapon Mod")]
	public class WeaponMod : ScriptableObject, IStorageItemTarget, IStorageItemValue
	{
		private static GunStatSet GunStats => GameManager.InitParams.GunStats;

		// Temporary Query Variables: -------------------------------------------------------------

		private static readonly List<Slot> qSlotList = new List<Slot>();

		private static readonly List<Stat.Modifier> qStatModList = new List<Stat.Modifier>();

		private static readonly List<StaticTrait> qStatTraitList = new List<StaticTrait>();

		private static readonly List<WeaponAttribute> qAttrs = new List<WeaponAttribute>();

		// ----------------------------------------------------------------------------------------

		public static WeaponMod Create(
			SlotType requiredSlot, StorageItem item, WeaponAttribute[] attributes,
			StaticTrait[] statTraits, BehaviorTrait[] behaviorTraits, IReadOnlyList<SlotType> slots
		)
		{
			WeaponMod r = CreateInstance<WeaponMod>();
			r.selfAttributes = attributes;
			r.StatTraits = statTraits;
			r.BehaviorTraits = behaviorTraits;
			r.RequiredSlotType = requiredSlot;
			r.storageItem = item;
			r.StorageItem = item.AttachToObject(r);
			r.name = item.Label;

			qSlotList.Clear();
			foreach (SlotType st in slots)
			{
				qSlotList.Add(new Slot(st, r));
			}
			r.slots = qSlotList.ToArray();
			r.AttachSlots();

			return r;
		}

		// Events: --------------------------------------------------------------------------------

		public readonly UnityEvent<Slot> OnModTreeChanged = new UnityEvent<Slot>();

		// Field Flags: ---------------------------------------------------------------------------

		private bool requirementsDirty = true;

		private bool slotsAttached = false;

		private bool meetsModRequirementsDirty = true;

		// Data Fields: ---------------------------------------------------------------------------

		[SerializeField]
		private StorageItem storageItem = null;

		[SerializeField]
		private Slot[] slots = new Slot[] { };

		[SerializeField]
		private WeaponAttribute[] selfAttributes = null;

		[SerializeField]
		private WeaponRequirementStatic selfWeaponRequirements = null;

		private WeaponRequirementStatic totalWeaponRequirements = new WeaponRequirementStatic();

		private Stat.Modifier[] statModifiers = null;

		private bool meetsModRequirements = false;

		// Property Definitions: ------------------------------------------------------------------

		public Object Self => this;

		public StorageItem StorageItem
		{
			get
			{
				if (storageItem == null || storageItem.ItemTarget == null) CreateStorageItem();
				return storageItem;
			}
			private set { storageItem = value; }
		}

		public IReadOnlyList<WeaponAttribute> SelfAtributes => selfAttributes;

		public WeaponRequirementStatic TotalWeaponRequirements
		{
			get
			{
				if (requirementsDirty) RecalculateWeaponRequirements();
				return totalWeaponRequirements;
			}
		}

		public Stat.Modifier[] StatModifiers
		{
			get
			{
				if (statModifiers == null) RecalculateStatModifiers();
				return statModifiers;
			}
		}

		public bool MeetsModRequirements
		{
			get
			{
				if (meetsModRequirementsDirty) RecalculateMeetsRequirements();
				return meetsModRequirements;
			}
		}

		[field: SerializeField]
		public SlotType RequiredSlotType { get; private set; } = SlotType.None;

		[field: SerializeField]
		public StaticTrait[] StatTraits { get; private set; } = null;

		[field: SerializeField]
		public BehaviorTrait[] BehaviorTraits { get; private set; } = null;

		public IReadOnlyList<Slot> Slots
		{
			get
			{
				if (!slotsAttached) AttachSlots();
				return slots;
			}
		}

		// Cached Property Calculation: -----------------------------------------------------------

		private void AttachSlots()
		{
			foreach (var slot in slots) slot.AttachSlot(this);
			slotsAttached = true;
		}

		private void RecalculateWeaponRequirements()
		{
			totalWeaponRequirements.ClearRequirements();

			if(selfWeaponRequirements != null)
				totalWeaponRequirements.ConcatRequirements(selfWeaponRequirements);

			foreach (StaticTrait trait in StatTraits)
				totalWeaponRequirements.ConcatRequirements(trait.StaticRequirements);

			foreach (BehaviorTrait trait in BehaviorTraits)
				totalWeaponRequirements.ConcatRequirements(trait.StaticRequirements);
		}

		private void RecalculateStatModifiers()
		{
			qStatModList.Clear();

			foreach (StaticTrait trait in StatTraits)
				qStatModList.AddRange(trait.Modifiers);

			statModifiers = qStatModList.ToArray();
		}

		private void RecalculateMeetsRequirements()
		{
			bool recursiveCall(WeaponMod mod)
			{
				foreach (var slot in mod.Slots)
				{
					if (slot.Required)
					{
						if (slot.ActiveMod == null) return false;
						if (!recursiveCall(slot.ActiveMod)) return false;
					}
				}

				return true;
			}

			bool meetsReqs = recursiveCall(this);
			meetsModRequirements = meetsReqs;
			meetsModRequirementsDirty = true;
		}

		// Primary Usage: -------------------------------------------------------------------------

		public virtual void ApplyMod(Weapon target)
		{
			if (target.AppliedMods.Contains(this)) return;

			foreach (Stat.Modifier statMod in StatModifiers)
			{
				if (statMod.IsValid) target.Stats.ApplyModifier(statMod);
			}
		}

		public virtual void UnapplyMod(Weapon target)
		{
			if (!target.AppliedMods.Contains(this)) return;

			foreach (Stat.Modifier statMod in StatModifiers)
			{
				if (statMod.IsValid) target.Stats.UnapplyModifier(statMod);
			}
		}

		public WeaponAttribute[] GetAllAttributes()
		{
			qAttrs.Clear();
			
			foreach(WeaponModTrait trait in StatTraits)
			{
				qAttrs.AddRange(trait.Attributes);
			}

			foreach (WeaponModTrait trait in BehaviorTraits)
			{
				qAttrs.AddRange(trait.Attributes);
			}

			return qAttrs.ToArray();
		}

		public Slot[] GetAllSlotsRecursively()
		{
			qSlotList.Clear();

			void recursiveCall(WeaponMod child)
			{
				qSlotList.AddRange(child.Slots);

				foreach (var slot in child.Slots)
				{
					if (slot.ActiveMod != null)
						recursiveCall(slot.ActiveMod);
				}
			}

			recursiveCall(this);

			return qSlotList.ToArray();
		}
		
		// Auxiliary Usage: -----------------------------------------------------------------------

		public int GetGoldValue()
		{
			int val = 1;
			val += slots.Length;
			val += StatTraits.Length;
			val += BehaviorTraits.Length * 3;

			foreach(StaticTrait trait in StatTraits)
			{
				val += trait.Attributes.Count;
			}

			return val;
		}

		// Internal Usage: ------------------------------------------------------------------------

		private StorageItem CreateStorageItem()
		{
			storageItem = new StorageItem(this);
			// storageItem.Icon = icon;
			// storageItem.IconColor = iconColor;
			// storageItem.Description = description;
			return storageItem;
		}

		// ----------------------------------------------------------------------------------------

		/// <summary>
		/// check to see if a mod with a slot requirement (inner), fits into the outer slot
		/// </summary>
		/// <returns></returns>
		public static bool CheckSlotTypeCompatibility(SlotType outer, SlotType inner)
		{
			return (
				(outer == inner || outer == SlotType.Any || inner == SlotType.Any) &&
				(outer != SlotType.None && inner != SlotType.None)
			);
		}

		// Subtype Definitions: -------------------------------------------------------------------

		public enum SlotType
		{
			None,
			Any, Base,
			Chassis, ChassisMod,
			Sights, SightsMod,
			Barrel, BarrelMod,
			Chamber, ChamberMod,
			Clip, ClipMod,
			Stock, StockMod,
			Grip, GripMod,
			Muzzle,
			Booster,
			Payload,
			PowerSupply, PowerSupplyMod,
			Circuit
		}

		[System.Serializable]
		public class Slot
		{
			public Slot(SlotType type) { SlotType = type; }

			public Slot(SlotType type, WeaponMod parent)
			{
				SlotType = type;
				AttachSlot(parent);
			}

			// Data Fields: -----------------------------------------------------------------------

			[SerializeField]
			[Tooltip("The current mod that's equipped in this slot")]
			private WeaponMod equippedMod = null;

			[SerializeField, Tooltip("The default weapon mod that this slot acts as if it's empty")]
			private WeaponMod defaultMod = null;

			// Property Definitions: --------------------------------------------------------------

			[field: SerializeField]
			public SlotType SlotType { get; private set; } = SlotType.Any;

			/// <summary> the mod that the slot is attached to </summary>
			public WeaponMod ParentMod { get; private set; } = null;

			public WeaponMod EquippedMod {
				get => equippedMod;
				set
				{
					if (value == equippedMod) return;
					if(equippedMod != null)
					{
						equippedMod.OnModTreeChanged.RemoveListener(OnActiveModeTreeChange);
					}
					
					equippedMod = value;
					if(ParentMod != null) ParentMod.OnModTreeChanged.Invoke(this);
					if(equippedMod != null)
					{
						equippedMod.OnModTreeChanged.AddListener(OnActiveModeTreeChange);
					}
				}
			}
			
			public WeaponMod DefaultMod {
				get => defaultMod;
				set
				{
					if (defaultMod == value) return;
					if(defaultMod != null)
					{
						defaultMod.OnModTreeChanged.RemoveListener(OnActiveModeTreeChange);
					}

					defaultMod = value;
					if (ActiveMod == defaultMod && ParentMod != null)
					{
						ParentMod.OnModTreeChanged.Invoke(this);
					}

					if(defaultMod != null)
					{
						defaultMod.OnModTreeChanged.AddListener(OnActiveModeTreeChange);
					}
				}
			}

			[field: SerializeField, Tooltip("Whether or not mods can be added or removed from this slot")]
			public bool Locked { get; set; } = false;

			[field: SerializeField, Tooltip("Whether or not this slot needs to be filled for the mod to work")]
			public bool Required { get; set; } = false;

			// ------------------------------------------------------------------------------------

			public static Slot CreateForMod(WeaponMod mod) 
			{
				Slot r = new Slot(mod.RequiredSlotType);
				r.EquippedMod = mod;
				r.Locked = true;
				r.Required = false;
				return r;
			}

			// Internal Functionality: ------------------------------------------------------------

			private void OnActiveModeTreeChange(Slot slot)
			{
				if(ParentMod != null) ParentMod.OnModTreeChanged.Invoke(slot);
			}

			// ------------------------------------------------------------------------------------

			/// <summary>
			/// attaches a mod slot to a specific weapon mod. Can only be used once - should only
			/// be used in initialization
			/// </summary>
			/// <param name="mod">the mod to attach the slot to</param>
			public void AttachSlot(WeaponMod mod)
			{
				if (ParentMod == mod) return;
				if (ParentMod != null)
				{
					Debug.LogError(
						"Cannot parent slot to " + mod.name + 
						": slot already has parent " + ParentMod.name
					);
					return;
				}
				ParentMod = mod;
			}

			/// <summary>
			/// the mod that's actively being used for this slot (either default or equipped mod if 
			/// there is one)
			/// </summary>
			public WeaponMod ActiveMod
			{
				get
				{
					return EquippedMod == null ? DefaultMod : EquippedMod;
				}
			}

			/// <summary>
			/// tests to see if the weapon is able to use the mod in this mod slot
			/// </summary>
			/// <param name="wep">the weapon to test the mod's requirements against</param>
			public bool GetIsModUsable(Weapon wep)
			{
				if (ActiveMod == null) return false;
				return (ActiveMod != null) && ActiveMod.TotalWeaponRequirements.MeetsRequirements(wep);
			}
		}
	}
}