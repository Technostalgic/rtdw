﻿using System.Collections;
using UnityEngine;

namespace RTDW.Weapons
{
	public enum WeaponAttributeType
	{
		FiringMode,
		ReloadingMode,
		ProjectileType,
		DamageType
	}

	[System.Serializable]
	public class WeaponAttributeBlueprint
	{
		public WeaponAttributeType type;

		public float precedence;

		[HideInInspector]
		public int enumValue;

		[HideInInspector]
		public Object objReference;

		public WeaponAttribute CreateInstance()
		{
			WeaponAttribute r = null;
			switch (type)
			{
				case WeaponAttributeType.FiringMode:
					r = new WeaponAttribute.FiringMode((Weapon.FireModeType)enumValue);
					break;

				case WeaponAttributeType.ReloadingMode:
					r = new WeaponAttribute.ReloadingMode((Weapon.ReloadModeType)enumValue);
					break;

				case WeaponAttributeType.ProjectileType:
					r = new WeaponAttribute.ProjectileTypeSpec(objReference as Projectile);
					break;

				case WeaponAttributeType.DamageType:
					r = new WeaponAttribute.DamageTypeSpec(objReference as DamageType);
					break;
			}
			if (r == null) return null;
			r.Precedence = precedence;
			return r;
		}
	}
}