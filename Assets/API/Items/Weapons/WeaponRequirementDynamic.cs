﻿using System.Collections;
using UnityEngine;

namespace RTDW.Weapons
{
	[System.Serializable]
	public class WeaponRequirementDynamic
	{
		[Tooltip("Whether or not this requirement is even checked, if false, any weapon will always meet the requirements")]
		public bool useRequirements = false;

		[field: SerializeField]
		public CompareMode RoundsCompareMode { get; private set; }
			= CompareMode.GreaterOrEqual;

		[field: SerializeField]
		public float RequiredRounds { get; private set; } = 0;

		[field: SerializeField]
		public CompareMode EnergyCompareMode { get; private set; }
			= CompareMode.GreaterOrEqual;

		[field: SerializeField]
		public float RequiredEnergy { get; private set; } = 0;

		// ----------------------------------------------------------------------------------------

		public bool MeetsRequirements(Weapon wep)
		{
			if (!useRequirements) return true;
			return (
				RoundsCompareMode.CompareValue(wep.CurrentState.ClipRounds, RequiredRounds) &&
				EnergyCompareMode.CompareValue(wep.CurrentState.currentEnergy, RequiredEnergy)
			);
		}

		public WeaponRequirementDynamic Clone()
		{
			WeaponRequirementDynamic r = new WeaponRequirementDynamic();
			r.RoundsCompareMode = RoundsCompareMode;
			r.EnergyCompareMode = EnergyCompareMode;
			r.RequiredRounds = RequiredRounds;
			r.RequiredEnergy = RequiredEnergy;
			return r;
		}
	}
}