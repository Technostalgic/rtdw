﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Weapons
{
	[Serializable]
	public class StaticTrait : WeaponModTrait
	{
		public StaticTrait(
			WeaponRequirementStatic requirements, WeaponAttribute[] attributes, 
			Stat.Modifier[] modifiers
		) {
			StaticRequirements.ConcatRequirements(requirements);
			base.attributes = attributes;
			Modifiers = modifiers;
		}

		// ----------------------------------------------------------------------------------------

		[field: SerializeField, Tooltip("The stat modifiers that this trait will apply")]
		public Stat.Modifier[] Modifiers { get; private set; } = default;
	}
}