﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Weapons
{
	public abstract class WeaponModTrait
	{
		// ----------------------------------------------------------------------------------------

		protected WeaponAttribute[] attributes = null;

		// ----------------------------------------------------------------------------------------

		[SerializeField]
		public string Label { get; set; } = "Attribute";

		public WeaponRequirementStatic StaticRequirements { get; protected set; } = 
			new WeaponRequirementStatic();

		public IReadOnlyList<WeaponAttribute> Attributes => attributes;
	}
}