﻿using System.Collections;
using UnityEngine;

namespace RTDW.Weapons
{
	public abstract class WeaponModTraitBlueprint : ScriptableObject
	{
		[field: SerializeField]
		public string Label { get; set; } = "Trait";

		public abstract WeaponModTrait CreateInstance(float effectFactor);
	}
}