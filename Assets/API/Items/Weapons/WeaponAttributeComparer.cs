﻿using System.Collections;
using UnityEngine;

namespace RTDW.Weapons
{
	[System.Serializable]
	public class WeaponAttributeComparer
	{
		public WeaponAttributeType type;

		[HideInInspector]
		public int enumValue;

		[HideInInspector]
		public Object objReference;

		// Primary Functionality: -----------------------------------------------------------------

		/// <summary>
		/// returns true if the specified attribute is comparable and has the same value as the 
		/// attribute specified by this comparer
		/// </summary>
		/// <param name="attr">the attribute to test</param>
		public bool IsEqualTo(WeaponAttribute attr)
		{
			switch (attr)
			{
				case WeaponAttribute.FiringMode tatr:
					if (type != WeaponAttributeType.FiringMode) return false;
					return enumValue == (int)tatr.FiringType;

				case WeaponAttribute.ReloadingMode tatr:
					if (type != WeaponAttributeType.ReloadingMode) return false;
					return enumValue == (int)tatr.ReloadType;

				case WeaponAttribute.ProjectileTypeSpec tatr:
					if (type != WeaponAttributeType.ProjectileType) return false;
					return objReference == tatr.ProjectileType;

				case WeaponAttribute.DamageTypeSpec tatr:
					if (type != WeaponAttributeType.DamageType) return false;
					return objReference == tatr.Type;
			}

			return false;
		}

		/// <summary>
		/// returns true if the specified weapon has the has the attribute specified by this 
		/// comparer
		/// </summary>
		/// <param name="wep">the weapon to test</param>
		public bool WeaponHasAttribute(Weapon wep)
		{
			if (wep.SpecialAttributes != null)
			{
				switch (type)
				{
					case WeaponAttributeType.FiringMode:
						if (wep.SpecialAttributes.FiringMode != (Weapon.FireModeType)enumValue)
							return false;
						break;

					case WeaponAttributeType.ReloadingMode:
						if (wep.SpecialAttributes.ReloadMode != (Weapon.ReloadModeType)enumValue)
							return false;
						break;

					case WeaponAttributeType.ProjectileType:
						if (wep.SpecialAttributes.ProjectileType != objReference)
							return false;
						break;

					case WeaponAttributeType.DamageType:
						DamageType dtype = objReference as DamageType;
						if (wep.SpecialAttributes.DamageTypeData[dtype] <= 0)
							return false;
						break;
				}
			}

			return true;
		}
	}
}