using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Weapons
{
	[Serializable]
	public class WeaponRequirementStatic
	{
		// Temporary Query Variables: -------------------------------------------------------------

		private readonly static List<StatRequirement> qStatReqList = 
			new List<StatRequirement>();

		private readonly static List<WeaponAttributeComparer> qAttrList = 
			new List<WeaponAttributeComparer>();

		// Requirement Parameters: ----------------------------------------------------------------

		[field: SerializeField]
		public StatRequirement[] StatRequirements { get; private set; } = null;

		[field: SerializeField]
		public WeaponAttributeComparer[] AttributeRequirements { get; private set; } = null;

		public bool HasAnyRequirement => 
			(StatRequirements != null && StatRequirements.Length > 0) &&
			(AttributeRequirements != null && AttributeRequirements.Length > 0);

		// Primary Functionality: -----------------------------------------------------------------

		public bool MeetsRequirements(Weapon wep)
		{
			if (StatRequirements != null)
			{
				foreach (StatRequirement req in StatRequirements)
				{
					if (!req.MeetsRequirement(wep.Stats)) return false;
				}
			}

			if(AttributeRequirements != null)
			{
				foreach (var req in AttributeRequirements)
				{
					if (!req.WeaponHasAttribute(wep)) return false;
				}
			}

			return true;
		}

		// Internal Tests: ------------------------------------------------------------------------

		/// <summary>
		/// whether or not this requirement set requires the specified test
		/// </summary>
		/// <param name="test">the requirement to test if it's already contained within this 
		///		weapon requirement set</param>
		private bool HasStatRequirement(StatRequirement test)
		{
			if (StatRequirements == null) return false;

			for (int i = StatRequirements.Length - 1; i >= 0; i--)
			{
				if (StatRequirement.IsEquavalent(StatRequirements[i], test)) return true;
			}

			return false;
		}

		// Mutation: ------------------------------------------------------------------------------

		/// <summary>
		/// Combine multiple requirement sets into this one
		/// </summary>
		public void ConcatMultipleRequirements(params WeaponRequirementStatic[] from)
		{
			foreach (WeaponRequirementStatic req in from)
			{
				ConcatRequirements(req);
			}
		}

		/// <summary>
		/// Appends the requirements from the specified object into this one so it contains both 
		/// sets of requirements
		/// </summary>
		/// <param name="from">the weapon requirements to add to this one</param>
		public void ConcatRequirements(WeaponRequirementStatic from)
		{
			if (from == null || !from.HasAnyRequirement) return;

			if (from.StatRequirements != null && from.StatRequirements.Length > 0)
			{
				qStatReqList.Clear();
				qStatReqList.AddRange(StatRequirements);
				for (int i = from.StatRequirements.Length - 1; i >= 0; i--)
				{
					if (HasStatRequirement(from.StatRequirements[i])) continue;
					qStatReqList.Add(from.StatRequirements[i]);
				}
				StatRequirements = qStatReqList.ToArray();
			}

			if(from.AttributeRequirements != null && from.AttributeRequirements.Length > 0)
			{
				qAttrList.Clear();
				qAttrList.AddRange(AttributeRequirements);
				for(int i = from.AttributeRequirements.Length - 1; i >= 0; i--)
				{
					qAttrList.Add(from.AttributeRequirements[i]);
				}
				AttributeRequirements = qAttrList.ToArray();
			}
		}

		/// <summary>
		/// removes all requirements from the set
		/// </summary>
		public void ClearRequirements()
		{
			StatRequirements = null;
			AttributeRequirements = null;
		}

		// SubType Definitions: -------------------------------------------------------------------

		[Serializable]
		public struct StatRequirement
		{
			public Stat stat;

			public float value;

			public CompareMode compareMode;

			// ------------------------------------------------------------------------------------

			public bool MeetsRequirement(EntityStats stats)
			{
				switch (compareMode)
				{
					case CompareMode.LessOrEqual: return stats[stat].Value <= value;
					case CompareMode.GreaterOrEqual: return stats[stat].Value >= value;
					case CompareMode.Less: return stats[stat].Value < value;
					case CompareMode.Greater: return stats[stat].Value > value;
					case CompareMode.Equal: return stats[stat].Value == value;
				}

				return false;
			}

			public string GetLabel()
			{
				string comparator = default;
				switch (compareMode)
				{
					case CompareMode.Greater: comparator = ">"; break;
					case CompareMode.GreaterOrEqual: comparator = ">="; break;
					case CompareMode.Less: comparator = "<"; break;
					case CompareMode.LessOrEqual: comparator = "<="; break;
					case CompareMode.Equal: comparator = "="; break;
				}

				string str = stat.Label + " " + comparator + " " + value.ToString("0.00");
				return str;
			}

			// ------------------------------------------------------------------------------------

			/// <summary>
			/// whether or not the stat requirements are comparing the same condition
			/// </summary>
			public static bool IsEquavalent(in StatRequirement a, in StatRequirement b)
			{
				return (
					a.stat == b.stat &&
					a.compareMode == b.compareMode &&
					a.value == b.value
				);
			}
		}
	}
}
