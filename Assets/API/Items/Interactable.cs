using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RTDW
{
	public class Interactable : MonoBehaviour
	{
		[field: SerializeField]
		public bool AutoInteract { get; private set; }

		[field: SerializeField]
		public OnInteractEvent OnInteract { get; private set; }

		// --------------------------------------------------------------------------------------------

		public void Interact(GameObject interactor)
		{
			OnInteract.Invoke(interactor);
		}
	}

	[System.Serializable]
	public class OnInteractEvent : UnityEvent<GameObject> { }
}