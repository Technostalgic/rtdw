using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	public interface IStorageItemTarget
	{
		public StorageItem StorageItem { get; }
		public Object Self { get; }
	}

	public interface IStorageItemValue
	{
		public int GetGoldValue();
	}

	[System.Serializable]
	public class StorageItem
	{
		public static readonly StorageItem[] qStorageItemSingleArray = new StorageItem[1];
		
		// --------------------------------------------------------------------------------------------

		public StorageItem(IStorageItemTarget item)
		{
			if (item != null)
			{
				AttachToObject(item);
				Label = item.Self.name;
			}
		}

		[field: SerializeField]
		public Sprite Icon { get; set; } = null;

		[field: SerializeField]
		public Sprite IconBackground { get; set; } = null;

		[field: SerializeField]
		public Color IconColor { get; set; } = Color.white;

		[field: SerializeField]
		public Color IconBackgroundColor { get; set; } = Color.clear;

		[field: SerializeField]
		public string Label { get; set; } = "Item";

		[field: SerializeField, TextArea]
		public string Description { get; set; } = "Item Description";

		public IStorageItemTarget ItemTarget
		{
			get
			{
				if (itemTarget_object != null) return itemTarget_object as IStorageItemTarget;
				else if (itemTarget_gameObject != null)
					return itemTarget_gameObject.GetComponent<IStorageItemTarget>();
				return null;
			}
			private set
			{
				if (value == null)
				{
					itemTarget_gameObject = null;
					itemTarget_object = null;
					return;
				}
				if (value.Self is GameObject go) itemTarget_gameObject = go;
				else itemTarget_object = value.Self;
			}
		}

		[SerializeField, HideInInspector]
		private Object itemTarget_object = null;

		[SerializeField, HideInInspector]
		private GameObject itemTarget_gameObject = null;

		// --------------------------------------------------------------------------------------------
		
		/// <summary> Calculates the gold value that an item is worth </summary>
		public static int CalculateGoldValue(StorageItem item)
		{
			if (item.ItemTarget is IStorageItemValue val) return val.GetGoldValue();
			return 0;
		}

		// --------------------------------------------------------------------------------------------

		/// <summary>
		/// Attach the storage item to an object and return self if valid, otherwise return null
		/// </summary>
		/// <param name="ob"></param>
		public StorageItem AttachToObject(IStorageItemTarget ob)
		{
			if (ItemTarget != null && ItemTarget != ob) return null;
			ItemTarget = ob;
			return this;
		}

		/// <summary>
		/// copies the visual storage fields from another storage item
		/// </summary>
		/// <param name="from">the item to copy the fields from</param>
		public void CopyFrom(StorageItem from) 
		{
			Icon = from.Icon;
			IconBackground = from.IconBackground;
			IconColor = from.IconColor;
			IconBackgroundColor = from.IconBackgroundColor;
			Label = from.Label;
			Description = from.Description;
		}

		/// <summary> Get the gold value that the item is worth </summary>
		public int GetGoldValue() => CalculateGoldValue(this);

		public IReadOnlyList<StorageItem> ToSingleArray()
		{
			qStorageItemSingleArray[0] = this;
			return qStorageItemSingleArray;
		}
	}
}