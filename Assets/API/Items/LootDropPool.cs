﻿using System.Collections;
using UnityEngine;

namespace RTDW
{
	[CreateAssetMenu(fileName = "LootDropPool", menuName = "RTDW/Loot Drop Pool")]
	public class LootDropPool : ScriptableObject
	{
		public LootDrop[] drops = null;

		public virtual void DropLoot(Vector2 position, Vector2 velocity)
		{
			float spread = 10;
			foreach (LootDrop drop in drops)
			{
				if(Random.value < drop.chance)
				{
					if (drop.Item is StackableItem stackable)
					{
						ItemPickup pickup = ObjectPooling.ItemPickupPool.GetObject();
						StackableItem instance = ObjectPooling.StackableItemPool.GetObject();
						instance.Set(stackable, drop.count);
						pickup.Item = instance.StorageItem;
						pickup.gameObject.SetActive(true);

						Vector2 velOff = Random.insideUnitCircle * spread;
						pickup.Rigidbody2d.velocity = velocity + velOff;
						pickup.transform.position = position;
					}
					else
					{
						for(int i = drop.count; i > 0; i--)
						{
							ItemPickup pickup = ObjectPooling.ItemPickupPool.GetObject();
							pickup.Item = drop.Item.StorageItem;
							pickup.gameObject.SetActive(true);

							Vector2 velOff = Random.insideUnitCircle * spread;
							pickup.Rigidbody2d.velocity = velocity + velOff;
							pickup.transform.position = position;
						}
					}
				}
			}
		}

		[System.Serializable]
		public struct LootDrop
		{
			[SerializeField]
			private Object item;

			[Min(1)]
			public int count;

			[Range(0, 1)]
			public float chance;

			public IStorageItemTarget Item => item is GameObject go ?
				go.GetComponent<IStorageItemTarget>() :
				item as IStorageItemTarget;
		}
	}
}