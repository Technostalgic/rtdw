using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RTDW
{
	public class StorageContainer : MonoBehaviour
	{
		[SerializeField, HideInInspector]
		private List<StorageItem> storedItems = new List<StorageItem>();
		public virtual IReadOnlyList<StorageItem> AllStoredItems => storedItems;

		public readonly StorageContainerEvent OnItemsAdded = new StorageContainerEvent();
		public readonly StorageContainerEvent OnItemsRemoved = new StorageContainerEvent();
		public readonly StorageContainerEvent OnItemsModified = new StorageContainerEvent();

		// Property Defintions: -------------------------------------------------------------------------

		/// <summary>
		/// returns the total number of items in the storage container, note that each stack of stacked
		/// items only counts as a single item, even if the stack size is greater than 1
		/// </summary>
		public virtual int TotalItemCount
		{
			get => storedItems.Count;
		}

		// Primary Functionality: -----------------------------------------------------------------------

		/// <summary>
		/// whether the item can be added to the inventory or not
		/// </summary>
		/// <param name="item">the item to test</param>
		public bool CanAddItem(StorageItem item)
		{
			if (item.ItemTarget is StackableItem stackable)
			{
				if (CanAddUniqueItem(item)) return true;
				return GetItemStack(stackable) != null;
			}
			else
			{
				return CanAddUniqueItem(item);
			}
		}

		/// <summary>
		/// Adds the specified items to the storage container
		/// </summary>
		/// <param name="item">the item to add, can specify multiple</param>
		public void AddItem(params StorageItem[] item)
		{
			List<StorageItem> added = new List<StorageItem>();

			for (int i = 0; i < item.Length; i++)
			{
				if (!CanAddItem(item[i])) continue;
				if(item[i].ItemTarget is StackableItem stackAddition)
				{
					StackableItem stack = GetItemStack(stackAddition.ItemAsset);
					if (stack != null)
					{
						stack.StackSize += stackAddition.StackSize;
						added.Add(stack.StorageItem);
						continue;
					}
				}
				AddUniqueItem(item[i]);
				added.Add(item[i]);
			}

			OnItemsAdded?.Invoke(added);
		}

		/// <summary>
		/// Gets the item stack of the item asset if it has a stack in the container
		/// </summary>
		/// <param name="itemAsset">
		///		the asset that the stackable item is based off of, 
		///		not the stackable item stack itself
		///	</param>
		public StackableItem GetItemStack(StackableItem itemAsset)
		{
			foreach (var itm in AllStoredItems)
			{
				if (itm.ItemTarget is StackableItem stackable)
				{
					if (stackable.ItemAsset == itemAsset) return stackable;
				}
			}
			return null;
		}

		/// <summary> remove items at specified indices </summary>
		/// <param name="index">the indices of the items in the stored items array</param>
		public virtual void RemoveItemAt(params int[] index)
		{
			List<StorageItem> removed = new List<StorageItem>();
			for (int i = 0; i < index.Length; i++)
			{
				int ind = index[i];
				if (storedItems.Count <= ind || ind < 0) continue;

				removed.Add(storedItems[ind]);
				storedItems.RemoveAt(ind);
			}
			OnItemsRemoved?.Invoke(removed);
		}

		/// <summary>
		/// Clears all the items from the storage container
		/// </summary>
		public void ClearAllItems()
		{
			storedItems.Clear();
		}

		/// <summary>
		/// attempt to remove item from the container, return true if successful, otherwise false
		/// </summary>
		/// <param name="item"> the item to remove </param>
		public void RemoveItem(params StorageItem[] item)
		{
			int[] indices = new int[item.Length];
			for (int i = item.Length - 1; i >= 0; i--) indices[i] = storedItems.IndexOf(item[i]);
			RemoveItemAt(indices);
		}

		/// <summary> Check to see if the storage container contains a specific item instance </summary>
		/// <param name="item">the item to look for</param>
		public bool ContainsItem(StorageItem item) => storedItems.Contains(item);

		/// <summary>
		/// get the index of a specific item instance
		/// </summary>
		/// <param name="item"> the item to look for </param>
		public int IndexOfItem(StorageItem item) => storedItems.IndexOf(item);

		/// <summary> create new instances of each inventory item </summary>
		public void CloneInventory()
		{
			List<StorageItem> items = storedItems;
			storedItems = new List<StorageItem>();

			foreach (StorageItem item in items)
			{
				IStorageItemTarget clone = Instantiate(item.ItemTarget.Self) as IStorageItemTarget;
				clone.Self.name = item.ItemTarget.Self.name;
				storedItems.Add(clone.StorageItem);
			}
		}

		// Internal Functionality: ----------------------------------------------------------------------

		/// <summary> check to see if the specified item can be added in the container </summary>
		/// <param name="item">the item to check</param>
		protected virtual bool CanAddUniqueItem(StorageItem item)
		{
			if (item?.ItemTarget == null) return false;
			int index = storedItems.IndexOf(item);
			if (index >= 0) return false;
			return true;
		}

		/// <summary> store the specified items in the container </summary>
		/// <param name="item">the items to store</param>
		protected virtual void AddUniqueItem(params StorageItem[] item)
		{
			for (int i = 0; i < item.Length; i++)
			{
				StorageItem itm = item[i];
				storedItems.Add(itm);
			}
		}

		// Unity Messages: ------------------------------------------------------------------------------

		private void Start()
		{
			CloneInventory();
			OnItemsAdded.AddListener(OnItemsModified.Invoke);
			OnItemsRemoved.AddListener(OnItemsModified.Invoke);
		}
	}

	[System.Serializable]
	public class StorageContainerEvent : UnityEvent<IReadOnlyList<StorageItem>> { }
}