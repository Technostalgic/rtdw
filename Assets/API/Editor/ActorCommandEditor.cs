using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace RTDW.Editor.Actor
{
	[CustomPropertyDrawer(typeof(RTDW.Actors.Command))]
	public class ActorCommandEditor : PropertyDrawer
	{
		private bool expanded = true;

		// ----------------------------------------------------------------------------------------------

		private void DrawPropertyBody(Rect position, SerializedProperty property)
		{
			float lineHeight = EditorGUIUtility.singleLineHeight;
			float lineSpace = EditorGUIUtility.standardVerticalSpacing;

			int oindent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			float leftWidth = 150;
			float rightX = position.x + leftWidth + 5;
			float rightWidth = position.width - leftWidth - 25;

			Rect KeyLblRect = new Rect(position.x, position.y, leftWidth, lineHeight);
			Rect KeyValRect = new Rect(position.x, position.y + lineHeight + lineSpace, leftWidth, lineHeight);
			Rect ParamTypeRect = new Rect(position.x, KeyValRect.y + lineHeight + lineSpace, leftWidth, lineHeight);

			Rect EventRect = new Rect(rightX, position.y - lineHeight, rightWidth, position.height);

			EditorGUI.LabelField(KeyLblRect, "Command Key");
			string evtType = "";
			property.Next(true);
			do
			{
				switch (property.propertyType)
				{
					case SerializedPropertyType.String:
						EditorGUI.PropertyField(KeyValRect, property, new GUIContent());
						break;
					case SerializedPropertyType.Enum:
						EditorGUI.PropertyField(ParamTypeRect, property, new GUIContent());
						evtType = "";
						switch ((Actors.Command.ParameterType)property.enumValueIndex)
						{
							case Actors.Command.ParameterType.Void:
								evtType = typeof(Actors.Command.CommandVoidAction).Name.ToLower();
								break;
							case Actors.Command.ParameterType.Analogue:
								evtType = typeof(Actors.Command.CommandAnalogueAction).Name.ToLower();
								break;
							case Actors.Command.ParameterType.Vector:
								evtType = typeof(Actors.Command.CommandVectorAction).Name.ToLower();
								break;
							case Actors.Command.ParameterType.Agnostic:
								evtType = typeof(Actors.Command.CommandAgnosticAction).Name.ToLower();
								break;
						}
						break;
					case SerializedPropertyType.Generic:
						string ptype = property.type.ToLower();
						if (ptype == evtType)
						{
							GUIContent content = new GUIContent(property.type);
							EditorGUI.PropertyField(EventRect, property, content);
						}
						break;
				}
			} while (property.Next(false));

			EditorGUI.indentLevel = oindent;
		}

		// ----------------------------------------------------------------------------------------------

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			float lineHeight = EditorGUIUtility.singleLineHeight;

			EditorGUI.BeginProperty(position, label, property);
			Rect position2 = position;
			position2.x += 5;
			position2.width -= 5;
			EditorGUI.PrefixLabel(position2, GUIUtility.GetControlID(FocusType.Passive), label);

			expanded = EditorGUI.Foldout(new Rect(position.x + 5, position.y, 10, lineHeight), expanded, "");
			if (expanded)
			{
				position2.x = position.x;
				position2.x += 10;
				position2.y += lineHeight + EditorGUIUtility.standardVerticalSpacing;
				position2.height -= lineHeight + EditorGUIUtility.standardVerticalSpacing;
				DrawPropertyBody(position2, property);
			}

			EditorGUI.EndProperty();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			int lineCount = 1;

			if (expanded) lineCount += 4;

			return 
				lineCount * EditorGUIUtility.singleLineHeight + 
				EditorGUIUtility.standardVerticalSpacing * (lineCount - 1);
		}
	}
}
