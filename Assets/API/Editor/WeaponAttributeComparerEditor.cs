﻿using System.Collections;
using UnityEngine;
using UnityEditor;
using RTDW.Weapons;

namespace RTDW.Editor
{
	[CustomPropertyDrawer(typeof(WeaponAttributeComparer))]
	public class WeaponAttributeComparerEditor : PropertyDrawer
	{

		// ----------------------------------------------------------------------------------------

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);

			float vspace = EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

			// define property rects:
			Rect firstRect = position;
			Rect thirdRect = position;

			firstRect.width *= 0.3f;
			thirdRect.x = firstRect.xMax;
			thirdRect.width *= 0.65f;

			// find the specified weapon attribute type
			WeaponAttributeType attrType = WeaponAttributeType.FiringMode;
			property.Next(true);
			int desiredDepth = property.depth;
			do
			{
				switch (property.propertyType)
				{
					case SerializedPropertyType.Enum:
						attrType = (WeaponAttributeType)EditorGUI.EnumPopup(
							firstRect, (WeaponAttributeType)property.enumValueIndex
						);
						property.enumValueIndex = (int)attrType;
						break;

					// case SerializedPropertyType.Float:
					// 	property.floatValue = EditorGUI.FloatField(secondRect, property.floatValue);
					// 	break;

					case SerializedPropertyType.Integer:
						switch (attrType)
						{
							case WeaponAttributeType.FiringMode:
								property.intValue = (int)(Weapon.FireModeType)EditorGUI.EnumPopup(
									thirdRect, (Weapon.FireModeType)property.intValue
								);
								break;
							case WeaponAttributeType.ReloadingMode:
								property.intValue = (int)(Weapon.FireModeType)EditorGUI.EnumPopup(
									thirdRect, (Weapon.ReloadModeType)property.intValue
								);
								break;
						}
						break;

					case SerializedPropertyType.ObjectReference:
						switch (attrType)
						{
							case WeaponAttributeType.ProjectileType:
								property.objectReferenceValue = EditorGUI.ObjectField(
									thirdRect, property.objectReferenceValue,
									typeof(Projectile), false
								);
								break;
							case WeaponAttributeType.DamageType:
								property.objectReferenceValue = EditorGUI.ObjectField(
									thirdRect, property.objectReferenceValue,
									typeof(DamageType), false
								);
								break;
						}
						break;
				}
			} while (property.Next(false) && property.depth == desiredDepth);

			EditorGUI.EndProperty();
		}
	}
}