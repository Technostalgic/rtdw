using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace RTDW.Editor
{
	[CustomEditor(typeof(ItemPickup))]
	public class ItemPickupEditor : UnityEditor.Editor
	{
		public ItemPickup Target => target as ItemPickup;
		public ItemPickup[] Targets => targets as ItemPickup[];

		// ----------------------------------------------------------------------------------------

		public static void CreateItemFieldGUI(ItemPickupEditor instance)
		{
			ItemPickup targ = instance.Target;
			void LogWarning()
			{
				Debug.LogWarning(
					"Only Objects that implement IStorageItemTarget or GameObjects with a " +
					"component that implements IStorageItem are accepted"
				);
			}

			GUIContent itmFieldContent = new GUIContent("Item", "The item that will be stored");
			Object itmSet = 
				EditorGUILayout.ObjectField(
					itmFieldContent,
					targ.Item?.ItemTarget?.Self,
					typeof(Object), 
					true
				);

			if(itmSet != targ.Item?.ItemTarget?.Self)
			{
				if (itmSet == null) targ.Item = null;
				else if (itmSet is IStorageItemTarget sit)
				{
					targ.Item = sit.StorageItem;
					EditorUtility.SetDirty(targ);
				}
				else if (itmSet is GameObject go)
				{
					sit = go.GetComponent<IStorageItemTarget>();
					if (sit != null)
					{
						targ.Item = sit.StorageItem;
						EditorUtility.SetDirty(targ);
					}
					else LogWarning();
				}
				else LogWarning();
			}
		}

		// ----------------------------------------------------------------------------------------

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			CreateItemFieldGUI(this);
		}
	}
}
