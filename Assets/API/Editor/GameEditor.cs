using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace RTDW.Editor
{
	public class GameEditor : UnityEditor.Editor
	{
		/// <summary>
		/// Match the BoxCollider Component to have the same size as the SpriteRenderer's size 
		/// property in the scene
		/// </summary>
		[MenuItem("GameObject/RTDW/Match BoxColliders to SpriteRenderer", priority = 10)]
		public static void MatchBoxCollidersToSprites()
		{
			Object[] selectedObjs = Selection.objects;
			HashSet<GameObject> modified = new HashSet<GameObject>();
			for (int i = selectedObjs.Length - 1; i >= 0; i--)
			{
				GameObject go = selectedObjs[i] as GameObject;
				if(go == null && selectedObjs[i] is Component comp) go = comp.gameObject;
				if (go == null) continue;

				if (modified.Contains(go)) continue;

				BoxCollider2D bc = go.GetComponent<BoxCollider2D>();
				SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
				if (bc == null || sr == null) continue;

				bc.size = new Vector2(
					sr.size.x - bc.edgeRadius * 2,
					sr.size.y - bc.edgeRadius * 2
				);

				EditorUtility.SetDirty(bc);
				modified.Add(go);
			}
		}

		// ----------------------------------------------------------------------------------------
	}
}
