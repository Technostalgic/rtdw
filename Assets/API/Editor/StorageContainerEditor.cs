using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace RTDW.Editor
{
	[CustomEditor(typeof(StorageContainer))]
	public class StorageContainerEditor : UnityEditor.Editor
	{
		public StorageContainer Target => target as StorageContainer;
		public StorageContainer[] Targets => targets as StorageContainer[];

		public DragAndDrop dd = new DragAndDrop();

		public bool IsListExpanded { get; protected set; } = true;

		// ----------------------------------------------------------------------------------------

		public static void CreateContainerListUI(StorageContainerEditor instance)
		{
			GUIContent foldoutContent = new GUIContent(
				"Container Items", 
				"Expand to view or modify container contents"
			);
			instance.IsListExpanded = EditorGUILayout.Foldout(
				instance.IsListExpanded, 
				foldoutContent
			);

			if (instance.IsListExpanded)
			{
				StorageContainer targ = instance.Target;
				string wrongTypeWarning = " could not be added to the container, " +
					"please make sure it implements " + typeof(IStorageItemTarget).Name +
					", or that it has a component on it that does";

				(int, IStorageItemTarget) toReplace = (-1, null);
				for(int i = 0; i < targ.AllStoredItems.Count; i++)
				{ 
					StorageItem si = targ.AllStoredItems[i];
					int buttonSz = 16;
					Rect itemRect = EditorGUILayout.GetControlRect(false, buttonSz);
					Rect buttonRect = new Rect(itemRect);
					buttonRect.size = new Vector2(buttonSz, buttonSz);
					itemRect.width -= buttonSz;
					itemRect.x += buttonSz + 4;
					GUIContent itemContent = new GUIContent(si.Label, si.Description);
					EditorGUI.LabelField(itemRect, itemContent);
					if (GUI.Button(buttonRect, "�"))
					{
						targ.RemoveItemAt(i--);
						EditorUtility.SetDirty(targ);
					}
				}

				if(toReplace.Item1 >= 0)
				{
					targ.RemoveItemAt(toReplace.Item1);
					targ.AddItem(toReplace.Item2.StorageItem);
				}

				GUIContent addedContent = new GUIContent("Add Items:");
				Object addedObject = 
					EditorGUILayout.ObjectField(
						addedContent, 
						null, 
						typeof(Object), 
						false
					);

				if(addedObject != null)
				{
					Object[] obs = DragAndDrop.objectReferences;
					if (obs.Length < 1 || System.Array.IndexOf(obs, addedObject) < 0)
						obs = new Object[] { addedObject };

					for(int i = 0; i < obs.Length; i++)
					{
						IStorageItemTarget sit = obs[i] as IStorageItemTarget;
						if (sit == null && obs[i] is GameObject go) 
							sit = go.GetComponent<IStorageItemTarget>();
						if (sit != null) 
							targ.AddItem(sit.StorageItem);
						EditorUtility.SetDirty(targ);
					}
				}
			}
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			CreateContainerListUI(this);
		}
	}
}
