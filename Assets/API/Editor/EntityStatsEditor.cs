using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace RTDW.Editor
{
	[CustomEditor(typeof(EntityStats))]
	public class EntityStatsEditor : UnityEditor.Editor
	{
		public EntityStats Target => target as EntityStats;

		private bool IsValueListExpanded { get; set; } = false;

		// ----------------------------------------------------------------------------------------

		public static void StatValuesGUI(EntityStatsEditor instance)
		{

			GUIContent foldoutContent = new GUIContent(
				"Stat Base Values",
				"Expand to view or modify the stat values"
			);

			instance.IsValueListExpanded = EditorGUILayout.Foldout(
				instance.IsValueListExpanded,
				foldoutContent
			);

			if (instance.IsValueListExpanded)
			{
				EntityStats targ = instance.Target;
				EntityStats.Friend tfriend = new EntityStats.Friend(targ);
				bool modified = false;

				EditorGUI.indentLevel++;
				for (int i = 0; i < tfriend.SerializedValues.Length; i++)
				{
					EntityStats.StatValue sval = tfriend.SerializedValues[i];
					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.LabelField(sval.Stat.Label);
					modified = modified || (sval.BaseValue != 
						(sval.BaseValue = EditorGUILayout.FloatField(sval.BaseValue)));
					modified = modified || (sval.MinValue !=
						(sval.MinValue = EditorGUILayout.FloatField(sval.MinValue)));
					modified = modified || (sval.MaxValue !=
						(sval.MaxValue = EditorGUILayout.FloatField(sval.MaxValue)));
					EditorGUILayout.EndHorizontal();
				}
				EditorGUI.indentLevel--;
				if (modified) EditorUtility.SetDirty(targ);

				GUILayout.BeginHorizontal();
				if (GUILayout.Button("Clear Values")) ClearValues(tfriend);
				if (targ.StatSet != null)
					if(GUILayout.Button("Add StatSet Values")) AddValuesFromSet(tfriend);
				GUILayout.EndHorizontal();
			}
		}

		public static void ClearValues(EntityStats.Friend targFriend)
		{
			targFriend.SerializedValues = new EntityStats.StatValue[] { };
			targFriend.Target.StatValues.Clear();
			EditorUtility.SetDirty(targFriend.Target);
		}

		public static void AddValuesFromSet(EntityStats.Friend targFriend)
		{
			List<EntityStats.StatValue> vals = new List<EntityStats.StatValue>();
			foreach(Stat stat in targFriend.Target.StatSet.AllStats)
			{
				if (stat == null) continue;
				EntityStats.StatValue sval = new EntityStats.StatValue(targFriend.Target, stat, 0);
				vals.Add(sval);
			}
			targFriend.SerializedValues = vals.ToArray();
			EditorUtility.SetDirty(targFriend.Target);
			targFriend.Target.OnAfterDeserialize();
		}

		// ----------------------------------------------------------------------------------------

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			StatValuesGUI(this);
		}
	}
}
