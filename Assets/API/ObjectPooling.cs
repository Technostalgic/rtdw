﻿using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	public static class ObjectPooling
	{
		public static Pool<ItemPickup> ItemPickupPool { get; private set; } = null;

		public static Pool<StackableItem> StackableItemPool { get; private set; } = null;

		public static void Initialize()
		{
			ItemPickupPool = new Pool<ItemPickup>(
				((GameObject)GameManager.InitParams.itemPickupPool.template).GetComponent<ItemPickup>(),
				GameManager.InitParams.itemPickupPool.initialPoolCount,
				GameManager.InitParams.itemPickupPool.instanceName
			);

			StackableItemPool = new Pool<StackableItem>(
				ScriptableObject.CreateInstance<StackableItem>(),
				10, "StackableItem"
			);
		}

		[System.Serializable]
		public class Pool<T> where T : Object
		{
			public Pool(T template, int initialCount, string name = "PooledObject")
			{
				objectTemplate = template;
				for (int i = initialCount; i > 0; i--)
				{
					T obj = GetObject();
					if (obj is GameObject go) go.SetActive(false);
					else if (obj is Component comp) comp.gameObject.SetActive(false);
					PoolObject(obj);
				}
				instanceName = name;
			}

			[SerializeField]
			protected T objectTemplate = null;

			[SerializeField]
			protected string instanceName = "PooledObject";

			private uint uid = 0;

			private Queue<T> pooledObjects = new Queue<T>();

			/// <summary>
			/// pulls an object from the object pool if there is any available, otherwise 
			/// instantiates a new one
			/// </summary>
			public T GetObject()
			{
				if (pooledObjects.Count > 0) return pooledObjects.Dequeue();

				T obj = Object.Instantiate(objectTemplate);
				obj.name = instanceName + "-" + uid++;

				return obj;
			}

			public void PoolObject(T obj)
			{
				pooledObjects.Enqueue(obj);
			}
		}

		[System.Serializable]
		public class PoolInitProperties
		{
			[Tooltip("The template object to use for the pool")]
			public Object template = null;

			[Tooltip("The name given to the objects by the pool when they are created")]
			public string instanceName = "PooledObject";

			[Tooltip("The amount of objects that the pool will start out with")]
			public int initialPoolCount = 5;
		}
	}
}