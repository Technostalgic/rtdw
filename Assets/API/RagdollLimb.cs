using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Rigidbody2D))]
	public class RagdollLimb : MonoBehaviour
	{
		[field: SerializeField]
		public Rigidbody2D Rigidbody { get; protected set; }

		[field: SerializeField]
		public SpriteRenderer SpriteRenderer { get; protected set; }

		[field: SerializeField]
		public Collider2D Collider { get; protected set; }

		// ----------------------------------------------------------------------------------------

		/// <summary>
		/// creates a limb component on the specified target and returns it
		/// </summary>
		/// <param name="limb">the game object to add the component to</param>
		/// <param name="bone">the original bone that this limb is supposed to represent</param>
		/// <param name="rb">the rigidbody on this gameobject</param>
		/// <param name="sr">the sprite renderer on this gameobject</param>
		/// <param name="col">the collider on this gameobject</param>
		/// <returns></returns>
		public static RagdollLimb CreateLimb(GameObject limb, Rigidbody2D rb, SpriteRenderer sr, Collider2D col)
		{
			RagdollLimb r = limb.GetComponent<RagdollLimb>();
			if (r == null) r = limb.AddComponent<RagdollLimb>();

			r.Rigidbody = rb;
			r.SpriteRenderer = sr;
			r.Collider = col;

			return r;
		}

		// ----------------------------------------------------------------------------------------

		/// <summary>
		/// returns true if the specified object is elligible to be made a limb for a ragdoll
		/// </summary>
		/// <param name="limb">the limb to check</param>
		public static bool EligibleForRagdollLimb(Transform limb)
		{
			return (
				limb.GetComponent<SpriteRenderer>() != null ||
				limb.GetComponent<Collider2D>() != null
			);
		}

		// ----------------------------------------------------------------------------------------

		/// <summary> Matches this limb to the pose of the target bone </summary>
		public void MatchTargetBoneTransform(Transform targetBone)
		{
			Vector2 localPos = targetBone.localPosition;
			transform.localPosition.Set(localPos.x, localPos.y, transform.localPosition.z);
			transform.localRotation = targetBone.localRotation;
			transform.localScale = targetBone.localScale;
		}

		// ----------------------------------------------------------------------------------------

		private void OnEnable()
		{
			if (SpriteRenderer != null)
			{

			}
		}
	}
}
