﻿using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Terrain
{
	[RequireComponent(typeof(PolygonCollider2D))]
	public class TerrainChunk : MonoBehaviour
	{
		private delegate int ConfigurationAction(
			in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid,
			ref LineSegment[] outSegments
		);

		private static readonly ConfigurationAction[] configActions = new ConfigurationAction[]
		{
			// 0 - square is completely empty
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				return 0;
			},
			// 1 - bottom left corner
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				outSegments[0].start = lmid;
				outSegments[0].end = bmid;
				return 1;
			},
			// 2 - bottom right corner
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				outSegments[0].start = rmid;
				outSegments[0].end = bmid;
				return 1;
			},
			// 3 - bottom half
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				outSegments[0].start = lmid;
				outSegments[0].end = rmid;
				return 1;
			},
			// 4 - top right corner
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				outSegments[0].start = rmid;
				outSegments[0].end = tmid;
				return 1;
			},
			// 5 - top left and bottom right corners
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				outSegments[0].start = tmid;
				outSegments[0].end = lmid;
				outSegments[1].start = bmid;
				outSegments[1].end = rmid;
				return 2;
			},
			// 6 - right half
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				outSegments[0].start = tmid;
				outSegments[0].end = bmid;
				return 1;
			},
			// 7 - all corners except top left
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				outSegments[0].start = lmid;
				outSegments[0].end = tmid;
				return 1;
			},
			// 8 - top left corner
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				outSegments[0].start = tmid;
				outSegments[0].end = lmid;
				return 1;
			},
			// 9 - left half
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				outSegments[0].start = bmid;
				outSegments[0].end = tmid;
				return 1;
			},
			// 10 - top right and bottom left corners
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				outSegments[0].start = rmid;
				outSegments[0].end = tmid;
				outSegments[1].start = lmid;
				outSegments[1].end = bmid;
				return 2;
			},
			// 11 - all corners except top right
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				outSegments[0].start = tmid;
				outSegments[0].end = rmid;
				return 1;
			},
			// 12 - top half
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				outSegments[0].start = rmid;
				outSegments[0].end = lmid;
				return 1;
			},
			// 13 - all corners except bottom right
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				outSegments[0].start = rmid;
				outSegments[0].end = bmid;
				return 1;
			},
			// 14 - all corners except bottom left
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				outSegments[0].start = bmid;
				outSegments[0].end = lmid;
				return 1;
			},
			// 15 - square is completely full
			(in Vector2 tmid, in Vector2 lmid, in Vector2 bmid, in Vector2 rmid, ref LineSegment[] outSegments) => {
				return 0;
			}
		};

		private static readonly float threshold = 0.5f;

		private static readonly float epsilon = 0.001f;

		private static readonly bool interpolate = true;

		private static readonly Vector2 chunkSize = new Vector2(4, 4);

		// ----------------------------------------------------------------------------------------

		public MarchingSquares.ScalarField scalarField = null;

		private readonly List<LineSegment> allLineSegments = new List<LineSegment>();

		private readonly List<Path> allPaths = new List<Path>();

		private readonly List<Path> closedPaths = new List<Path>();

		private LineSegment[] qLineSeqConfigOut = new LineSegment[2];

		private PolygonCollider2D selfCollider = null;

		// ----------------------------------------------------------------------------------------

		public PolygonCollider2D SelfCollider
		{
			get
			{
				if (selfCollider == null) selfCollider = GetComponent<PolygonCollider2D>();
				return selfCollider;
			}
		}

		// ----------------------------------------------------------------------------------------

		private static int GetConfiguration(float tl, float tr, float bl, float br)
		{

			return (
				(bl >= threshold ? 1 : 0) << 0 |
				(br >= threshold ? 1 : 0) << 1 |
				(tr >= threshold ? 1 : 0) << 2 |
				(tl >= threshold ? 1 : 0) << 3
			);
		}

		/// <summary>
		/// returns a value from 0 to 1 of how interpolated the value from tile 1 to tile 2, 
		/// the midpoint should be. 0 represents no interpolation, so the midpoint value would
		/// just be the vertex position of tile 1, and 1 represents full interpolation so the 
		/// midpoint value would just be the vertex position of tile 2
		/// </summary>
		/// <param name="val1"> tile1 the value of the 1st tile vertex to interpolate from </param>
		/// <param name="val2"> tile2 the value of the 2nd tile vertex to interpolat to </param>
		/// <returns></returns>
		private static float FindInterpolationDelta(float val1, float val2)
		{
			// if the vertex value at tile 1 is equal to the threshold, no interpolation
			if (Mathf.Abs(threshold - val1) <= epsilon)
				return 0;

			// if the vertex value at tile 2 is equal to the threshold, full interpolation
			if (Mathf.Abs(threshold - val2) <= epsilon)
				return 1;

			// if the values are the same, it doesn't matter, so just return 0
			if (Mathf.Abs(val1 - val2) <= epsilon)
				return 0;

			// the delta interpolation value is equal to the difference between the threshold from first 
			// value over the difference of value 2 from value 1
			return (threshold - val1) / (val2 - val1);
		}

		private static int GetTileSegments(
			Vector2 position, Vector2 size,
			float valueTL, float valueTR, float valueBL, float valueBR,
			ref LineSegment[] outSegments
		)
		{
			// the value that gets interpolated, represents the midpoint weight between two
			// corners, 0.5 will be directly in between those two corners, where 0.75 will be
			// 3/4ths of the way, etc
			float val = 0.5f;

			// apply the interpolation value for the top midpoint if interpolation is enabled
			if (interpolate)
			{
				val = FindInterpolationDelta(valueTL, valueTR);
			}

			// find the approriate midpoint position between top left and top right
			Vector2 tmid = position;
			tmid.x += val * size.x;

			// apply the interpolation value for the left midpoint if interpolation is enabled
			if (interpolate)
			{
				val = FindInterpolationDelta(valueTL, valueBL);
			}

			// find the approriate position between top left and bottom left
			Vector2 lmid = position;
			lmid.y += val * size.y;

			// apply the interpolation value for the bottom midpoint if interpolation is enabled
			if (interpolate)
			{
				val = FindInterpolationDelta(valueBL, valueBR);
			}

			// find the approriate midpoint position between bottom left and bottom right
			Vector2 bmid = position + new Vector2(0, size.y);
			bmid.x += val * size.x;

			// apply the interpolation value for the right midpoint if interpolation is enabled
			if (interpolate)
			{
				val = FindInterpolationDelta(valueTR, valueBR);
			}

			// find the approriate midpoint position between top right and bottom right
			Vector2 rmid = position + new Vector2(size.x, 0);
			rmid.y += val * size.y;

			// determine which of the 16 square configurations the current square is
			int configuration = GetConfiguration(valueTL, valueTR, valueBL, valueBR);

			// add the line segments based on configuration
			int segmentCount = configActions[configuration].Invoke(
				tmid, lmid, bmid, rmid, ref outSegments
			);

			// return the amount of relevant line segments that were put into the output array
			return segmentCount;
		}

		// ----------------------------------------------------------------------------------------

		private void UpdateLineSegments(
			MarchingSquares.ScalarField neighborR, MarchingSquares.ScalarField neighborB,
			MarchingSquares.ScalarField neighborBR
		)
		{

			// clear the line segment list
			allLineSegments.Clear();

			// iterate through each scalar value in the scalar, each set of four scalar values in
			// a square shape makes up a tile
			for (int x = 0; x < scalarField.Width; x++)
			{
				bool isRightEdge = x + 1 >= scalarField.Width;
				for (int y = 0; y < scalarField.Height; y++)
				{
					bool isBottomEdge = y + 1 >= scalarField.Height;

					// get the 4 scalar values for each tile corner
					float val = scalarField[x, y];
					float valB;
					float valBR;

					if (isBottomEdge)
					{
						valB = neighborB[x, 0];
						valBR = isRightEdge ? neighborBR[0, 0] : neighborB[x + 1, 0];
					}
					else
					{
						valB = scalarField[x, y + 1];
						valBR = isRightEdge ? neighborR[0, y + 1] : scalarField[x + 1, y + 1];
					}

					float valR = isRightEdge ? neighborR[0, y] : scalarField[x + 1, y];

					// get the line segments for each tile
					int segCount = GetTileSegments(
						new Vector2(x, y), Vector2.one,
						val, valR, valB, valBR, ref qLineSeqConfigOut
					);

					// add the tile line segments to the line segment list
					for (int i = segCount - 1; i >= 0; i--) allLineSegments.Add(qLineSeqConfigOut[i]);
				}
			}

			int width = scalarField.Width;
			int height = scalarField.Height;

			// iterate around the outside to enclose the outer edges with fake full tile values
			for (int x = 0; x <= scalarField.Width; x++)
			{
				bool isRightEdge = x + 1 >= scalarField.Width;
				int segCount;

				if (x < scalarField.Width)
				{

					// top edge
					float valB = scalarField[x, 0];
					float valBR = isRightEdge ? neighborR[0, 0] : scalarField[x + 1, 0];

					// get the line segments for the fake tile
					segCount = GetTileSegments(
						new Vector2(x, -1), Vector2.one,
						0, 0, valB, valBR, ref qLineSeqConfigOut
					);

					// add the tile line segments to the line segment list
					for (int i = segCount - 1; i >= 0; i--)
					{
						qLineSeqConfigOut[i].start.y = 0;
						qLineSeqConfigOut[i].end.y = 0;
						allLineSegments.Add(qLineSeqConfigOut[i]);
					}
				}

				// bottom edge
				float val = isRightEdge ? neighborBR[0, 0] : neighborB[x, 0];
				float valR = x >= width ? 0 : (isRightEdge ? neighborBR[0, 0] : neighborB[x + 1, 0]);

				// get the line segments for the fake tile
				segCount = GetTileSegments(
					new Vector2(x, height), Vector2.one,
					val, valR, 0, 0, ref qLineSeqConfigOut
				);

				// add the tile line segments to the line segment list
				for (int i = segCount - 1; i >= 0; i--)
				{
					qLineSeqConfigOut[i].start.y = height;
					qLineSeqConfigOut[i].end.y = height;
					if (qLineSeqConfigOut[i].start.x > width) qLineSeqConfigOut[i].start.x = width;
					if (qLineSeqConfigOut[i].end.x > width) qLineSeqConfigOut[i].end.x = width;
					allLineSegments.Add(qLineSeqConfigOut[i]);
				}
			}

			for (int y = 0; y < scalarField.Height; y++)
			{
				bool isBottomEdge = y + 1 >= scalarField.Height;
				int segCount;

				if (y < scalarField.Height)
				{
					// left edge
					float valR = scalarField[0, y];
					float valBR = isBottomEdge ? neighborB[0, 0] : scalarField[0, y + 1];

					// get the line segments for the fake tile
					segCount = GetTileSegments(
						new Vector2(-1, y), Vector2.one,
						0, valR, 0, valBR, ref qLineSeqConfigOut
					);

					// add the tile line segments to the line segment list
					for (int i = segCount - 1; i >= 0; i--)
					{
						qLineSeqConfigOut[i].start.x = 0;
						qLineSeqConfigOut[i].end.x = 0;
						allLineSegments.Add(qLineSeqConfigOut[i]);
					}
				}

				// right edge
				float val = neighborR[0, y];
				float valB = isBottomEdge ? neighborBR[0, 0] : neighborR[0, y + 1];

				// get the line segments for the fake tile
				segCount = GetTileSegments(
					new Vector2(width, y), Vector2.one,
					val, 0, valB, 0, ref qLineSeqConfigOut
				);

				// add the tile line segments to the line segment list 
				for (int i = segCount - 1; i >= 0; i--)
				{
					qLineSeqConfigOut[i].start.x = width;
					qLineSeqConfigOut[i].end.x = width;
					allLineSegments.Add(qLineSeqConfigOut[i]);
				}
			}
		}

		private void BuildPaths()
		{
			for (int i = allPaths.Count - 1; i >= 0; i--) allPaths[i].Recycle();
			allPaths.Clear();

			// repeat as long as there are still unpathed line segments left
			while (allLineSegments.Count > 0)
			{
				Path curPath = Path.Get();
				curPath.AddFirst(allLineSegments[allLineSegments.Count - 1].start);
				curPath.AddLast(allLineSegments[allLineSegments.Count - 1].end);
				allLineSegments.RemoveAt(allLineSegments.Count - 1);

				Vector2 first = curPath.First.Value;
				Vector2 last = curPath.Last.Value;

				// compare path ends to each other line segment
				for (int i = allLineSegments.Count - 1; i >= 0; i--)
				{
					// check to see if they are connected, if they are extend the path in the line
					// segment direction, remove that line segment from the list and reiterate
					// through the list from the top again

					// if they are connected by the end
					if (last.x == allLineSegments[i].start.x && last.y == allLineSegments[i].start.y)
					{
						last.x = allLineSegments[i].end.x;
						last.y = allLineSegments[i].end.y;
						curPath.AddLast(last);
						allLineSegments.RemoveAt(i);
						i = allLineSegments.Count;
						continue;
					}
					if (last.x == allLineSegments[i].end.x && last.y == allLineSegments[i].end.y)
					{
						last.x = allLineSegments[i].start.x;
						last.y = allLineSegments[i].start.y;
						curPath.AddLast(last);
						allLineSegments.RemoveAt(i);
						i = allLineSegments.Count;
						continue;
					}

					// if they are connected by the start
					if (first.x == allLineSegments[i].end.x && first.y == allLineSegments[i].end.y)
					{
						first.x = allLineSegments[i].start.x;
						first.y = allLineSegments[i].start.y;
						curPath.AddFirst(first);
						allLineSegments.RemoveAt(i);
						i = allLineSegments.Count;
						continue;
					}
					if (first.x == allLineSegments[i].start.x && first.y == allLineSegments[i].start.y)
					{
						first.x = allLineSegments[i].end.x;
						first.y = allLineSegments[i].end.y;
						curPath.AddFirst(first);
						allLineSegments.RemoveAt(i);
						i = allLineSegments.Count;
						continue;
					}
				}

				allPaths.Add(curPath);
			}
		}

		private float GetVertexBoxPerimeterDelta(
			in Vector2 vertex, in float boxLeft, in float boxTop,
			in float boxRight, in float boxBottom
		)
		{
			float width = boxRight - boxLeft;
			float height = boxBottom - boxTop;
			float delta = 0;

			if (vertex.y == boxTop)
			{
				delta += 0;
				delta += vertex.x;
			}
			else if (vertex.x == boxRight)
			{
				delta += width;
				delta += vertex.y;
			}
			else if (vertex.y == boxBottom)
			{
				delta += width + height;
				delta += width - vertex.x;
			}
			else if (vertex.x == boxLeft)
			{
				delta += width * 2 + height;
				delta += height - vertex.y;
			}

			// vertex does not lie on box edge
			else return -1;

			return delta;
		}

		private void UpdatePaths()
		{
			BuildPaths();
		}

		private void UpdatePolygonCollider()
		{
			// TODO optomize

			SelfCollider.pathCount = allPaths.Count;
			for (int i = 0; i < allPaths.Count; i++)
			{
				Vector2[] pathList = new Vector2[allPaths[i].Count];
				int k = 0;
				foreach (Vector2 vec in allPaths[i])
					pathList[k++] = new Vector2(vec.x / scalarField.Width * chunkSize.x, vec.y / scalarField.Height * chunkSize.y);
				SelfCollider.SetPath(i, pathList);
			}
		}

		// ----------------------------------------------------------------------------------------

		public void UpdateCollisionMesh(
			MarchingSquares.ScalarField neighborRight, MarchingSquares.ScalarField neighborBelow,
			MarchingSquares.ScalarField neighborBelowRight
		)
		{
			if (neighborBelow == null || neighborRight == null || neighborBelowRight == null)
			{
				throw new System.ArgumentNullException(
					"Neighbor Scalar data must be provided in order to generate mesh data"
				);
			}

			UpdateLineSegments(neighborRight, neighborBelow, neighborBelowRight);
			UpdatePaths();
			UpdatePolygonCollider();
		}

		// ----------------------------------------------------------------------------------------

		public struct LineSegment
		{
			public LineSegment(Vector2 start, Vector2 end)
			{
				this.start = start;
				this.end = end;
			}

			public Vector2 start;

			public Vector2 end;
		}

		/// <summary>
		/// TODO base this off a custom linked list type that pools nodes so we generate a bunch
		/// of garbage from not reusing nodes
		/// </summary>
		private class Path : LinkedList<Vector2>
		{
			public static readonly Queue<Path> pool = new Queue<Path>();

			public static Path Get()
			{
				Path r;
				if (pool.Count > 0) r = pool.Dequeue();
				else r = new Path();

				r.Clear();
				r.openSides = 0;

				return r;
			}

			private Path() { }

			public float minRectDelta = -1;

			public float maxRectDelta = -1;

			public Side openSides = 0;

			public Path Clone()
			{
				Path r = Get();

				LinkedListNode<Vector2> crawl = First;
				while (crawl != null)
				{
					r.AddLast(crawl.Value);
					crawl = crawl.Next;
				}

				r.minRectDelta = minRectDelta;
				r.maxRectDelta = maxRectDelta;

				return r;
			}

			public void Recycle()
			{
				pool.Enqueue(this);
			}

			[System.Flags]
			public enum Side
			{
				top = 0b00001,
				left = 0b00010,
				bottom = 0b00100,
				right = 0b01000,
				enclosed = 0b11111
			}
		}
	}
}