﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Terrain
{
	public class TerrainChunkSet
	{
		public static int ChunkWidth => MarchingSquares.ScalarField.width;

		public static int ChunkHeight => MarchingSquares.ScalarField.width;


		// ----------------------------------------------------------------------------------------

		public TerrainChunkSet(Transform chunkPool, Transform activeChunks)
		{
			chunkPool.gameObject.SetActive(false);
			chunkPoolTransform = chunkPool;
			chunkContainerTransform = activeChunks;
		}

		// ----------------------------------------------------------------------------------------

		private Transform chunkPoolTransform;

		private Transform chunkContainerTransform;

		private Dictionary<(int, int), TerrainChunk> chunkData =
			new Dictionary<(int, int), TerrainChunk>();

		private Dictionary<(int, int), MarchingSquares.ScalarField> scalarData =
			new Dictionary<(int, int), MarchingSquares.ScalarField>();

		private RectInt activeChunkRange = new RectInt();

		private RectInt activeScalarRange = new RectInt(
			int.MinValue / 2, int.MinValue / 2, int.MaxValue, int.MaxValue
		);

		// ----------------------------------------------------------------------------------------

		/// <summary>
		/// 
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		public TerrainChunk this[int x, int y]
		{
			get => GetTerrainChunk(x, y);
		}

		// Internal Utility: ----------------------------------------------------------------------

		private TerrainChunk GetTerrainChunk() 
		{
			// TODO use chunk pool
			TerrainChunk r = new GameObject("chunk").AddComponent<TerrainChunk>();
			r.gameObject.SetActive(true);
			r.transform.SetParent(chunkContainerTransform, false);
			return r;
		}

		private MarchingSquares.ScalarField GenerateScalarData(int x, int y)
		{
			var r = MarchingSquares.GetScalarField();

			float noiseScale = 0.141f;
			float noiseOffset = 0; //Random.Range(-999999f, 999999f);

			for(int dx = ChunkWidth - 1; dx >= 0; dx--)
			{
				for(int dy = ChunkHeight - 1; dy >= 0; dy--)
				{
					// r[dx, dy] = Random.value;
					r[dx, dy] = Mathf.PerlinNoise(
						(x + dx) * noiseScale + noiseOffset,
						(y + dy) * noiseScale + noiseOffset
					);
				}
			}

			scalarData.Add((x, y), r);
			return r;
		}

		public TerrainChunk CreateChunkAt(int x, int y)
		{
			MarchingSquares.ScalarField scalars = GetScalarField(x, y);
			if (scalars == null)
			{
				scalars = GenerateScalarData(x, y);
			}

			// TODO create chunk from scalar data
			TerrainChunk chunk = GetTerrainChunk();
			chunk.scalarField = scalars;
			chunk.UpdateCollisionMesh(scalars, scalars, scalars);

			return chunk;
		}

		// Primary Functionality: -----------------------------------------------------------------

		public float GetScalarValue(int x, int y)
		{
			float divX = 1 / (ChunkWidth);
			float divY = 1 / (ChunkHeight);
			int sx = Mathf.FloorToInt(x * divX);
			int sy = Mathf.FloorToInt(y * divY);

			MarchingSquares.ScalarField scalars = GetScalarField(sx, sy);

			if(scalars == null) throw new System.Exception("Scalar field is not loaded");

			return scalars[x - (sx * ChunkWidth), y - (sx * ChunkHeight)];
		}

		public MarchingSquares.ScalarField GetScalarField(int x, int y)
		{
			if (!scalarData.ContainsKey((x, y))) return null;
			return scalarData[(x, y)];
		}

		public TerrainChunk GetTerrainChunk(int x, int y)
		{
			if (!scalarData.ContainsKey((x, y))) return null;
			return chunkData[(x, y)];
		}

		public TerrainChunk ForceGetTerrainChunk(int x, int y)
		{
			TerrainChunk r = this[x, y];
			if (r == null) r = CreateChunkAt(x, y);

			return r;
		}

		/// <summary>
		/// Set the range of indices which correspond to which chunks should be active
		/// </summary>
		/// <returns> 
		/// 	a bool indicating whether or not the range changed at all from it's 
		/// 	previous value
		/// </returns>
		public bool SetActiveChunkRange(int minX, int minY, int maxX, int maxY)
		{
			bool r = ( 
				activeChunkRange.x != minX || activeChunkRange.y != minY ||
				activeChunkRange.xMax != maxX || activeChunkRange.yMax != maxY
			);

			activeChunkRange.x = minX;
			activeChunkRange.y = minY;
			activeChunkRange.width = maxX - minX;
			activeChunkRange.height = maxY - minY;

			return r;
		}

		/// <summary>
		/// ensure all the chunks within the active range are loaded and active
		/// </summary>
		public void CreateActiveChunks()
		{
			for (int x = activeChunkRange.width; x >= 0; x--)
			{
				int dx = x + activeChunkRange.x;
				for (int y = activeChunkRange.height; y >= 0; y--)
				{
					int dy = y + activeChunkRange.y;

					TerrainChunk chunk = this[dx, dy];
					if (chunk == null) CreateChunkAt(dx, dy);
				}
			}
		}
	}
}