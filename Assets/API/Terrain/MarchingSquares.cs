using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Terrain
{
	public class MarchingSquares : MonoBehaviour
	{
		private static readonly Queue<ScalarField> scalarFieldPool = new Queue<ScalarField>();

		private static TerrainChunk chunkBlueprint = null;

		protected static TerrainChunk ChunkBlueprint
		{
			get
			{
				if (chunkBlueprint == null) CreateChunkBlueprint();
				return chunkBlueprint;
			}
		}

		public static ScalarField GetScalarField()
		{
			ScalarField r;

			if (scalarFieldPool.Count > 0) r = scalarFieldPool.Dequeue();
			else r = new ScalarField();

			return r;
		}

		public static void RecycleScalarField(ScalarField field)
		{
			scalarFieldPool.Enqueue(field);
		}

		// Static Methods: ------------------------------------------------------------------------

		private static void CreateChunkBlueprint()
		{
			// only create chunk if it hasn't already been initialized
			if (chunkBlueprint != null)
			{
				throw new System.Exception("Blueprint already initialized");
			}

			GameObject go = new GameObject("Chunk");
			go.AddComponent<EdgeCollider2D>();
			go.SetActive(false);

			TerrainChunk chunk = go.AddComponent<TerrainChunk>();
			chunkBlueprint = chunk;

		}

		// ----------------------------------------------------------------------------------------

		private readonly Queue<TerrainChunk> chunkPool = new Queue<TerrainChunk>();

		private TerrainChunkSet chunks = null;

		[SerializeField]
		private Transform chunkContainer = null;

		[SerializeField]
		private Transform chunkPoolContainer = null;

		// Exposed Properties: --------------------------------------------------------------------

		public TerrainChunkSet Chunks 
		{
			get
			{
				if (chunks == null) chunks = new TerrainChunkSet(ChunkPoolContainer, ChunkContainer);
				return chunks;
			}
		}

		public Transform ChunkContainer
		{
			get
			{
				if (chunkContainer == null)
				{
					chunkContainer = new GameObject("Chunks").transform;
					chunkContainer.SetParent(transform, false);
				}
				return chunkContainer;
			}
		}

		public Transform ChunkPoolContainer
		{
			get
			{
				if(chunkPoolContainer == null)
				{
					chunkPoolContainer = new GameObject("Chunk Pool").transform;
					chunkPoolContainer.gameObject.SetActive(false);
					chunkPoolContainer.SetParent(transform, false);
				}
				return chunkPoolContainer;
			}
		}

		// Internal Utility: ----------------------------------------------------------------------

		private TerrainChunk GetChunk()
		{
			TerrainChunk r;

			if (chunkPool.Count > 0) r = chunkPool.Dequeue();
			else
			{
				r = Instantiate(ChunkBlueprint);
				r.gameObject.SetActive(true);
			}

			r.transform.SetParent(ChunkContainer, false);

			return r;
		}

		private void RecycleChunk(TerrainChunk chunk)
		{
			chunk.transform.SetParent(ChunkPoolContainer);
			chunkPool.Enqueue(chunk);
		}

		[ContextMenu("Test Chunk Generation with chunk 0")]
		private void TestChunk0()
		{
			Chunks.CreateChunkAt(0, 0);
		}

		// Primary Funcitonality: -----------------------------------------------------------------

		/// <summary>
		/// Force all the chunks that overlap the specifed world rect to be loaded and active
		/// </summary>
		public void SetActiveChunkRangeWorld(Rect worldRect)
		{
			Vector2 tmin = transform.InverseTransformPoint(worldRect.min);
			Vector2 tmax = transform.InverseTransformPoint(worldRect.max);

			Vector2 min = Vector2.Min(tmin, tmax);
			Vector2 max = Vector2.Max(tmin, tmax);

			bool changed = Chunks.SetActiveChunkRange(
				Mathf.FloorToInt(min.x), Mathf.FloorToInt(min.y),
				Mathf.FloorToInt(max.x), Mathf.FloorToInt(max.y)
			);

			if (changed) chunks.CreateActiveChunks();
		}

		// ----------------------------------------------------------------------------------------

		private void Start()
		{
			SetActiveChunkRangeWorld(Rect.MinMaxRect(-10, -10, 10, 10));
		}

		// ----------------------------------------------------------------------------------------

		public class ScalarField
		{
			public static readonly int width = 16;
			public static readonly int height = 16;

			public ScalarField()
			{
				fieldData = new float[width * height];
				Width = width;
				Height = height;
			}

			public float this[int x, int y] 
			{
				get => fieldData[y * Width + x];
				set => fieldData[y * Width + x] = value;
			}

			private float[] fieldData = null;

			[field: SerializeField]
			public int Width { get; private set; } = 0;

			[field: SerializeField]
			public int Height { get; private set; } = 0;

			public IReadOnlyList<float> FieldData => fieldData;
		}
	}
}
