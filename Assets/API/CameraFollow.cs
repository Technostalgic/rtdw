using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
    [RequireComponent(typeof(Camera))]
    public class CameraFollow : MonoBehaviour
    {

        private Camera cam = null;
        public Camera Camera
        {
            get
            {
                if (cam == null) cam = GetComponent<Camera>();
                return cam;
            }
        }

        [field: SerializeField]
        public Transform TrackingTarget { get; set; } = null;

        // --------------------------------------------------------------------------------------------

        void Start()
        {
            _ = Camera;
        }

        void Update()
        {
            TrackTarget();
        }

        // --------------------------------------------------------------------------------------------

        private void TrackTarget()
        {
            if (TrackingTarget == null) return;

            Vector2 tpos = TrackingTarget.position;
            transform.position = new Vector3(tpos.x, tpos.y, -10);
        }
    }
}