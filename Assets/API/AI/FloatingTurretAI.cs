﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTDW.Actors;

namespace RTDW.AI
{
	public interface ITargeting
	{
		public Transform Target { get; }
	}

	[SelectionBase]
	[RequireComponent(typeof(CommandPalette))]
	public class FloatingTurretAI : StateMachineAIBehavior, ITargeting
	{
		public static readonly List<RaycastHit2D> qRaycastHits = new List<RaycastHit2D>();

		public static readonly List<Collider2D> qOverlapHits = new List<Collider2D>();

		public static ContactFilter2D losFilter => GameManager.InitParams.actorLosFilter;

		public static ContactFilter2D targetFilter => GameManager.InitParams.actorTargetFilter;

		// ----------------------------------------------------------------------------------------

		private CommandPalette cmdPalette = null;

		private Command.IVectorCommand freeformMovement = null;

		private Command.IVectorCommand attackCmd = null;

		[Tooltip("Initial target target for attack - can and in most cases should be left blank")]
		public DamageReceiver target = null;

		public float searchRange = 10;

		public float fireCloseRange = 4;

		public float fireFarRange = 8;

		public float fireBurstLength = 0.5f;

		private WanderState wander = null;

		private PursuitState pursuit = null;

		private FireState fire = null;

		// ----------------------------------------------------------------------------------------

		public CommandPalette CmdPalette
		{
			get
			{
				if (cmdPalette == null) cmdPalette = GetComponent<CommandPalette>();
				return cmdPalette;
			}
		}

		public Command.IVectorCommand FreeformMovement
		{
			get
			{
				if (freeformMovement == null)
					freeformMovement = CmdPalette.GetCommand("FreeformMovement");
				return freeformMovement;
			}
		}

		public Command.IVectorCommand AttackCmd
		{
			get
			{
				if (attackCmd == null) attackCmd = CmdPalette.GetCommand("Attack");
				return attackCmd;
			}
		}

		public WanderState Wander
		{
			get
			{
				if (wander == null) wander = new WanderState(this);
				return wander;
			}
		}

		public PursuitState Pursuit
		{
			get
			{
				if (pursuit == null) pursuit = new PursuitState(this);
				return pursuit;
			}
		}

		public FireState Fire
		{
			get
			{
				if (fire == null) fire = new FireState(this);
				return fire;
			}
		}

		public Transform Target => target?.transform;

		// ----------------------------------------------------------------------------------------

		public bool CanSeeTarget(DamageReceiver dr)
		{
			qRaycastHits.Clear();
			Vector2 dif = transform.position - dr.transform.position;
			Physics2D.Raycast(
				dr.transform.position, dif, losFilter, qRaycastHits, dif.magnitude
			);

			foreach (var hit in qRaycastHits)
			{
				if (hit.collider.GetComponentInParent<FloatingTurretAI>() == this)
					continue;

				// TODO make this work if the target dr has multiple children with other
				// damageReciever components, since right now it could possibly get the
				// damage receiver component on a child and this check will return false
				// when it should return true
				if (hit.collider.GetComponentInParent<DamageReceiver>() == dr)
					continue;

				return false;
			}

			return true;
		}

		// ----------------------------------------------------------------------------------------

		protected override void Start()
		{
			base.Start();
			StateMachine.SetState(Wander);
		}

		private void OnDrawGizmosSelected()
		{
			if(StateMachine.CurrentState == Wander)
			{
				if (Wander.wanderPointAlpha.HasValue)
				{
					float size = 1;
					Gizmos.color = Color.red;
					Gizmos.DrawWireCube(Wander.wanderPointAlpha.Value, new Vector3(size, size, size));
					Gizmos.color = Color.blue;
					Gizmos.DrawWireCube(Wander.wanderPoint, new Vector3(size, size, size));
				}
			}
		}

		// ----------------------------------------------------------------------------------------

		public class FloatingTurretState : StateMachine.IdleState
		{
			public FloatingTurretState(FloatingTurretAI target)
			{
				aiTarget = target;
			}

			protected FloatingTurretAI aiTarget = null;

			public FloatingTurretAI AiTarget => aiTarget;

			public StateMachine StateMachine => aiTarget.StateMachine;
		}

		public class WanderState : FloatingTurretState
		{
			public WanderState(FloatingTurretAI target) : base(target) { }

			protected readonly float searchIntervalTime = 0.25f;

			private float searchTimer = 0;

			public Vector2? wanderPointAlpha = null;

			public bool wanderPointAlphaReached = false;

			public Vector2 wanderPoint = default;

			private float wanderSearchDist = 50;

			private float wanderSearchHeightPref = 3;

			private float wanderRayRadius = 0.5f;

			private float preWanderTimer = 0;

			// ------------------------------------------------------------------------------------

			private void HandleTargetSearching() 
			{
				if (searchTimer <= 0) SearchForTarget();
				else searchTimer -= Time.deltaTime;
			}

			private void HandleMoving()
			{
				if (preWanderTimer > 0)
				{
					PreWanderMove();
					preWanderTimer -= Time.deltaTime;
					return;
				}

				if (wanderPointAlpha.HasValue)
				{
					MoveToWanderPoint();
				}

				else SearchForWanderPoint();
			}

			private void PreWanderMove()
			{
				Vector2 dif = wanderPoint - (Vector2)aiTarget.transform.position;

				AiTarget.FreeformMovement.Send(dif);
			}

			private void MoveToWanderPoint()
			{
				// if already at wander point, complete waypoint
				float dist = Vector3.Distance(aiTarget.transform.position, wanderPoint);
				if(dist <= wanderRayRadius * 4)
				{
					wanderPointAlpha = null;
					return;
				}

				if (!wanderPointAlphaReached)
				{
					float alphaDist = Vector3.Distance(
						aiTarget.transform.position, wanderPointAlpha.Value
					);
					if (alphaDist <= wanderRayRadius * 4)
					{
						wanderPointAlphaReached = true;
					}
				}

				Vector2 wpDif = wanderPoint - (Vector2)AiTarget.transform.position;

				// test if the wanderpoint is within line of sight
				qRaycastHits.Clear();
				Physics2D.CircleCast(
					AiTarget.transform.position, wanderRayRadius, wpDif, losFilter, 
					qRaycastHits, Vector3.Distance(aiTarget.transform.position, wanderPoint)
				);
				bool losHit = qRaycastHits.Count > 0;

				
				// if line of sight is blocked, travel to wanderpoint alpha instead
				bool alphatravel = false;
				if(losHit && !wanderPointAlphaReached) alphatravel = true;
				Vector2 dest = alphatravel ? wanderPointAlpha.Value : wanderPoint;

				// move toward destination
				AiTarget.FreeformMovement.Send(
					(dest - (Vector2)AiTarget.transform.position).normalized
				);
			}

			private bool PreWanderPointSearchCheck()
			{
				int hits = Physics2D.OverlapCircle(
					AiTarget.transform.position, wanderRayRadius, losFilter, qOverlapHits
				);

				if (hits > 0)
				{
					wanderPointAlpha = null;
					preWanderTimer = 0.5f;
					wanderPoint =
						((Vector2)AiTarget.transform.position) +
						Random.insideUnitCircle.normalized * 5;
					return false;
				}

				return true;
			}

			private void SearchForWanderPoint()
			{
				if (!PreWanderPointSearchCheck()) return;

				// raycast in random direction
				float searchDir = Random.Range(0, 2 * Mathf.PI);
				qRaycastHits.Clear();
				Physics2D.CircleCast(
					AiTarget.transform.position, wanderRayRadius,
					new Vector2(Mathf.Cos(searchDir), Mathf.Sin(searchDir)), 
					losFilter, qRaycastHits, wanderSearchDist
				);

				// stop if no hits
				if (qRaycastHits.Count <= 0) return;

				// find closest hit
				RaycastHit2D closest = qRaycastHits[0];
				for(int i = qRaycastHits.Count - 1; i > 0; i--)
				{
					if (closest.distance > qRaycastHits[i].distance) closest = qRaycastHits[i];
				}
				wanderPointAlpha = closest.point;
				wanderPointAlphaReached = false;

				// raycast in opposite direction to see if it's a corridor or open area
				qRaycastHits.Clear();
				Physics2D.CircleCast(
					closest.point, wanderRayRadius, closest.normal, losFilter, qRaycastHits
				);

				// if open area, go to the wanderpoint above rayhit
				if(qRaycastHits.Count <= 0)
				{
					wanderPoint = closest.point + closest.normal * wanderSearchHeightPref;
					return;
				}

				// find the closest hit on this new raycast
				RaycastHit2D closest2 = qRaycastHits[0];
				for (int i = qRaycastHits.Count - 1; i > 0; i--)
				{
					if (closest.distance > qRaycastHits[i].distance) closest = qRaycastHits[i];
				}

				// if corridor, go to wanderpoint in between two ray hits
				wanderPoint = (closest.point + closest2.point) * 0.5f;
			}

			private void SearchForTarget()
			{
				searchTimer = searchIntervalTime;

				qOverlapHits.Clear();
				Physics2D.OverlapCircle(
					AiTarget.transform.position, AiTarget.searchRange, targetFilter, qOverlapHits
				);

				foreach(var hit in qOverlapHits)
				{
					DamageReceiver dr = hit.GetComponent<DamageReceiver>();
					if (IsTargetValid(dr))
					{
						if (AiTarget.CanSeeTarget(dr))
						{
							AiTarget.target = dr;
							break;
						}
					}
				}

				if(aiTarget.target != null)
				{
					StateMachine.SetState(aiTarget.Pursuit);
				}
			}

			private bool IsTargetValid(DamageReceiver dr)
			{
				if (dr == null) return false;

				// for now, only target players
				if (dr.GetComponentInParent<PlayerController>()) return true;
				return false;
			}

			// ------------------------------------------------------------------------------------

			public override void Update()
			{
				base.Update();
				HandleMoving();
				HandleTargetSearching();
			}

			public override void OnBegin()
			{
				base.OnBegin();
				searchTimer = 0;
			}
		}

		public class PursuitState : FloatingTurretState
		{
			public PursuitState(FloatingTurretAI target) : base(target) { }

			private readonly float pursuitCheckInterval = 1;

			private float pursuitCheckTimer = 0;

			protected float fireCheckIntervalTime = 0.5f;

			protected float fireCheckTimer = 0;

			// ------------------------------------------------------------------------------------

			private void HandlePursuitCheck()
			{
				if (pursuitCheckTimer <= 0) PursuitCheck();
				else pursuitCheckTimer -= Time.deltaTime;
			}

			private void PursuitCheck()
			{
				pursuitCheckTimer = pursuitCheckInterval;

				if (!AiTarget.CanSeeTarget(AiTarget.target))
				{
					AiTarget.target = null;
					StateMachine.SetState(AiTarget.Wander);
				}
			}

			private void DoPursuit()
			{
				if (AiTarget.target == null) return;
				Vector2 dif = AiTarget.target.transform.position - AiTarget.transform.position;
				dif.Normalize();
				AiTarget.FreeformMovement.Send(dif);
			}

			private void HandleFireCheck()
			{
				if (fireCheckTimer <= 0) FireCheck();
				else fireCheckTimer -= Time.deltaTime;
			}

			private void FireCheck()
			{
				fireCheckTimer = fireCheckIntervalTime;
				if (AiTarget.CanSeeTarget(AiTarget.target))
				{
					float distance = Vector3.Distance(
						AiTarget.target.transform.position, AiTarget.transform.position
					);

					if (distance <= AiTarget.fireCloseRange)
					{
						StateMachine.SetState(AiTarget.Fire);
					}
				}
			}

			// ------------------------------------------------------------------------------------

			public override void Update()
			{
				base.Update();
				DoPursuit();
				HandlePursuitCheck();
				HandleFireCheck();
			}

			public override void OnBegin()
			{
				base.OnBegin();
				pursuitCheckTimer = 0;
			}
		}

		public class FireState : FloatingTurretState
		{
			public FireState(FloatingTurretAI target) : base(target) { }

			private float fireCheckTimer = 0;

			private float attackInterval = 1;

			private float attackTimer = 0;

			private float fireBurstTimer = 0;

			// ----------------------------------------------------------------------------------------
			
			private void HandleFireCheck()
			{
				if (fireCheckTimer <= 0) FireCheck();
				else fireCheckTimer -= Time.deltaTime;
			}

			private void FireCheck()
			{
				fireCheckTimer -= Time.deltaTime;

				float distance = Vector3.Distance(
					AiTarget.target.transform.position, AiTarget.transform.position
				);

				// outside fire range
				if (distance > AiTarget.fireFarRange)
				{
					StateMachine.SetState(AiTarget.Pursuit);
				}

				// within fire range
				else
				{
					if (attackTimer <= 0) fireBurstTimer = AiTarget.fireBurstLength;
					else attackTimer -= Time.deltaTime;
				}
			}

			private void FireBurst()
			{
				attackTimer = attackInterval;
				fireBurstTimer -= Time.deltaTime;
				AiTarget.AttackCmd.Send();
			}

			// ----------------------------------------------------------------------------------------

			public override void Update()
			{
				base.Update();

				if (fireBurstTimer <= 0) HandleFireCheck();
				else FireBurst();
			}
		}
	}
}