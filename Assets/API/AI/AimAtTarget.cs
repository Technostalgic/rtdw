﻿using System.Collections;
using UnityEngine;

namespace RTDW.AI
{
	public class AimAtTarget : MonoBehaviour
	{
		[SerializeField, Tooltip("The gameobject with the ITargeting component where the target will be gotten from, self if null")]
		private GameObject targetingGameObject;

		private ITargeting targeting;

		[SerializeField]
		private float aimSpeed = 6;

		private Vector2 lastPos = Vector2.zero;

		private Vector2 moveDir = Vector2.zero;

		private float moveDirAngle = 0f;

		// ----------------------------------------------------------------------------------------

		public ITargeting Targeting
		{
			get
			{
				if(targeting == null)
				{
					GameObject go = targetingGameObject == null ? gameObject : targetingGameObject;
					targeting = go.GetComponent<ITargeting>();
				}
				return targeting;
			}
		}

		// ----------------------------------------------------------------------------------------
		
		private void HandleMoveDir()
		{
			Vector2 dif = (Vector2)transform.position - lastPos;
			moveDir = dif.normalized;
			moveDirAngle = Mathf.Atan2(moveDir.y, moveDir.x);
			lastPos = transform.position;
		}

		private void HandleAiming()
		{
			Transform target = Targeting.Target;
			float targetAngle;

			if (target != null)
			{
				Vector2 dif = target.position - transform.position;
				targetAngle = Mathf.Atan2(dif.y, dif.x);
			}

			else
			{
				if (moveDir == Vector2.zero) return;
				targetAngle = moveDirAngle;
			}

			float curAngle = Mathf.Atan2(transform.right.y, transform.right.x);
			float tAng = Mathf.MoveTowardsAngle(curAngle, targetAngle, aimSpeed * Time.deltaTime);
			transform.rotation = Quaternion.Euler(0, 0, tAng * Mathf.Rad2Deg);
		}

		// ----------------------------------------------------------------------------------------

		private void Start()
		{
			lastPos = transform.position;
		}

		private void Update()
		{
			HandleMoveDir();
			HandleAiming();
		}
	}
}