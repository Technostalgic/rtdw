using System.Collections;
using System.Collections.Generic;

namespace RTDW.AI
{
	public class SelectorNode : BehaviorNode
	{
		public SelectorNode(params BehaviorNode[] children) : base(children) { }

		// ----------------------------------------------------------------------------------------

		public override BehaviorState Evaluate()
		{
			for (int i = 0; i < children.Length; i++)
			{
				BehaviorState childState = Children[i].Evaluate();
				switch (childState)
				{
					case BehaviorState.Running:
						return BehaviorState.Running;
					case BehaviorState.Succeeded:
						return BehaviorState.Succeeded;
					case BehaviorState.Failed: continue;
				}
			}

			return BehaviorState.Failed;
		}
	}
}
