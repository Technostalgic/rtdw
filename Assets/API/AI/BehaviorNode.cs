using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTDW.Actors;

namespace RTDW.AI
{
	public enum BehaviorState
	{
		Running,
		Succeeded,
		Failed
	}

	// --------------------------------------------------------------------------------------------

	public abstract class BehaviorNode
	{
		public BehaviorNode(params BehaviorNode[] childs)
		{
			SetChildren(childs);
		}

		// ----------------------------------------------------------------------------------------

		public BehaviorTree Tree
		{
			get
			{
				// fetch tree recursively from parent if it is not set
				if (tree == null)
				{
					BehaviorNode node = Parent;
					while (node != null)
					{
						if (node.tree != null)
						{
							tree = node.tree;
							break;
						}
						node = node.Parent;
					}
				}
				return tree;
			}
			set
			{
				if (Tree != null)
					throw new System.Exception("Behavior node cannot be set to multiple trees");
				tree = value;
			}
		}
		private BehaviorTree tree = null;

		public BehaviorNode Parent { get; protected set; } = null;

		public IReadOnlyList<BehaviorNode> Children => children;
		protected BehaviorNode[] children = null;

		// ----------------------------------------------------------------------------------------

		public abstract BehaviorState Evaluate();

		public void SetChildren(BehaviorNode[] childs)
		{
			children = new BehaviorNode[childs.Length];
			for (int i = childs.Length - 1; i >= 0; i--)
			{
				children[i] = childs[i];
				children[i].Parent = this;
				children[i].Tree = Tree;
			}
		}

		public void SetTreeRecursive(BehaviorTree tree)
		{
			Tree = tree;
			for (int i = children.Length - 1; i >= 0; i--) children[i].SetTreeRecursive(tree);
		}

		// ----------------------------------------------------------------------------------------

		public abstract class RequireCommandPalette : BehaviorNode
		{
			private CommandPalette commands;
			public CommandPalette Commands => commands == null ?
				commands = (Tree as IBtComponent_CommandPalette)?.Commands : commands;
		}

		public abstract class RequireTarget : BehaviorNode
		{
			protected IBtData_Target targetHolder;
			public Transform Target
			{
				get => targetHolder == null ?
					(targetHolder = (Tree as IBtData_Target))?.Target : targetHolder.Target;
				set
				{
					if (targetHolder == null) targetHolder = (Tree as IBtData_Target);
					if (targetHolder != null) targetHolder.Target = value;
				}
			}
		}

		public abstract class RequireCmdPaletteAndTarget : BehaviorNode
		{
			private CommandPalette commands;
			public CommandPalette Commands => commands == null ?
				commands = (Tree as IBtComponent_CommandPalette)?.Commands : commands;

			protected IBtData_Target targetHolder;
			public Transform Target
			{
				get => targetHolder == null ?
					(targetHolder = (Tree as IBtData_Target))?.Target : targetHolder.Target;
				set
				{
					if (targetHolder == null) targetHolder = (Tree as IBtData_Target);
					if (targetHolder != null) targetHolder.Target = value;
				}
			}
		}
	}
}
