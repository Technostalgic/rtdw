﻿using System.Collections;
using UnityEngine;

namespace RTDW.AI
{
	public abstract class StateMachineAIBehavior : MonoBehaviour
	{
		private readonly StateMachine stateMachine = new StateMachine();

		// ----------------------------------------------------------------------------------------

		public StateMachine StateMachine => stateMachine;

		// ----------------------------------------------------------------------------------------

		protected virtual void Update()
		{
			stateMachine.Update();
		}

		protected virtual void Start()
		{
			StateMachine.SetState(new StateMachine.IdleState());
		}

		protected virtual void OnEnable()
		{
			StateMachine.CurrentState?.OnBegin();
		}

		protected virtual void OnDisable()
		{
			StateMachine.CurrentState?.OnFinish();
		}
	}
}