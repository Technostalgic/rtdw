using System.Collections;
using System.Collections.Generic;

namespace RTDW.AI
{
	public class SequenceNode : BehaviorNode
	{
		public SequenceNode(params BehaviorNode[] children) : base(children) { }

		// ----------------------------------------------------------------------------------------

		public override BehaviorState Evaluate()
		{
			bool childsRunning = false;

			for (int i = 0; i < children.Length; i++)
			{
				BehaviorState childState = Children[i].Evaluate();
				switch (childState)
				{
					case BehaviorState.Running: childsRunning = true; continue;
					case BehaviorState.Succeeded: continue;
					case BehaviorState.Failed: return childState;
				}
			}

			return childsRunning ?
				BehaviorState.Running :
				BehaviorState.Succeeded; ;
		}
	}
}
