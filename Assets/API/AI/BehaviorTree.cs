using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.AI
{
	[DisallowMultipleComponent]
	public class BehaviorTree : MonoBehaviour
	{

		public BehaviorNode RootNode
		{
			get => rootNode;
			set
			{
				if (rootNode == value) return;
				if (value.Parent != null)
					throw new System.Exception("Node is not a root node");
				if (value.Tree != null)
					throw new System.Exception("Node is already part of a different tree");
				rootNode = value;
				rootNode.Tree = this;
			}
		}
		private BehaviorNode rootNode = null;

		// ----------------------------------------------------------------------------------------

		public virtual void SetupBehaviorTree() { }

		// ----------------------------------------------------------------------------------------

		private void Start()
		{
			SetupBehaviorTree();
		}

		private void Update()
		{
			rootNode?.Evaluate();
		}
	}

	// --------------------------------------------------------------------------------------------

	public interface IBtComponent_CommandPalette { public Actors.CommandPalette Commands { get; } }

	public interface IBtData_Target { public Transform Target { get; set; } }

	public interface IBtData_RuntimeFrequency { public float RuntimeFrequency { get; set; } }
}
