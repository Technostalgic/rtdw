﻿using System.Collections;
using UnityEngine;

namespace RTDW.AI
{
	public class StateMachine
	{
		private State currentState = null;

		// ------------------------------------------------------------------------------------

		public State CurrentState => currentState;

		// ------------------------------------------------------------------------------------

		public void Update()
		{
			currentState?.Update();
		}

		public void SetState(State newState)
		{
			Debug.Log("Setting State to " + newState.GetType().Name);
			currentState?.OnFinish();
			currentState = newState;
			newState?.OnBegin();
		}

		// ------------------------------------------------------------------------------------

		public abstract class State
		{
			public abstract void Update();
			public abstract void OnBegin();
			public abstract void OnFinish();
		}

		public class IdleState : State
		{
			public override void Update() { }
			public override void OnBegin() { }
			public override void OnFinish() { }
		}
	}
	
}