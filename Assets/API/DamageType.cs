using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	[CreateAssetMenu(fileName = "DamageType", menuName = "RTDW/Damage Type")]
	public class DamageType : ScriptableObject
	{
		[field: SerializeField]
		public string Label { get; set; } = "Default";
	}
}
