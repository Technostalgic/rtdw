using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	[DisallowMultipleComponent]
	public sealed class RagdollSpawner : Corpse
	{
		[field: SerializeField]
		public Ragdoll RagdollToSpawn { get; set; } = null;

		[field: SerializeField]
		public Transform BodyRoot { get; set; } = null;

		[SerializeField]
		private LimbLink[] linkedLimbs = new LimbLink[] { };

		private Dictionary<RagdollLimb, LimbLink> linkedLimbMap = null;
		public IReadOnlyDictionary<RagdollLimb, LimbLink> LinkedLimbMap
		{
			get
			{
				if (linkedLimbMap == null) BuildLinkedLimbMap();
				return linkedLimbMap;
			}
		}

		// ----------------------------------------------------------------------------------------

		public override void CreateCorpseFromEntity(GameObject go)
		{
			MatchLimbsToTargetBoneTransforms();
			RagdollToSpawn.transform.SetParent(null, true);

			RagdollToSpawn.gameObject.SetActive(true);
			BodyRoot.gameObject.SetActive(false);

			MatchRagdollBoneVelocity();
		}

		// ----------------------------------------------------------------------------------------

		[ContextMenu("Build Ragdoll from Body")]
		public void BuildRagdollFromBody()
		{
			RagdollToSpawn = Ragdoll.BuildRagdollFromTargetRoot(BodyRoot);
			RagdollToSpawn.transform.SetParent(transform, false);
			RagdollToSpawn.gameObject.SetActive(false);

			AutoLinkRagdollLimbs();
		}

		public void AutoLinkRagdollLimbs()
		{
			List<LimbLink> r = new List<LimbLink>();

			RagdollLimb limb = RagdollToSpawn;
			AnimationBone bone = BodyRoot.GetComponent<AnimationBone>();
			r.Add(new LimbLink(RagdollToSpawn, bone));
			AutoLinkRagdollLimbs_Recursive(limb, bone, ref r);

			linkedLimbs = r.ToArray();
		}

		[ContextMenu("Match Ragdoll to Actor Pose")]
		public void MatchLimbsToTargetBoneTransforms()
		{
			for (int i = linkedLimbs.Length - 1; i >= 0; i--)
			{
				LimbLink link = linkedLimbs[i];
				link.limb.MatchTargetBoneTransform(link.linkedBone.transform);
			}
		}

		// ----------------------------------------------------------------------------------------

		private void AutoLinkRagdollLimbs_Recursive(
			RagdollLimb startLimb, AnimationBone startBone, ref List<LimbLink> outList
		)
		{

			for (int i = 0; i < startLimb.transform.childCount; i++)
			{
				RagdollLimb limbChild = startLimb.transform.GetChild(i).GetComponent<RagdollLimb>();
				if (limbChild == null) continue;

				int i2 = i;
				bool linkAdded = false;
				AnimationBone boneChildBone = null;
				while (i2 < startBone.transform.childCount)
				{
					Transform boneChild = startBone.transform.GetChild(i2++);
					if (!RagdollLimb.EligibleForRagdollLimb(boneChild)) continue;

					boneChildBone = boneChild.GetComponent<AnimationBone>();
					if (boneChildBone == null) continue;

					outList.Add(new LimbLink(limbChild, boneChildBone));
					linkAdded = true;
					break;
				}

				if (linkAdded) AutoLinkRagdollLimbs_Recursive(limbChild, boneChildBone, ref outList);
			}
		}

		private void BuildLinkedLimbMap()
		{
			linkedLimbMap = new Dictionary<RagdollLimb, LimbLink>();
			for (int i = linkedLimbs.Length - 1; i >= 0; i--)
			{
				linkedLimbMap.Add(linkedLimbs[i].limb, linkedLimbs[i]);
			}
		}

		// ----------------------------------------------------------------------------------------

		private void MatchRagdollBoneVelocity()
		{
			Rigidbody2D selfRb = GetComponent<Rigidbody2D>();
			if (selfRb == null) return;

			//Debug.Break();

			for (int i1 = RagdollToSpawn.Limbs.Count - 1; i1 >= 0; i1--)
			{
				RagdollLimb limb = RagdollToSpawn.Limbs[i1];
				// limb.Rigidbody.velocity = Vector2.zero;
				limb.Rigidbody.velocity = selfRb.velocity;

				if (LinkedLimbMap.ContainsKey(limb))
				{
					AnimationBone bone = LinkedLimbMap[limb].linkedBone;
					limb.Rigidbody.velocity += bone.ExtrapolatedVelocity;
					limb.Rigidbody.angularVelocity += bone.ExtrapolatedAngularVelocity;

					float timeThresh = Time.time - 0.1f;
					for (int i2 = (bone.ProjectileHits?.Count ?? 0) - 1; i2 >= 0; i2--)
					{
						AnimationBone.HitInfo phit = bone.ProjectileHits[i2];
						if (phit.timeStamp < timeThresh) break;

						limb.Rigidbody.AddForceAtPositionCapped(
							phit.impulse, phit.rayHit.point, ForceMode2D.Impulse
						);
					}
				}
			}
		}

		// ----------------------------------------------------------------------------------------

		[System.Serializable]
		public class LimbLink
		{
			public LimbLink(RagdollLimb limb, AnimationBone bone)
			{
				this.limb = limb;
				this.linkedBone = bone;
			}

			public RagdollLimb limb = null;

			public AnimationBone linkedBone = null;
		}
	}
}
