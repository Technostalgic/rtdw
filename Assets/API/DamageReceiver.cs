using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using RTDW.Weapons;

namespace RTDW
{
	[DisallowMultipleComponent]
	public class DamageReceiver : MonoBehaviour, IProjectileListener
	{
		// Temporary Query Variables: -------------------------------------------------------------

		private static readonly DamageData qDamageData = new DamageData();

		// ----------------------------------------------------------------------------------------

		[SerializeField, HideInInspector]
		private DamageData damageReceived = new DamageData();
		public IReadonlyDamageData DamageReceived => damageReceived;

		public float TotalDamage => damageReceived.Total;

		public DamageReceivedEvent OnDamaged { get; private set; } = new DamageReceivedEvent();

		[field: SerializeField]
		private bool autoAddDamageListeners = true;
		public bool AutoAddDamageListeners
		{
			get => autoAddDamageListeners;
			set
			{
				if (value && !autoAddDamageListeners) AttachDamageListeners();
				autoAddDamageListeners = value;
			}
		}

		private HashSet<UnityAction<IReadonlyDamageData>> listenersFound =
			new HashSet<UnityAction<IReadonlyDamageData>>();

		[SerializeField]
		private List<DamageMultiplierValue> damageMultiplierList = null;

		private Dictionary<DamageType, float> damageMultipliers = null;
		public IReadOnlyDictionary<DamageType, float> DamageMultipliers
		{
			get
			{
				if (damageMultipliers == null) CreateDamageMultiplierDict();
				return damageMultipliers;
			}
		}

		// ----------------------------------------------------------------------------------------

		private void AttachDamageListeners()
		{
			var listeners = GetComponents<IDamageListener>();
			foreach (var listen in listeners)
			{
				if (listenersFound.Contains(listen.OnDamageReceived)) continue;
				OnDamaged.AddListener(listen.OnDamageReceived);
				listenersFound.Add(listen.OnDamageReceived);
			}
		}

		[ContextMenu("Update Damage Multiplier Dictionary")]
		private void CreateDamageMultiplierDict()
		{
			if (damageMultipliers != null) damageMultipliers.Clear();
			else damageMultipliers = new Dictionary<DamageType, float>();

			for (int i = damageMultiplierList.Count - 1; i >= 0; i--)
			{
				DamageMultiplierValue val = damageMultiplierList[i];
				if (damageMultipliers.ContainsKey(val.Type)) damageMultiplierList.RemoveAt(i);
				else damageMultipliers.Add(val.Type, val.Multiplier);
			}
		}

		// ----------------------------------------------------------------------------------------

		private void AddMultiplier(DamageType type, float multiplier)
		{
			if (DamageMultipliers.ContainsKey(type))
			{
				SetMultiplier(type, multiplier);
				return;
			}

			damageMultipliers.Add(type, multiplier);
			damageMultiplierList.Add(new DamageMultiplierValue(type, multiplier));
		}

		/// <summary>
		/// sets the damage multiplier for the specified damage type, so that the damage received
		/// can be greater than or less than the normal amount
		/// </summary>
		/// <param name="type"> the damage type that the multiplier is applied to </param>
		/// <param name="newMultiplier">the value to multiply the damage by</param>
		public void SetMultiplier(DamageType type, float newMultiplier)
		{
			if (!damageMultipliers.ContainsKey(type))
			{
				AddMultiplier(type, newMultiplier);
				return;
			}

			damageMultipliers[type] = newMultiplier;
			for (int i = damageMultiplierList.Count - 1; i >= 0; i--)
			{
				DamageMultiplierValue val = damageMultiplierList[i];
				if (val.Type == type)
				{
					if (val.Multiplier != 1) val.Multiplier = newMultiplier;
					else damageMultiplierList.RemoveAt(i);
					break;
				}
			}
		}

		/// <summary>
		/// gets the damage multiplier for any given damage type. if no damage multiplier is 
		/// specified, the default multiplier 1 is given
		/// </summary>
		/// <param name="type">the damage type to get the multiplier for</param>
		public float GetMultiplier(DamageType type)
		{
			if (!DamageMultipliers.ContainsKey(type)) return 1;
			return DamageMultipliers[type];
		}

		/// <summary>
		/// applies the damage multipliers that this damage receiver has to the specified damage 
		/// data so that it's new value represents the damage with the multipliers taken into 
		/// account
		/// </summary>
		/// <param name="data">the data to apply the multipliers to</param>
		public void ApplyMultipliersToData(DamageData data)
		{
			foreach (var kv in DamageMultipliers)
			{
				if (!data.ContainsKey(kv.Key)) continue;
				data[kv.Key] *= DamageMultipliers[kv.Key];
			}
		}

		// ----------------------------------------------------------------------------------------

		public void OnProjectileHit(RaycastHit2D hit, Projectile proj)
		{
			Damage(proj.ParentGun.Stats[GameManager.InitParams.GunStats.Damage]);
		}

		public void Damage(float damage, DamageType type = null)
		{
			if (type == null) type = GameManager.DefaultDamageType;
			Damage(new DamageData((type, damage)));
		}

		public void Damage(IReadonlyDamageData damage)
		{
			qDamageData.Clear_NoDealloc();
			qDamageData.CopyFrom(damage);
			ApplyMultipliersToData(qDamageData);
			damageReceived.Combine(qDamageData);
			OnDamaged?.Invoke(qDamageData);
		}

		public void ResetDamage()
		{
			damageReceived.Clear();
		}

		[ContextMenu("Print Damage")]
		public void PrintDamageInfo()
		{
			damageReceived.PrintData();
		}

		// ----------------------------------------------------------------------------------------

		private void Start()
		{
			if (AutoAddDamageListeners) AttachDamageListeners();
			CreateDamageMultiplierDict();
		}

		// ----------------------------------------------------------------------------------------

		[System.Serializable]
		private class DamageMultiplierValue
		{
			public DamageMultiplierValue() { }

			public DamageMultiplierValue(DamageType type, float multiplier)
			{
				Type = type;
				Multiplier = multiplier;
			}

			public DamageType Type = null;
			public float Multiplier = 1;
		}
	}

	[System.Serializable]
	public class DamageReceivedEvent : UnityEvent<IReadonlyDamageData> { }

	public interface IDamageListener
	{
		void OnDamageReceived(IReadonlyDamageData damage);
	}
}
