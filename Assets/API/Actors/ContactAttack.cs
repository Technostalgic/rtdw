using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Actors
{
	public class ContactAttack : MonoBehaviour
	{
		[field: SerializeField, Tooltip("The damage that the attack will do")]
		public DamageData AttackDamage { get; private set; } = new DamageData();

		[field: SerializeField, Tooltip("How long the attack move lasts")]
		public float AttackSustain { get; private set; } = 0.5f;

		// [field:SerializeField, Tooltip("How long before the attack can happen again")]
		// public float AttackCooldown { get; private set; } = 0.75f;

		/// <summary>
		/// a set of entities that were attacked during the most recent attack sequence
		/// </summary>
		public IReadOnlyCollection<DamageReceiver> AttackedEntities => attackedEntities;
		private readonly HashSet<DamageReceiver> attackedEntities = new HashSet<DamageReceiver>();

		private Coroutine atkRoutine = null;

		public bool IsAttacking => atkRoutine != null;

		// ----------------------------------------------------------------------------------------

		private void OnAttackCommand(Command.ICommandState cmd)
		{
			IEnumerator cmdSequence()
			{
				yield return new WaitWhile(() => IsAttacking);

				cmd.IsRunning = false;
				yield break;
			}

			DoAttack();

			cmd.IsRunning = true;
			StartCoroutine(cmdSequence());
		}

		public void Cmd_Attack(Command.IVoidCommand cmd) => OnAttackCommand(cmd);

		public void Cmd_Attack(Command.IVectorCommand cmd) => OnAttackCommand(cmd);

		// ----------------------------------------------------------------------------------------

		public void DoAttack()
		{
			if (IsAttacking) return;

			IEnumerator attackSequence()
			{
				yield return new WaitForSeconds(AttackSustain);

				atkRoutine = null;
				yield break;
			}

			attackedEntities.Clear();
			atkRoutine = StartCoroutine(attackSequence());
		}

		// ----------------------------------------------------------------------------------------

		private void OnAttackEntity(DamageReceiver entity)
		{
			entity.Damage(AttackDamage);
			Debug.Log(name + " attacked entity '" + entity.name + "' for " + AttackDamage.Total + " damage");
			attackedEntities.Add(entity);
		}

		// ----------------------------------------------------------------------------------------

		private void OnCollisionStay2D(Collision2D collision)
		{
			DamageReceiver dr = collision.collider.GetAttachedDamageReceiver();
			if (dr == null || attackedEntities.Contains(dr)) return;

			OnAttackEntity(dr);
		}
	}
}
