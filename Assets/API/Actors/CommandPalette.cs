using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Actors
{
	public class CommandPalette : MonoBehaviour, ISerializationCallbackReceiver
	{
		[SerializeField]
		private Command[] serializedCommands;

		private Dictionary<int, Command> commands = null;
		public IReadOnlyDictionary<int, Command> Commands => commands;

		// ----------------------------------------------------------------------------------------

		public void OnAfterDeserialize()
		{
			commands = new Dictionary<int, Command>();
			foreach (Command cmd in serializedCommands)
			{
				int hash = cmd.Key.GetHashCode();
				if (commands.ContainsKey(hash))
				{
					Debug.LogError("Command list has duplicate keys");
					continue;
				}

				commands.Add(hash, cmd);
			}
		}

		public void OnBeforeSerialize() { }

		// ----------------------------------------------------------------------------------------

		/// <summary> gets the specified command from this command palette </summary>
		/// <param name="commandKeyHash">the hash of the key of the command to get, use
		/// 	string.GetHashCode() to get the hash</param>
		public Command GetCommand(int commandKeyHash)
		{
			if (commands.ContainsKey(commandKeyHash)) return commands[commandKeyHash];
			return null;
		}

		/// <summary> gets the specified command from this command palette </summary>
		/// <param name="commandKey">the key of the command to get</param>
		public Command GetCommand(string commandKey) => GetCommand(commandKey.GetHashCode());
	}

	// --------------------------------------------------------------------------------------------

	public static class CommandPaletteExtension
	{
		/// <summary>
		/// Same as GetCommand, but returns null if this commandpallete is null, instead of 
		/// throwing a null ref exception
		/// </summary>
		/// <param name="a">the command palette to get the command from</param>
		/// <param name="key">the command string</param>
		public static Command TryGetCommand(this CommandPalette a, string key)
		{
			if (a == null) return null;
			return a.GetCommand(key);
		}

		/// <summary>
		/// Same as GetCommand, but returns null if this commandpallete is null, instead of 
		/// throwing a null ref exception
		/// </summary>
		/// <param name="a">the command palette to get the command from</param>
		/// <param name="keyHash">the command string's hashcode</param>
		public static Command TryGetCommand(this CommandPalette a, int keyHash)
		{
			if (a == null) return null;
			return a.GetCommand(keyHash);
		}
	}
}
