using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Actors
{
	[RequireComponent(typeof(Rigidbody2D))]
	public class FreeformMovement : MonoBehaviour
	{
		private Rigidbody2D rbody;
		public Rigidbody2D Rigidbody => rbody == null ? rbody = GetComponent<Rigidbody2D>() : rbody;

		[field: SerializeField]
		public float Speed { get; set; } = 6;

		[field: SerializeField]
		public float AccelerationFactor { get; set; } = 6;

		[field: SerializeField]
		public float PushStrength { get; set; } = 1.25f;

		public Vector2 Movement { get; set; } = default;

		private bool didMove = false;

		[field: SerializeField]
		public bool RotateToMovementVector { get; set; } = false;

		[field: SerializeField]
		public float RotateSpeed { get; set; } = 45;

		// ----------------------------------------------------------------------------------------------

		public void Cmd_MoveFreeform(Command.IVectorCommand cmd) => MoveInDirection(cmd.VectorParam);

		// ----------------------------------------------------------------------------------------------

		public void MoveInDirection(Vector2 direction)
		{
			Movement = direction;
			didMove = true;
		}

		public void RotateTowardMovementVector()
		{
			if (Movement.sqrMagnitude <= 0) return;

			float maxDelta = RotateSpeed * Rigidbody.velocity.sqrMagnitude * Time.deltaTime;
			float angDif = Mathf.DeltaAngle(Rigidbody.rotation, Movement.Angle() * Mathf.Rad2Deg);
			float angVel = Mathf.Clamp(angDif, -maxDelta, maxDelta);

			Rigidbody.rotation += angVel;
		}

		// ----------------------------------------------------------------------------------------------

		private void FixedUpdate()
		{
			if (RotateToMovementVector) RotateTowardMovementVector();

			if (!didMove) return;
			if (Movement.SqrMagnitude() > 1) Movement.Normalize();

			Vector2 targetVel = Movement * Speed;
			float acc = AccelerationFactor * Speed * Time.deltaTime;

			Rigidbody.PushToVelocity(targetVel, acc, acc * Rigidbody.mass * PushStrength);

			Movement.Set(0, 0);
			didMove = false;
		}
	}
}
