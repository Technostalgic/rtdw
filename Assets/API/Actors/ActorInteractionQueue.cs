using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW.Actors
{
	[RequireComponent(typeof(Collider2D))]
	public class ActorInteractionQueue : MonoBehaviour
	{
		[field: SerializeField]
		public bool AutoInteract { get; private set; }

		private Collider2D col2D = null;
		public Collider2D Collider2D => col2D != null ? col2D : col2D = GetComponent<Collider2D>();

		protected List<Interactable> availableInteractives = new List<Interactable>();
		public IReadOnlyList<Interactable> AvailableInteractives => availableInteractives;

		[field: SerializeField]
		public GameObject TargetInteractor { get; private set; } = null;

		// --------------------------------------------------------------------------------------------

		private void Start()
		{
			_ = Collider2D;
		}

		private void OnTriggerEnter2D(Collider2D collision)
		{
			Interactable interactive = collision.gameObject.GetComponent<Interactable>();
			if (interactive != null)
			{
				availableInteractives.Add(interactive);
				if (AutoInteract && interactive.AutoInteract)
				{
					interactive.Interact(TargetInteractor);
				}
			}
		}

		private void OnTriggerExit2D(Collider2D collision)
		{
			Interactable interactive = collision.gameObject.GetComponent<Interactable>();
			if (interactive != null)
			{
				int index = availableInteractives.IndexOf(interactive);
				if (index >= 0) availableInteractives.RemoveAt(index);
			}
		}

		private void OnDisable()
		{
			availableInteractives.Clear();
		}

		// --------------------------------------------------------------------------------------------

		/// <summary>
		/// get the closest available interactible object to the specified point, or null if there is
		/// none available
		/// </summary>
		/// <param name="point">the point to perform distance check from, or self position if unspecified</param>
		public Interactable GetClosest(Vector2? point = null)
		{
			if (!point.HasValue) point = transform.position;

			Interactable r = null;
			float dist = 0;
			for (int i = AvailableInteractives.Count - 1; i >= 0; i--)
			{
				Interactable inter = AvailableInteractives[i];

				if (!inter.isActiveAndEnabled)
				{
					availableInteractives.RemoveAt(i);
					continue;
				}

				float tdist = Vector2.Distance(point.Value, inter.transform.position);
				if (r == null || tdist < dist)
				{
					r = inter;
					dist = tdist;
				}
			}

			return r;
		}

		/// <summary>
		/// Interacts with the closest available Interactible, if there is one
		/// </summary>
		public void InteractWithAvailable()
		{
			Interactable closest = GetClosest();
			if(closest != null)
			{
				closest.Interact(TargetInteractor);
			}
		}
	}
}