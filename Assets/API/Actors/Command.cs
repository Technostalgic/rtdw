using UnityEngine;
using UnityEngine.Events;

namespace RTDW.Actors
{
	[System.Serializable]
	public class Command : Command.ICommandState, Command.IVoidCommand, Command.IAnalogueCommand, Command.IVectorCommand, Command.IAgnosticCommand
	{
		[field: SerializeField]
		public string Key { get; set; } = "Command";

		[field: SerializeField]
		public ParameterType ParametersType { get; private set; } = ParameterType.Void;

		public ref float AnalogueParam => ref analogueParam;
		private float analogueParam = 0f;

		public ref Vector2 VectorParam => ref vectorParam;
		private Vector2 vectorParam = default;

		public ref object[] AgnosticParams => ref agnosticParams;
		private object[] agnosticParams = null;

		/// <summary>
		/// Whether or not the command is currently running and cannot be sent
		/// </summary>
		public bool IsRunning { get; set; }

		// ----------------------------------------------------------------------------------------

		[field: SerializeField, HideInInspector]
		public CommandVoidAction Action { get; private set; } = new CommandVoidAction();

		[field: SerializeField, HideInInspector]
		public CommandAnalogueAction AnalogueAction { get; private set; } = new CommandAnalogueAction();

		[field: SerializeField, HideInInspector]
		public CommandVectorAction VectorAction { get; private set; } = new CommandVectorAction();

		[field: SerializeField, HideInInspector]
		public CommandAgnosticAction AgnosticAction { get; private set; } = new CommandAgnosticAction();

		// ----------------------------------------------------------------------------------------

		public void Send()
		{
			if (IsRunning) return;

			switch (ParametersType)
			{
				case ParameterType.Void: Action?.Invoke(this); break;
				case ParameterType.Analogue: AnalogueAction?.Invoke(this); break;
				case ParameterType.Vector: VectorAction?.Invoke(this); break;
				case ParameterType.Agnostic: AgnosticAction?.Invoke(this); break;
			}
		}

		public void Send(float analogueValue)
		{
			analogueParam = analogueValue;
			Send();
		}

		public void Send(Vector2 vectorValue)
		{
			vectorParam = vectorValue;
			Send();
		}

		public void Send(params object[] args)
		{
			agnosticParams = args;
			Send();
		}

		// ----------------------------------------------------------------------------------------

		public enum ParameterType
		{
			Void,
			Analogue,
			Vector,
			Agnostic
		}

		[System.Serializable]
		public class CommandVoidAction : UnityEvent<IVoidCommand> { }

		[System.Serializable]
		public class CommandAnalogueAction : UnityEvent<IAnalogueCommand> { }

		[System.Serializable]
		public class CommandVectorAction : UnityEvent<IVectorCommand> { }

		[System.Serializable]
		public class CommandAgnosticAction : UnityEvent<IAgnosticCommand> { }

		// ----------------------------------------------------------------------------------------

		public interface ICommandState
		{
			public bool IsRunning { get; set; }
		}

		public interface ICommand
		{
			public void Send();
		}

		public interface IVoidCommand : ICommand, ICommandState { }

		public interface IAnalogueCommand : ICommand, ICommandState
		{
			public ref float AnalogueParam { get; }
			public void Send(float param);
		}

		public interface IVectorCommand : ICommand, ICommandState
		{
			public ref Vector2 VectorParam { get; }
			public void Send(Vector2 param);
		}

		public interface IAgnosticCommand : ICommand, ICommandState
		{
			public ref object[] AgnosticParams { get; }
			public void Send(params object[] param);
		}
	}
}
