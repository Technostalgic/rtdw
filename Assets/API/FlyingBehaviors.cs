using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;
using RTDW.Actors;

namespace RTDW
{
	[RequireComponent(typeof(FreeformMovement), typeof(CommandPalette))]
	public class FlyingBehaviors : MonoBehaviour
	{
		private CommandPalette commandPalette = null;
		public CommandPalette CommandPalette => commandPalette == null ?
			commandPalette = GetComponent<CommandPalette>() : commandPalette;

		private FreeformMovement freeMovement = null;
		public FreeformMovement FreeMovement => freeMovement == null ?
			freeMovement = GetComponent<FreeformMovement>() : freeMovement;

		private AnimancerComponent animancer = null;
		public AnimancerComponent Animancer => animancer == null ? animancer = GetComponent<AnimancerComponent>() : animancer;

		// ----------------------------------------------------------------------------------------

		private readonly int moveCmdHash = "MoveFreeform".GetHashCode();
		private readonly int attackCmdHash = "Attack".GetHashCode();

		private Command moveCommand = null;
		private Command MoveCommand => moveCommand ??= CommandPalette.GetCommand(moveCmdHash);

		private Command attackCommand = null;
		private Command AttackCommand => attackCommand ??= CommandPalette.GetCommand(attackCmdHash);

		// ----------------------------------------------------------------------------------------

		private float groundHeightVariance = 1;

		private float preferredGroundHeight = 3;

		private float maxExploreDirectionLife = 0.5f;

		private float exploreDirectionLife = 0;

		private Vector2 lastgroundSeen = default;

		private Vector2 exploreDirection = default;

		// ----------------------------------------------------------------------------------------

		[SerializeField]
		private AnimationClip flyAnimation = null;

		[SerializeField]
		private AnimationClip lungeAnimation = null;

		private Coroutine lungeCoroutine = null;

		private bool IsLunging => lungeCoroutine != null;

		// ----------------------------------------------------------------------------------------

		public void Explore()
		{
			if (IsLunging) return;

			if (exploreDirectionLife <= 0)
			{
				FindNewExploreDirection();
				exploreDirectionLife = maxExploreDirectionLife;
			}

			MoveCommand.Send(exploreDirection * 0.5f);
			exploreDirectionLife -= Time.deltaTime;
		}

		private void FindNewExploreDirection()
		{
			RaycastHit2D rayhit = Physics2D.Raycast(
				transform.position, Physics2D.gravity.normalized, 10, GameManager.TerrainLayer
			);

			if (rayhit.collider == null)
			{
				exploreDirection = lastgroundSeen - (Vector2)transform.position;
				return;
			}

			lastgroundSeen = transform.position;

			exploreDirection += Random.insideUnitCircle * 1.5f;
			exploreDirection.Normalize();

			// too high
			if (rayhit.distance > preferredGroundHeight + groundHeightVariance)
				exploreDirection.y = -Mathf.Abs(exploreDirection.y);

			// too low
			else if (rayhit.distance < preferredGroundHeight - groundHeightVariance)
				exploreDirection.y = Mathf.Abs(exploreDirection.y);
		}

		// ----------------------------------------------------------------------------------------

		public void LungeTarget(Transform target)
		{
			if (IsLunging) return;

			IEnumerator lungeSequence()
			{
				float maxChargeTime = 0.5f;
				float chargeTime = maxChargeTime;
				AnimancerState animState = Animancer.Play(lungeAnimation, chargeTime);
				animState.Speed = 0;

				while (chargeTime > 0)
				{
					MoveCommand.Send(Vector2.zero);
					chargeTime -= Time.deltaTime;
					yield return null;
				}

				animState.Speed = 1;
				Rigidbody2D rb = FreeMovement.Rigidbody;
				Vector2 dif = (Vector2)target.position - (Vector2)transform.position;
				AttackCommand?.Send(dif);

				float lungeTime = 0.2f;
				float lungeForce = rb.mass * 5;
				while (lungeTime > 0)
				{
					float dt = Time.deltaTime;
					dif = ((Vector2)target.position - (Vector2)transform.position).normalized;
					rb.AddForce(dif * lungeForce);
					lungeTime -= dt;
					FreeMovement.Movement = dif;
					FreeMovement.RotateTowardMovementVector();
					yield return null;
				}

				float cooldownTime = 0.25f;
				Animancer.Play(flyAnimation, cooldownTime);
				yield return new WaitForSeconds(cooldownTime);

				lungeCoroutine = null;
				yield break;
			}

			lungeCoroutine = StartCoroutine(lungeSequence());
		}

		public void CancelLunge()
		{
			if (!IsLunging) return;
			StopCoroutine(lungeCoroutine);
			lungeCoroutine = null;
		}

		// public void HandleAttackCollision(Collision2D col)
		// {
		// 	DamageReceiver dr = col.collider.GetAttachedDamageReceiver();
		// 	if (dr == null) return;
		// 
		// 	dr.Damage(new DamageData((damageType, attackDamage)));
		// 
		// 	Rigidbody2D rb = col.otherRigidbody;
		// 	// apply knowckback
		// }

		// ----------------------------------------------------------------------------------------

		// private void OnCollisionStay2D(Collision2D collision)
		// {
		// 	if(attackCommand.IsRunning) HandleAttackCollision(collision);
		// }

		private void Start()
		{
			Animancer.Play(flyAnimation);
		}
	}
}
