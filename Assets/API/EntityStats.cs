using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	[System.Serializable]
	public class EntityStats : MonoBehaviour, ISerializationCallbackReceiver
	{
		public StatValue this[Stat key]
		{
			get
			{
				EnsureLoaded();
				if (StatValues.ContainsKey(key)) return StatValues[key];
				return null;
			}
		}

		// ----------------------------------------------------------------------------------------

		[field: SerializeField]
		public StatSet StatSet { get; private set; } = null;

		public Dictionary<Stat, StatValue> StatValues = null;

		public bool isLoaded => StatValues != null;

		[SerializeField, HideInInspector]
		private StatValue[] serializedValues = null;

		private readonly List<Stat.Modifier> modifiers = new List<Stat.Modifier>();

		private readonly Dictionary<Stat, List<Stat.Modifier>> statMods = new Dictionary<Stat, List<Stat.Modifier>>();

		public IReadOnlyList<Stat.Modifier> Modifiers { get => modifiers; }

		// --------------------------------------------------------------------------------------------

		public void EnsureLoaded()
		{
			if (!isLoaded) LoadStatValues();
		}

		public void ApplyModifier(Stat.Modifier mod)
		{
			if (!mod.IsValid || modifiers.Contains(mod)) return;

			modifiers.Add(mod);

			if (mod.Stat != null && !statMods.ContainsKey(mod.Stat))
			{
				statMods.Add(mod.Stat, new List<Stat.Modifier>() { mod });
			}

			else statMods[mod.Stat].Add(mod);

			this[mod.Stat].MarkCachedValuesDirty();
		}

		public void UnapplyModifier(Stat.Modifier mod)
		{
			modifiers.Remove(mod);
			statMods[mod.Stat].Remove(mod);

			this[mod.Stat].MarkCachedValuesDirty();
		}

		public float GetModifiedStatValue(Stat stat, float value, float min = float.MinValue, float max = float.MaxValue)
		{
			if (statMods != null && !statMods.ContainsKey(stat)) return value;

			List<Stat.Modifier> mods = statMods[stat];
			float add = 0;
			float mult = 1;

			foreach (Stat.Modifier mod in mods)
			{
				add += mod.Addition;
				mult += mod.Multiplier;
			}

			if (mult < 0) mult = 0;

			float tval = (value + add) * mult;
			return Mathf.Clamp(tval, min, max);
		}

		// ----------------------------------------------------------------------------------------

		private void Start()
		{
			EnsureLoaded();
		}

		private void OnValidate()
		{
			EnsureLoaded();
		}

		// ----------------------------------------------------------------------------------------

		private void SaveStatValues()
		{
			if (StatValues == null)
			{
				return;
			}
			serializedValues = new StatValue[StatValues.Count];
			int i = 0;
			foreach (var kv in StatValues)
			{
				if (kv.Value?.Stat == null) continue;
				serializedValues[i++] = kv.Value;
			}
		}

		private void LoadStatValues()
		{
			Dictionary<Stat, StatValue> values = new Dictionary<Stat, StatValue>();
			for (int i = 0; i < serializedValues.Length; i++)
			{
				// TODO fix this bullshit
				string check = serializedValues[i].Stat?.Label;
				if (check == null) continue;
				///if (!Equals(serializedValues[i].Stat, serializedValues[i].Stat)) continue;
				// Debug.Log(!serializedValues[i].Stat);
				try
				{
					if (values.ContainsKey(serializedValues[i].Stat))
					{
						serializedValues[i] = new StatValue(this, null, 0);
						continue;
					}
					else serializedValues[i].OnPostDeserialize(this);

					values.Add(serializedValues[i].Stat, serializedValues[i]);
				}
				catch (System.Exception e)
				{
					Debug.LogWarning("Don't forget to fix this bs");
					Debug.LogError(e);
				}
			}
			if (values.Count > 0) StatValues = values;
		}

		public void OnBeforeSerialize()
		{
			SaveStatValues();
		}

		public void OnAfterDeserialize()
		{
			LoadStatValues();
		}

		// ----------------------------------------------------------------------------------------

#if UNITY_EDITOR
		public class Friend
		{
			public Friend(EntityStats target)
			{
				this.target = target;
			}

			private EntityStats target;
			public EntityStats Target => target;

			public StatValue[] SerializedValues
			{
				get => target.serializedValues;
				set => target.serializedValues = value;
			}
		}
#endif

		// ----------------------------------------------------------------------------------------

		[System.Serializable]
		public class StatValue
		{
			/// <summary> create a new stat value based on the specified parameters </summary>
			/// <param name="parent">the parent entity stat object that holds this value</param>
			/// <param name="stat">the stat reference that this value is for</param>
			/// <param name="baseValue">the base value of this stat with no modifiers</param>
			/// <param name="min">the min possible value this stat can be after modifiers</param>
			/// <param name="max">the max possible value this stat can be after modifiers</param>
			public StatValue(
				EntityStats parent, Stat stat,
				float baseValue, float min = float.NegativeInfinity,
				float max = float.PositiveInfinity
			)
			{
				Stat = stat;
				Parent = parent;
				BaseValue = baseValue;
				MinValue = min;
				MaxValue = max;
			}

			// ------------------------------------------------------------------------------------

			private float? cachedValue = null;

			private float? cachedFunctionalValue = null;

			// ------------------------------------------------------------------------------------

			[field: SerializeField]
			public Stat Stat { get; private set; } = null;

			[field: SerializeField, HideInInspector] // this will not be serialized by unity so we need to handle manual serialization
			public EntityStats Parent { get; private set; } = null;

			[field: SerializeField]
			public float BaseValue { get; set; } = 0;

			[field: SerializeField]
			public float MinValue { get; set; } = float.PositiveInfinity;

			[field: SerializeField]
			public float MaxValue { get; set; } = float.NegativeInfinity;

			public float Value => 
				cachedValue.HasValue ? 
					cachedValue.Value : 
					(cachedValue = (
						Stat != null ?
							Parent.GetModifiedStatValue(Stat, BaseValue, MinValue, MaxValue) : 
							BaseValue
					)).Value;

			public float FunctionalValue => 
				cachedFunctionalValue.HasValue ? 
					cachedFunctionalValue.Value :
					(cachedFunctionalValue = Stat.ApplyNumericFunction(Value)).Value;

			// ------------------------------------------------------------------------------------

			public void MarkCachedValuesDirty()
			{
				cachedValue = null;
				cachedFunctionalValue = null;
			}

			public void OnPostDeserialize(EntityStats parent)
			{
				if (Parent != null)
				{
					if (Parent != parent)
						Debug.LogError("Stat Value already has Parent stats container");
					return;
				}
				Parent = parent;
			}

			// ------------------------------------------------------------------------------------

			public static implicit operator float(StatValue a) => a.Value;
		}
	}
}
