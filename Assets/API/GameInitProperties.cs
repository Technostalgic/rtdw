using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTDW.Weapons;

namespace RTDW
{
	[CreateAssetMenu(fileName = "GameInitParams", menuName = "RTDW/Game Initialization Parameters")]
	public class GameInitParameters : ScriptableObject
	{

		// ----------------------------------------------------------------------------------------

		[Header("Pool Properties")]
		public ObjectPooling.PoolInitProperties itemPickupPool = null;

		// ----------------------------------------------------------------------------------------

		[Header("Asset References")]
		public StackableItem goldAsset = null;

		// ----------------------------------------------------------------------------------------

		[Header("Other")]
		public ContactFilter2D actorLosFilter = default;

		public ContactFilter2D actorTargetFilter = default;

		// ----------------------------------------------------------------------------------------

		[field: Header("Mod Slot Icons")]

		[field: SerializeField]
		public Sprite ModSlotIcon_Chassis { get; private set; } = null;

		[field: SerializeField]
		public Sprite ModSlotIcon_Stock { get; private set; } = null;

		[field: SerializeField]
		public Sprite ModSlotIcon_Barrel { get; private set; } = null;

		[field: SerializeField]
		public Sprite ModSlotIcon_Clip { get; private set; } = null;

		[field: SerializeField]
		public Sprite ModSlotIcon_Grip { get; private set; } = null;

		[field: SerializeField]
		public Sprite ModSlotIcon_Sights { get; private set; } = null;

		[field: SerializeField]
		public Sprite ModSlotIcon_Chamber { get; private set; } = null;

		[field: SerializeField]
		public Sprite ModSlotIcon_Booster { get; private set; } = null;

		[field: SerializeField]
		public Sprite ModSlotIcon_ChassisMod { get; private set; } = null;

		[field: SerializeField]
		public Sprite ModSlotIcon_StockMod { get; private set; } = null;

		[field: SerializeField]
		public Sprite ModSlotIcon_BarrelMod { get; private set; } = null;

		[field: SerializeField]
		public Sprite ModSlotIcon_ClipMod { get; private set; } = null;

		[field: SerializeField]
		public Sprite ModSlotIcon_GripMod { get; private set; } = null;

		[field: SerializeField]
		public Sprite ModSlotIcon_SightsMod { get; private set; } = null;

		[field: SerializeField]
		public Sprite ModSlotIcon_ChamberMod { get; private set; } = null;

		// ----------------------------------------------------------------------------------------

		[field: Space(20)]

		[field: SerializeField]
		public DamageType DefaultDamageType { get; private set; } = null;

		// ----------------------------------------------------------------------------------------

		[field: SerializeField]
		public GunStatSet GunStats { get; private set; } = null;
	}
}
