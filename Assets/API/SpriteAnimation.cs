using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RTDW
{
	[RequireComponent(typeof(SpriteRenderer))]
	public class SpriteAnimation : MonoBehaviour
	{
		private SpriteRenderer spriteRenderer = null;
		public SpriteRenderer SpriteRenderer => spriteRenderer == null ?
			spriteRenderer = GetComponent<SpriteRenderer>() : spriteRenderer;

		public Sprite[] animationFrames = null;

		public float framesPerSecond = 30;

		public bool loop = false;

		private float animTimeElapsed = 0;

		[SerializeField]
		private bool destroyOnFinish = false;

		public UnityEvent OnAnimFinished = new UnityEvent();

		private bool finishInvokeDirty = false;

		// ----------------------------------------------------------------------------------------

		private void Animate()
		{
			animTimeElapsed += Time.deltaTime;
			if (animationFrames != null && animationFrames.Length > 0)
			{
				int frameIndex = Mathf.FloorToInt(animTimeElapsed * framesPerSecond);
				if (frameIndex >= animationFrames.Length)
				{
					if (!finishInvokeDirty)
					{
						OnAnimFinished.Invoke();
						finishInvokeDirty = true;
						if (destroyOnFinish) Destroy(gameObject);
					}

					if (loop)
					{
						frameIndex %= animationFrames.Length;
						SpriteRenderer.sprite = animationFrames[frameIndex];
					}

					else SpriteRenderer.sprite = animationFrames[animationFrames.Length - 1];
				}

				else SpriteRenderer.sprite = animationFrames[frameIndex];
			}
		}

		public void Restart()
		{
			animTimeElapsed = 0;
			finishInvokeDirty = false;
		}

		// ----------------------------------------------------------------------------------------

		void Update()
		{
			Animate();
		}

		private void OnValidate()
		{
			if (animationFrames != null && animationFrames.Length > 0)
			{
				SpriteRenderer.sprite = animationFrames[0];
			}
		}
	}
}
