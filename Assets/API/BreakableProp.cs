using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(DamageReceiver)), RequireComponent(typeof(Killable))]
	public class BreakableProp : MonoBehaviour, IDamageListener
	{
		private DamageReceiver damage = null;
		public DamageReceiver Damage => damage == null ?
			damage = GetComponent<DamageReceiver>() : damage;

		private Killable killable = null;
		public Killable Killable => killable == null ?
			killable = GetComponent<Killable>() : killable;

		public float damageThreshold = 10;

		// ----------------------------------------------------------------------------------------

		public void OnDamageReceived(IReadonlyDamageData damage)
		{
			if (Damage.TotalDamage > damageThreshold) Killable.Kill();
		}
	}
}
