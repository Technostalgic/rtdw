using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	public abstract class StatSet : ScriptableObject
	{
		public abstract IReadOnlyCollection<Stat> AllStats { get; }

		public bool HasStat(Stat stat)
		{
			foreach (Stat s in AllStats) if (s == stat) return true;
			return false;
		}
	}
}