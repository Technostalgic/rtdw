using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using RTDW.UI;

namespace RTDW
{
	public class GameManager : MonoBehaviour
	{
		public static GameManager Instance { get; private set; } = null;

		public static GameInitParameters InitParams { get; private set; } = null;

		public static GameUI GameUi { get; private set; } = null;

		public static PlayerController Player { get; private set; } = null;

		public static readonly LayerMask physicsGibsLayer = LayerMask.GetMask("PhysicsGibs");

		public static readonly LayerMask ActorsLayer = LayerMask.GetMask("Actors");

		public static readonly LayerMask ActorsAndPeripheralsLayers = LayerMask.GetMask("Actors", "ActorPeripherals");

		public static readonly LayerMask TerrainLayer = LayerMask.GetMask("Terrain");

		public static readonly int SingleColliderQueryLayer = LayerMask.NameToLayer("SingleColliderQuery");
		
		// Init Param Shortcuts: ------------------------------------------------------------------

		public static DamageType DefaultDamageType => InitParams.DefaultDamageType;

		public static StackableItem GoldAsset => InitParams.goldAsset;

		// ----------------------------------------------------------------------------------------

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void Initialize()
		{
			// begin loading the game initialization parameters
			AsyncOperationHandle<GameInitParameters> loadhandle =
				Addressables.LoadAssetAsync<GameInitParameters>(
					"Assets/DataAssets/GameInitParams.asset"
				);
			loadhandle.WaitForCompletion();

			if (Instance == null)
				Instance = new GameObject("GameManager").AddComponent<GameManager>();

			InitParams = loadhandle.Result;
			if (InitParams == null) throw new Exception("Game Init Paramters not found");

			ObjectPooling.Initialize();

			Debug.Log("Game Manager Initialized");
		}

		private void HandleInput()
		{
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				GameUi.CloseMenus();
			}
			if (Input.GetKeyDown(KeyCode.Tab))
			{
				if (!GameUi.WeaponModMenu.gameObject.activeSelf) 
				{
					GameUi.CloseMenus();
					GameUi.OpenWeaponModMenu();
				}

				else GameUi.CloseMenus();
			}
		}

		// ----------------------------------------------------------------------------------------

		private void Awake()
		{
			if (Instance != null && Instance != this)
			{
				Destroy(gameObject);
				Debug.LogError("GameManager class is not allowed to have multiple instances");
				return;
			}

			GameUi = FindObjectOfType<GameUI>();
			Player = FindObjectOfType<PlayerController>();

			Instance = this;
			Instance.transform.SetAsFirstSibling();

			GameUi.CloseMenus();
		}

		private void Update()
		{
			HandleInput();
		}
	}

	// --------------------------------------------------------------------------------------------

	public enum CompareMode
	{
		LessOrEqual,
		GreaterOrEqual,
		Less,
		Greater,
		Equal
	}

	public static class Extensions
	{
		private static ContactFilter2D qContactFilter = new ContactFilter2D()
		{
			useLayerMask = true,
			layerMask = 1 << GameManager.SingleColliderQueryLayer,
		};

		// Component: -----------------------------------------------------------------------------

		/// <summary>
		/// Copies the values from the source component into the target component.
		/// sourced from https://answers.unity.com/questions/530178/how-to-get-a-component-from-an-object-and-add-it-t.html
		/// </summary>
		/// <typeparam name="T">The component type to copy values of</typeparam>
		/// <param name="target"> the target component who's values will be modified </param>
		/// <param name="source"> where to get the component values from </param>
		public static T CopyComponentValues<T>(this Component target, T source) where T : Component
		{
			Type type = target.GetType();
			if (type != source.GetType()) return null; // type mis-match
			BindingFlags flags =
				BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance |
				BindingFlags.Default | BindingFlags.DeclaredOnly;

			PropertyInfo[] pinfos = type.GetProperties(flags);
			foreach (var pinfo in pinfos)
			{
				if (pinfo.CanWrite)
				{
					try { pinfo.SetValue(target, pinfo.GetValue(source, null), null); }

					// In case of NotImplementedException being thrown. For some reason specifying
					// that exception didn't seem to catch it, so I didn't catch anything specific.
					catch { }
				}
			}

			FieldInfo[] finfos = type.GetFields(flags);
			foreach (var finfo in finfos)
			{
				finfo.SetValue(target, finfo.GetValue(source));
			}
			return target as T;
		}

		public static DamageReceiver GetAttachedDamageReceiver(this Collider2D collider)
		{
			DamageReceiver dr = collider.GetComponent<DamageReceiver>();

			if (dr == null && collider.attachedRigidbody != null)
				dr = collider.attachedRigidbody.GetComponent<DamageReceiver>();

			return dr;
		}

		/// <summary>
		/// Get the count of children in the transform, but only count the children who are active
		/// </summary>
		/// <param name="t">the transform whose chilren to count</param>
		public static int GetActiveChildCount(this Transform t)
		{
			int count = 0;
			foreach (Transform child in t) if (child.gameObject.activeSelf) count++;
			return count;
		}

		// UI: ------------------------------------------------------------------------------------

		public static Rect GetScreenRect(this RectTransform rt)
		{
			Vector2 size = rt.rect.size * rt.lossyScale;
			return new Rect(
				rt.position.x - (rt.pivot.x * size.x), rt.position.y - (rt.pivot.y * size.y),
				size.x, size.y
			);
		}

		// Physics: -------------------------------------------------------------------------------

		/// <summary>
		/// gets the velocity of the specified collider (uses the attached rigid body's velocity 
		/// if there is one, otherwise returns Vector2.zero)
		/// </summary>
		/// <param name="col">the collider to get the velocity of</param>
		public static Vector2 GetVelocity(this Collider2D col)
		{
			if (col.attachedRigidbody != null) return col.attachedRigidbody.velocity;
			return Vector2.zero;
		}

		public static void AddForceAtPositionCapped(
			this Rigidbody2D rb, in Vector2 force, in Vector2 position, in ForceMode2D mode,
			in float maxDeltaSpeed = float.PositiveInfinity,
			in float maxDeltaAngularSpeed = float.PositiveInfinity)
		{
			Vector2 oVel = rb.velocity;
			float oAngVel = rb.angularVelocity;
			rb.AddForceAtPosition(force, position, mode);

			Vector2 dVel = rb.velocity - oVel;
			float dAngVel = rb.angularVelocity - oAngVel;
			if (dVel.sqrMagnitude > maxDeltaSpeed * maxDeltaSpeed)
			{
				Vector2 tVel = oVel + dVel.normalized * maxDeltaSpeed;
				rb.velocity = tVel;
			}
			if (Mathf.Abs(dAngVel) > maxDeltaAngularSpeed)
			{
				float tAngVel = oAngVel + Mathf.Sign(dAngVel) * maxDeltaAngularSpeed;
				rb.angularVelocity = tAngVel;
			}
			// TODO calculate how much speed variance there is and cap it at the specified values
		}

		/// <summary>
		/// pushes a rigidbody toward the specified target velocity using a physics impulse,
		/// capped at the specified max acceleration and force
		/// </summary>
		/// <param name="rb">the rigid body to push</param>
		/// <param name="targetVel">the velocity that the rigid body is trying to achieve</param>
		/// <param name="maxAcceleration">the maximum acceleration that can be applied</param>
		/// <param name="maxForce">the maximum impulse magnitude that can be applied</param>
		public static void PushToVelocity(
			this Rigidbody2D rb, in Vector2 targetVel,
			in float maxAcceleration, in float maxForce)
		{
			// calculate the velocity error
			Vector2 dif = targetVel - rb.velocity;

			// calc a force proportional to the error clamped at max force
			Vector2 force = Vector2.ClampMagnitude(
				Vector2.ClampMagnitude(dif, maxAcceleration) * rb.mass,
				maxForce);

			rb.AddForce(force, ForceMode2D.Impulse);
		}

		/// <summary>
		/// pushes a rigidbody to match a specific velocity along a specified arbitrary axis
		/// using a physics impulse clamped by a max acceleration and force 
		/// </summary>
		/// <param name="rb">the rigidbody to push</param>
		/// <param name="axis">the axis in which the velocity is measured</param>
		/// <param name="targetVel">the desired final velocity in the specifeid axis</param>
		/// <param name="maxAcceleration">the max acceleration that possible in that axis</param>
		/// <param name="maxForce">the max amount of force that can be applied</param>
		public static void PushToAxisVelocity(
			this Rigidbody2D rb, in Vector2 axis, in float targetVel,
			in float maxAcceleration, in float maxForce)
		{
			float curVel = Vector2.Dot(axis, rb.velocity);
			float error = targetVel - curVel;

			float force = Mathf.Clamp(error, -maxAcceleration, maxAcceleration) * rb.mass;
			Vector2 forceProjection = Vector2.ClampMagnitude(axis * force, maxForce);

			rb.AddForce(forceProjection, ForceMode2D.Impulse);
		}

		/// <summary> Raycast against a single collider object </summary>
		/// <param name="col">the collider to test against</param>
		/// <param name="outResults">the results list to store the raycast hits in</param>
		/// <param name="origin">the beginning of the raycast</param>
		/// <param name="direction">the direction the ray points in</param>
		/// <param name="distance">the distance for the ray to check for hits</param>
		public static void RaycastAgainstCollider(
			this Collider2D col, List<RaycastHit2D> outResults,
			in Vector2 origin, in Vector2 direction, in float distance)
		{
			int oLayer = col.gameObject.layer;
			col.gameObject.layer = GameManager.SingleColliderQueryLayer;
			Physics2D.Raycast(origin, direction, qContactFilter, outResults, distance);
			col.gameObject.layer = oLayer;
		}

		/// <summary> Circle cast against a single collider object </summary>
		/// <param name="col">the collider to test against</param>
		/// <param name="outResults">the results list to store the raycast hits in</param>
		/// <param name="origin">the beginning of the raycast</param>
		/// <param name="radius">the width of the raycast</param>
		/// <param name="direction">the direction the ray points in</param>
		/// <param name="distance">the distance for the ray to check for hits</param>
		public static void CircleCastAgainstCollider(
			this Collider2D col, List<RaycastHit2D> outResults,
			in Vector2 origin, in float radius, in Vector2 direction, in float distance)
		{
			int oLayer = col.gameObject.layer;
			col.gameObject.layer = GameManager.SingleColliderQueryLayer;
			Physics2D.CircleCast(origin, radius, direction, qContactFilter, outResults, distance);
			col.gameObject.layer = oLayer;
		}

		// Math: ----------------------------------------------------------------------------------

		/// <summary>
		/// compares value b to value a, as a is the left hand operand where b is the righthand 
		/// operand, given the specified comparison mode
		/// </summary>
		/// <param name="compareMode">comparison mode</param>
		/// <param name="a">lefthand operand</param>
		/// <param name="b">righthand operand</param>
		public static bool CompareValue(this CompareMode compareMode, in float a, in float b)
		{
			switch (compareMode)
			{
				case CompareMode.Equal: return a == b;
				case CompareMode.GreaterOrEqual: return a >= b;
				case CompareMode.LessOrEqual: return a <= b;
				case CompareMode.Greater: return a > b;
				case CompareMode.Less: return a < b;
			}
			return false;
		}

		/// <summary>
		/// rotates the specified vector by 90 degrees clockwise in a very optomized way, note this
		/// DOES modify the given vector's value
		/// </summary>
		/// <param name="a">the vector to rotate</param>
		public static ref Vector2 RotateCWRef(this ref Vector2 a)
		{
			a.Set(a.y, -a.x);
			return ref a;
		}

		/// <summary>
		/// rotates the specified vector by 90 degrees counter-clockwise in a very optomized way, 
		/// note this DOES modify the given vector's value
		/// </summary>
		/// <param name="a">the vector to rotate</param>
		public static ref Vector2 RotateCCWRef(this ref Vector2 a)
		{
			a.Set(-a.y, a.x);
			return ref a;
		}

		/// <summary>
		/// rotates the specified vector by 90 degrees clockwise in a very optomized way
		/// </summary>
		public static Vector2 RotateCW(this Vector2 a)
		{
			a.Set(a.y, -a.x);
			return a;
		}

		/// <summary>
		/// rotates the specified vector by 90 degrees counter-clockwise in a very optomized way
		/// </summary>
		public static Vector2 RotateCCW(this Vector2 a)
		{
			a.Set(-a.y, a.x);
			return a;
		}

		/// <summary>
		/// get the angle direction that the specified vector is pointing, in radians
		/// </summary>
		/// <param name="a">the angle</param>
		public static float Angle(this Vector2 a)
		{
			return Mathf.Atan2(a.y, a.x);
		}

		// Collections: ---------------------------------------------------------------------------

		/// <summary> checks to see if the specified collection contains an object </summary>
		/// <typeparam name="T">the object type that the collection contains</typeparam>
		/// <param name="a">the collection to check</param>
		/// <param name="obj">the object to look for</param>
		public static bool Contains<T>(this IReadOnlyCollection<T> a, T obj)
		{
			if (a is ICollection<T> collection) return collection.Contains(obj);
			foreach (T item in a) if (item.Equals(obj)) return true;
			return false;
		}

		/// <summary>
		/// gets the appropriate sprite for the specified weapon mod slot type
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static Sprite SlotTypeToSprite(this Weapons.WeaponMod.SlotType type)
		{
			return GameUI.Instance.GetWeaponSlotSprite(type);
		}
	}
}