using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTDW.Actors;
using RTDW.Weapons;

namespace RTDW
{
	[SelectionBase]
	[RequireComponent(typeof(Rigidbody2D))]
	[RequireComponent(typeof(WeaponEquipper))]
	public class PlayerController : MonoBehaviour
	{
		[field: SerializeField]
		public float JumpStrength { get; private set; } = 10;

		[field: SerializeField, Range(0, 1)]
		public float JumpSustain { get; private set; } = 0.25f;

		[SerializeField]
		private float accelerationFactor = 4;

		[SerializeField]
		private float speed = 25;

		[field: SerializeField, Range(0, 1)]
		public float GroundStoppingCoefficient { get; private set; } = 0.9f;

		[field: SerializeField]
		public Collider2D Collider2D { get; private set; } = null;

		[field: SerializeField]
		public ActorInteractionQueue InteractionQueue { get; private set; } = null;

		[field: SerializeField]
		public Weapon Gun { get; private set; } = null;

		[field: SerializeField]
		public Transform WeaponContainer { get; private set; } = null;

		// --------------------------------------------------------------------------------------------

		public Vector2 moveVector = default;

		public Vector2 MoveVector => moveVector;

		public Vector2 AimVector
		{
			get => WeaponContainer.localPosition;
			private set
			{
				WeaponContainer.localPosition = new Vector3(
					value.x, value.y, WeaponContainer.localPosition.z
				);
			}
		}

		private bool flag_jumpCommandReceived = false;
		private bool flag_didTryJump = false;
		private bool flag_didDropPlatform = false;

		public Vector2 Velocity
		{
			get { return Rigidbody2D.velocity; }
			set { Rigidbody2D.velocity = value; }
		}

		private Rigidbody2D rbody2D = null;
		public Rigidbody2D Rigidbody2D
		{
			get
			{
				if (rbody2D == null) rbody2D = GetComponent<Rigidbody2D>();
				return rbody2D;
			}
		}

		private Collider2D currentGround = null;
		public Collider2D CurrentGround
		{
			get
			{
				if (flag_currentGroundDirty) FindCurrentGround();
				return currentGround;
			}
		}

		private bool flag_currentGroundDirty = true;

		public Vector2 CurrentGroundNormal { get; private set; } = new Vector2();

		public bool OnGround { get => CurrentGround != null; }

		private readonly HashSet<Collider2D> IgnoredPlatforms = new HashSet<Collider2D>();

		// --------------------------------------------------------------------------------------------

		public void Cmd_DropPlatform(Command.IVoidCommand cmd)
		{
			flag_didDropPlatform = true;
		}

		public void Cmd_MoveHorizontal(Command.IAnalogueCommand cmd)
		{
			moveVector.x = cmd.AnalogueParam;
		}

		public void Cmd_Jump(Command.IVoidCommand cmd)
		{
			flag_jumpCommandReceived = true;
		}

		public void Cmd_Aim(Command.IVectorCommand cmd)
		{
			AimVector += cmd.VectorParam;

			float aimLength = 0.5f;
			if (AimVector.magnitude > aimLength) AimVector = AimVector.normalized * aimLength;

			Gun.transform.rotation = Quaternion.Euler(0, 0, AimVector.Angle() * Mathf.Rad2Deg);
		}

		// --------------------------------------------------------------------------------------------

		private void HandleInput()
		{
			// moveVector.Set(0, 0);

			// if (Input.GetKey(moveLeft)) moveVector.x -= 1;
			// if (Input.GetKey(moveRight)) moveVector.x += 1;
			// if (Input.GetKey(moveUp)) moveVector.y += 1;
			// if (Input.GetKey(moveDown)) moveVector.y -= 1;

			// for debug
			if (Input.GetKeyDown(KeyCode.Backslash))
			{
				transform.position = Vector2.zero;
				Rigidbody2D.MovePosition(Vector2.zero);
				Velocity = Vector2.zero;
			}

			if (flag_jumpCommandReceived)
			{
				if (!flag_didTryJump) TryJump();
				flag_didTryJump = true;
			}
			else flag_didTryJump = false;
			flag_jumpCommandReceived = false;

			if (flag_didDropPlatform) DropPlatforms();
			else UndropPlatforms();
			flag_didDropPlatform = false;
		}

		private void HandleMovement()
		{
			if (Mathf.Abs(moveVector.x) > Mathf.Epsilon)
			{

				// flag_didMove = true;
				float targetVel = moveVector.x * speed;
				//float tVel = Vector2.Dot(transform.right, Velocity);

				//if (Mathf.Sign(tVel) != Mathf.Sign(moveVector.x) && OnGround) tVel *= GroundStoppingCoefficient;

				float acc = accelerationFactor * speed * Time.deltaTime;
				// if (acc < Mathf.Abs(targetVel - tVel)) tVel += acc * Mathf.Sign(targetVel - tVel);
				// else tVel = targetVel;

				Rigidbody2D.PushToAxisVelocity(transform.right, targetVel, acc, acc * Rigidbody2D.mass);

				//Velocity = new Vector2(tVel, Velocity.y);
			}

			// apply friction if on ground
			else if (OnGround)
			{
				Velocity = new Vector2(GroundStoppingCoefficient * Velocity.x, Velocity.y);
			}

			if (!OnGround && flag_didTryJump)
			{
				SustainJump();
			}

			moveVector.Set(0, 0);
		}

		public void TryJump()
		{
			if (!OnGround) return;
			Jump();
		}

		private void Jump()
		{
			Velocity = new Vector2(Velocity.x, Velocity.y + JumpStrength);
		}

		private void SustainJump()
		{
			if (Velocity.y <= 0) return;
			Velocity += Physics2D.gravity * -JumpSustain * Time.deltaTime;
		}

		private void DropPlatforms()
		{
			if (CurrentGround == null) return;

			if (CurrentGround.usedByEffector)
			{
				PlatformEffector2D platform = CurrentGround.GetComponent<PlatformEffector2D>();
				if(platform != null)
				{
					Physics2D.IgnoreCollision(Collider2D, CurrentGround, true);
					IgnoredPlatforms.Add(CurrentGround);
				}
			}
		}

		private void UndropPlatforms()
		{
			if (IgnoredPlatforms.Count <= 0) return;

			foreach(var plat in IgnoredPlatforms)
			{
				Physics2D.IgnoreCollision(Collider2D, plat, false);
			}
			IgnoredPlatforms.Clear();
		}

		// --------------------------------------------------------------------------------------------

		private void FindCurrentGround()
		{
			float rayWidth = 0;
			float rayDist = 0.01f;
			float maxExtentLength = 1;
			Vector2 start = transform.position;
			if (Collider2D != null)
			{
				// rayWidth = Mathf.Min(Collider2D.bounds.extents.x, Collider2D.bounds.extents.y);
				rayDist = Mathf.Max(Collider2D.bounds.extents.x, Collider2D.bounds.extents.y) + 0.1f;
				maxExtentLength = Mathf.Max(Collider2D.bounds.extents.x, Collider2D.bounds.extents.y) + 0.1f;

				Vector2 centroid = Collider2D.transform.TransformPoint(Collider2D.offset);
				Vector2 gravDir = Physics2D.gravity.normalized;
				start = Collider2D.ClosestPoint(centroid + gravDir * maxExtentLength) - gravDir * (rayDist * 0.5f);
			}

			RaycastHit2D[] hits = Physics2D.RaycastAll(start, Physics2D.gravity, rayDist);
			RaycastHit2D? closest = null;
			for (int i = hits.Length - 1; i >= 0; i--)
			{
				if (hits[i].collider.attachedRigidbody == Rigidbody2D) continue;
				if (closest.HasValue)
				{
					if (hits[i].distance < closest.Value.distance) closest = hits[i];
				}
				else closest = hits[i];
			}
			if (closest.HasValue)
				TestColliderForGround(closest.Value.collider, closest.Value.normal);

			flag_currentGroundDirty = false;
		}

		private void TestColliderForGround(Collider2D collider, Vector2 colNormal)
		{
			if (flag_didTryJump)
			{
				Vector2 colVel = collider.GetVelocity();
				Vector2 upDir = -Physics2D.gravity.normalized;
				float colUpwardVel = Vector2.Dot(upDir, colVel);
				float selfUpwardVel = Vector2.Dot(upDir, Velocity);
				if (selfUpwardVel > colUpwardVel) return;
			}

			// if the normal is facing the opposite direction of gravity, it can be considered a ground
			if (Vector2.Dot(Physics2D.gravity.normalized, colNormal) < -0.5)
			{
				currentGround = collider;
				CurrentGroundNormal = colNormal;
				flag_currentGroundDirty = false;
			}
		}

		// --------------------------------------------------------------------------------------------

		private void Start()
		{
			_ = Rigidbody2D;
		}

		private void FixedUpdate()
		{
			// flag_didMove = false;
			HandleMovement();

			currentGround = null;
			flag_currentGroundDirty = true;
		}

		private void OnCollisionStay2D(Collision2D collision)
		{
			// find average contact normal
			Vector2 norm = Vector2.zero;
			float lenRecip = 1f / (float)collision.contacts.Length;
			for (int i = collision.contacts.Length - 1; i >= 0; i--)
			{
				norm += collision.contacts[i].normal * lenRecip;
			}

			norm.Normalize();
			TestColliderForGround(collision.collider, norm);
		}

		private void Update()
		{
			HandleInput();
		}
	}
}