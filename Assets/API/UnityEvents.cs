using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using RTDW.Weapons;

namespace RTDW
{
	[Serializable]
	public class GunUpdateBehavior : UnityEvent<BehaviorTrait, Weapon, float> { }

	[Serializable]
	public class GunFireBehavior : UnityEvent<BehaviorTrait, Weapon, IReadOnlyList<Projectile>> { }

	[Serializable]
	public class GunActionBehavior : UnityEvent<BehaviorTrait, Weapon> { }
}