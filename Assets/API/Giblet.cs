using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	[DisallowMultipleComponent]
	public class Giblet : MonoBehaviour
	{
		public SpriteRenderer spriteRenderer;
		public Rigidbody2D rbody;
		public BoxCollider2D boxCollider;
		public CircleCollider2D circleCollider;
		public Collider2D activeCollider;

		public Vector2 SpriteWorldSize =>
			new Vector2(spriteRenderer.sprite.rect.width, spriteRenderer.sprite.rect.height) /
			spriteRenderer.sprite.pixelsPerUnit;

		// ----------------------------------------------------------------------------------------

		private void VerifyActiveCollider()
		{
			if (activeCollider == null)
				activeCollider = boxCollider.enabled ? (Collider2D)
				boxCollider : circleCollider;

			if (activeCollider == boxCollider)
			{
				boxCollider.enabled = true;
				circleCollider.enabled = false;
			}
			else
			{
				boxCollider.enabled = false;
				circleCollider.enabled = true;
			}
		}

		public void SetColliderSize(Vector2 size, float edgeRadius)
		{
			if (activeCollider == boxCollider)
			{
				boxCollider.size = size;
				boxCollider.edgeRadius = edgeRadius;
			}
			else
			{
				circleCollider.radius = Mathf.Min(size.x) + edgeRadius;
			}
		}

		// ----------------------------------------------------------------------------------------

		private void OnEnable()
		{
			VerifyActiveCollider();
		}
	}
}
