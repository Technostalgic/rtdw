using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	[DisallowMultipleComponent]
	public abstract class Corpse : MonoBehaviour
	{
		public abstract void CreateCorpseFromEntity(GameObject go);
	}
}
