using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTDW.Weapons;

namespace RTDW
{
	public sealed class AnimationBone : MonoBehaviour, IProjectileListener
	{
		private float lastRotation = default;

		private float currentRotation = default;

		private Vector2 lastPosition = default;

		private Vector2 currentPosition = default;

		private float timeDelta = 0;

		private Vector2? cachedExtrapolatedVelocity = default;
		public Vector2 ExtrapolatedVelocity
		{
			get
			{
				if (!cachedExtrapolatedVelocity.HasValue)
					cachedExtrapolatedVelocity = (currentPosition - lastPosition) / timeDelta;
				return cachedExtrapolatedVelocity.Value;
			}
		}

		private float? cachedExtrapolatedAngularVelocity = default;
		public float ExtrapolatedAngularVelocity
		{
			get
			{
				if (!cachedExtrapolatedAngularVelocity.HasValue)
					cachedExtrapolatedAngularVelocity = Mathf.DeltaAngle(
						lastRotation, currentRotation
					) / timeDelta;
				return cachedExtrapolatedAngularVelocity.Value;
			}
		}

		[SerializeField]
		private bool trackProjectileHits = true;

		private List<HitInfo> projectileHits = null;
		public IReadOnlyList<HitInfo> ProjectileHits => projectileHits;

		// ----------------------------------------------------------------------------------------

		public void OnProjectileHit(RaycastHit2D hit, Projectile proj)
		{
			if (!trackProjectileHits) return;
			if (projectileHits == null) projectileHits = new List<HitInfo>();

			projectileHits.Add(new HitInfo(Time.time, this, hit, proj));
		}

		// ----------------------------------------------------------------------------------------

		/// <summary>
		/// resets the last and current position in such a way that the extrapolated 
		/// velocities are 0
		/// </summary>
		private void ResetLastPosition()
		{
			timeDelta = Time.deltaTime;

			lastRotation = transform.rotation.eulerAngles.z;
			currentRotation = lastRotation;

			lastPosition = transform.position;
			currentPosition = lastPosition;

			cachedExtrapolatedVelocity = null;
			cachedExtrapolatedAngularVelocity = null;
		}

		private void UpdateLastPosition()
		{
			cachedExtrapolatedVelocity = null;
			timeDelta = Time.deltaTime;

			lastPosition = currentPosition;
			currentPosition = transform.position;

			lastRotation = currentRotation;
			currentRotation = transform.rotation.eulerAngles.z;

			cachedExtrapolatedVelocity = null;
			cachedExtrapolatedAngularVelocity = null;
		}

		// ----------------------------------------------------------------------------------------

		private void Start()
		{
			ResetLastPosition();
		}

		private void FixedUpdate()
		{
			UpdateLastPosition();
		}

		// ----------------------------------------------------------------------------------------

		[System.Serializable]
		public class HitInfo
		{
			public HitInfo(float timestamp, AnimationBone boneHit, RaycastHit2D hit, Projectile proj)
			{
				Vector2 otherVel = Vector2.zero;
				AnimationBone hitBone = hit.collider.GetComponent<AnimationBone>();
				if (hitBone != null) otherVel = hitBone.ExtrapolatedVelocity;
				Vector2 imp = proj.GetImpulseForHit(hit, otherVel);

				timeStamp = timestamp;
				bone = boneHit;
				rayHit = hit;
				impulse = imp;
			}

			public readonly float timeStamp = default;
			public readonly AnimationBone bone = null;
			public readonly RaycastHit2D rayHit = default;
			public readonly Vector2 impulse = default;
		}
	}
}
