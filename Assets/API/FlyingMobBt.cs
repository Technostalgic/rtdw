using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTDW.AI;
using RTDW.Actors;

namespace RTDW
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(CommandPalette), typeof(FlyingBehaviors))]
	public class FlyingMobBt : BehaviorTree, IBtComponent_CommandPalette, IBtData_Target
	{
		public Transform Target { get; set; }

		private CommandPalette commands = null;
		public CommandPalette Commands => commands == null ?
			commands = GetComponent<CommandPalette>() : commands;

		private FlyingBehaviors behaviors = null;
		public FlyingBehaviors Behaviors => behaviors == null ?
			behaviors = GetComponent<FlyingBehaviors>() : behaviors;

		// ----------------------------------------------------------------------------------------

		public override void SetupBehaviorTree()
		{
			base.SetupBehaviorTree();
			RootNode = new SelectorNode(new BehaviorNode[]
			{
				new SequenceNode(new BehaviorNode[]
				{
					new Node_CanSeeTarget(),
					new Node_CanAttackTarget(2),
					new Node_AttackTargetSequence()
				}),
				new SequenceNode(new BehaviorNode[]
				{
					new Node_FindTarget(8),
					new Node_CanSeeTarget(),
					new Node_GoToTarget()
				}),
				new Node_Explore()
			});
		}

		// ----------------------------------------------------------------------------------------

		public class Node_CanAttackTarget : BehaviorNode.RequireTarget
		{
			public Node_CanAttackTarget(float attackRange) => AttackRange = attackRange;

			public float AttackRange { get; set; } = 2;

			public override BehaviorState Evaluate()
			{
				if (Target == null) return BehaviorState.Failed;

				if (Vector2.Distance(Tree.transform.position, Target.position) <= AttackRange)
					return BehaviorState.Succeeded;

				return BehaviorState.Failed;
			}
		}

		public class Node_AttackTargetSequence : BehaviorNode.RequireCmdPaletteAndTarget
		{
			public override BehaviorState Evaluate()
			{
				if (Target == null) return BehaviorState.Failed;

				(Tree as FlyingMobBt).Behaviors.LungeTarget(Target);

				return BehaviorState.Running;
			}
		}

		public class Node_FindTarget : BehaviorNode.RequireTarget
		{
			public Node_FindTarget(float range) => FindRange = range;

			public float FindRange { get; set; } = 8;

			public override BehaviorState Evaluate()
			{
				if (Target == null) FindNewTarget();
				if (Target == null) return BehaviorState.Failed;

				return BehaviorState.Succeeded;
			}

			private void FindNewTarget()
			{
				Collider2D[] cols = Physics2D.OverlapCircleAll(
					Tree.transform.position, FindRange, GameManager.ActorsLayer
				);

				foreach (Collider2D col in cols)
				{
					if (col.transform.IsChildOf(Tree.transform)) continue;
					PlayerController dr = col.attachedRigidbody?.GetComponent<PlayerController>();
					if (dr == null) continue;
					Target = dr.transform;
				}
			}
		}

		public class Node_CanSeeTarget : BehaviorNode.RequireTarget
		{
			public override BehaviorState Evaluate()
			{
				if (Target == null) return BehaviorState.Failed;

				Vector2 dif = Target.position - Tree.transform.position;
				RaycastHit2D rayHit = Physics2D.Raycast(Tree.transform.position, dif.normalized, dif.magnitude, GameManager.TerrainLayer);
				if (rayHit.collider == null) return BehaviorState.Succeeded;

				return BehaviorState.Failed;
			}
		}

		public class Node_GoToTarget : BehaviorNode.RequireCmdPaletteAndTarget
		{
			public override BehaviorState Evaluate()
			{
				Command move = Commands.TryGetCommand("MoveFreeform");
				if (move != null)
				{
					Vector2 dif = Target.position - Tree.transform.position;
					move.Send(dif.normalized);
				}

				return BehaviorState.Running;
			}
		}

		public class Node_Explore : BehaviorNode
		{
			public override BehaviorState Evaluate()
			{
				(Tree as FlyingMobBt).Behaviors.Explore();
				return BehaviorState.Running;
			}
		}
	}
}
