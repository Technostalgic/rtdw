using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RTDW
{
	public class DamageThreshold : MonoBehaviour, IDamageListener
	{
		[field: SerializeField, Tooltip("The threshold that needs to be reached")]
		public float Threshold { get; set; } = 10;

		private DamageReceiver damageReceiver = null;
		public DamageReceiver DamageReceiver => damageReceiver == null ?
			damageReceiver = GetComponent<DamageReceiver>() : damageReceiver;

		[field: SerializeField]
		public UnityEvent OnThresholdReached { get; private set; } = new UnityEvent();

		// ----------------------------------------------------------------------------------------

		public void OnDamageReceived(IReadonlyDamageData damage)
		{
			if (DamageReceiver.TotalDamage >= Threshold) OnThresholdReached.Invoke();
		}
	}
}
