using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTDW
{
	public class GibletSpawner : Corpse
	{
		[field: SerializeField]
		public Giblet GibletToSpawn { get; set; } = null;

		[field: SerializeField]
		public Sprite[] GibletSprites { get; set; } = null;

		[field: SerializeField]
		public int GibletCount { get; set; } = 5;

		[field: SerializeField]
		public float BurstSpeed { get; set; } = 10;

		[field: Header("Collider Settings"), SerializeField]
		[field: Tooltip("Whether or not these collider settings are applied to the gibs")]
		public bool UseColliderSettings { get; set; } = true;

		[field: SerializeField]
		public ColliderMode ColliderType { get; set; } = ColliderMode.Default;

		[field: SerializeField, Tooltip("The rounded edge radius for box collider, or just added to radius if circle collider")]
		public float ColliderEdgeRadius { get; set; } = 0;

		[field: SerializeField, Tooltip("Whether the collider will always be the same size (Absolute) or be offset from the size of the sprite")]
		public ColliderSizeMode SizeMode { get; set; } = ColliderSizeMode.RelativeToSprite;

		[field: SerializeField, Tooltip("The width and height of the box collider, for circle collider, the x value will be used for radius")]
		public Vector2 ColliderSize { get; set; } = default;

		// ----------------------------------------------------------------------------------------

		private Giblet GetNewGiblet()
		{
			return Instantiate(GibletToSpawn);
		}

		private void SpawnGibs(GameObject parent)
		{
			Rigidbody2D parentrb = parent.GetComponent<Rigidbody2D>();

			for (int i = GibletCount; i > 0; i--)
			{
				float dir = Random.Range(-Mathf.PI, Mathf.PI);
				Giblet gib = GetNewGiblet();
				gib.transform.position = transform.position;

				SpriteRenderer sr = gib.GetComponent<SpriteRenderer>();
				sr.sprite = GibletSprites[Random.Range(0, GibletSprites.Length)];

				float radius = 0.5f;
				Collider2D col = gib.GetComponent<Collider2D>();
				if (col != null)
				{
					if (col is BoxCollider2D box)
					{
						box.size =
						new Vector2(sr.sprite.rect.width, sr.sprite.rect.height) /
						sr.sprite.pixelsPerUnit;
						radius = Mathf.Min(box.size.x, box.size.y) * 0.5f;
					}
					else if (col is CircleCollider2D circ)
					{
						circ.radius =
						Mathf.Max(sr.sprite.rect.width, sr.sprite.rect.height) /
						(2 * sr.sprite.pixelsPerUnit);
						radius = circ.radius;
					}
				}

				Vector2 dirNorm = new Vector2(Mathf.Cos(dir), Mathf.Sin(dir));
				transform.position += (Vector3)dirNorm * Random.Range(0, radius);

				gib.rbody.angularVelocity = Random.Range(-1000f, 1000f);
				gib.rbody.velocity = dirNorm * BurstSpeed;
				if (parentrb != null) gib.rbody.velocity += parentrb.velocity;

				if (UseColliderSettings) ApplyColliderSettings(gib);
			}
		}

		private void ApplyColliderSettings(Giblet gib)
		{
			if (ColliderType != ColliderMode.Default)
			{
				Collider2D active = null;
				Collider2D inactive = null;
				if (ColliderType == ColliderMode.Box)
				{
					active = gib.boxCollider;
					inactive = gib.circleCollider;
				}
				else
				{
					active = gib.circleCollider;
					inactive = gib.boxCollider;
				}
				active.enabled = true;
				inactive.enabled = false;
				gib.activeCollider = active;
			}

			Vector2 size = ColliderSize;
			if (SizeMode == ColliderSizeMode.RelativeToSprite) size += gib.SpriteWorldSize;
			gib.SetColliderSize(size, ColliderEdgeRadius);
		}

		// ----------------------------------------------------------------------------------------

		public override void CreateCorpseFromEntity(GameObject go)
		{
			SpawnGibs(go);
		}

		// ----------------------------------------------------------------------------------------

		public enum ColliderMode
		{
			Default,
			Box,
			Rectangle
		}

		public enum ColliderSizeMode
		{
			RelativeToSprite,
			Absolute
		}
	}
}
