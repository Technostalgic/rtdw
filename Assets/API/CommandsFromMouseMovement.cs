using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTDW.Actors;

namespace RTDW
{
	[RequireComponent(typeof(CommandPalette))]
	public class CommandsFromMouseMovement : MonoBehaviour
	{
		private CommandPalette commands = null;
		public CommandPalette Commands => commands == null ?
			commands = GetComponent<CommandPalette>() : commands;

		[SerializeField]
		private InputControl[] inputControls = null;
		public IReadOnlyList<InputControl> InputControls => inputControls;

		// ----------------------------------------------------------------------------------------

		public void ResolveInputControls()
		{
			Vector2 mouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
			foreach (InputControl control in inputControls) control.Resolve(Commands, mouseDelta);
		}

		// ----------------------------------------------------------------------------------------

		private void Update()
		{
			ResolveInputControls();
		}

		// ----------------------------------------------------------------------------------------

		[System.Serializable]
		public class InputControl
		{
			public string command = "Command";

			public float sensitivity = 0.1f;

			private int? commandHash = null;
			private int CommandHash => commandHash.HasValue ?
				commandHash.Value : (commandHash = command.GetHashCode()).Value;

			// ------------------------------------------------------------------------------------

			public void Resolve(CommandPalette palette, in Vector2 mouseDelta)
			{
				if (mouseDelta.x == 0 && mouseDelta.y == 0) return;

				Command cmd = palette.TryGetCommand(CommandHash);
				cmd.Send(mouseDelta * sensitivity);
			}
		}
	}
}
